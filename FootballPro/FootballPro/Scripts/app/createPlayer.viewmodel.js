﻿function CreatePlayerViewModel(app, dataModel) {
    var self = this;

    // CreatePlayerViewModel currently does not require data binding, so there are no visible members.
}

app.addViewModel({
    name: "CreatePlayer",
    bindingMemberName: "createPlayer",
    factory: CreatePlayerViewModel
});
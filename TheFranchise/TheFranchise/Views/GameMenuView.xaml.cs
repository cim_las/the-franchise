﻿using System.Windows;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    /// <summary>
    /// Interaction logic for GameMenuView.xaml
    /// </summary>
    public partial class GameMenuView : Window
    {
        public GameMenuView()
        {
            InitializeComponent();
        }
    }
}

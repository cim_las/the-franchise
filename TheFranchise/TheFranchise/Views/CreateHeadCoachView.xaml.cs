﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    public partial class CreateHeadCoachView : Window
    {
        public CreateHeadCoachView()
        {
            InitializeComponent();
            DataContext = ViewModelLocator.CreateHeadCoachStatic;

            ViewModelLocator.CreateHeadCoachStatic.ReRoll();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.CreateHeadCoachStatic.CreateHeadCoach();

            if (ViewModelLocator.CreateHeadCoachStatic.Success)
                this.Close();
        }
    }
}
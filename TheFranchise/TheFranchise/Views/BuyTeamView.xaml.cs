﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    public partial class BuyTeamView : Window
    {
        public BuyTeamView()
        {
            InitializeComponent();
            DataContext = ViewModelLocator.BuyTeamStatic;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.BuyTeamStatic.BuyTeam();

            if (ViewModelLocator.BuyTeamStatic.Success)
                this.Close();
        }
    }
}
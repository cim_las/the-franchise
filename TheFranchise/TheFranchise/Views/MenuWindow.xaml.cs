﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Common.Types;
using GalaSoft.MvvmLight.Command;
using log4net;
using Microsoft.Win32;
using PresentSoftware.Common.Business;
using PresentSoftware.Common.Controls.ProgressDialog;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    public partial class MenuWindow : Window
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MenuWindow()
        {
            InitializeComponent();

            // Set ViewModels
            var locator = new ViewModelLocator();

            DataContext = ViewModelLocator.MenuStatic;
        }

        #region EventHandlers

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateGame();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            LoadGame();
        }

        #endregion EventHandlers

        #region Private methods

        private void CreateGame()
        {
            var c = new CreateGameView();
            c.ShowDialog();
            if (ViewModelLocator.CreateGameStatic.Success != true) { return; }


            var o = new CreateHeadCoachView();
            o.ShowDialog();
            if (ViewModelLocator.CreateHeadCoachStatic.Success != true) { return; }

            var b = new BuyTeamView();
            b.ShowDialog();
            if (ViewModelLocator.BuyTeamStatic.Success != true) { return; }
            
            var mainWindow = new MainWindow();

            Hide();
            mainWindow.ShowDialog();
            Show();
        }

        private void LoadGame()
        {
            var dialog = new OpenFileDialog();

            dialog.Multiselect = false;
            dialog.Filter = @"Email Archives (*.sdf)|*.sdf";

            if (dialog.ShowDialog() == true)
            {
                _log.InfoFormat("Loading Game...");
                DbAccessor.OpenDb(dialog.FileName);

                var mainWindow = new MainWindow();

                Hide();
                mainWindow.ShowDialog();
                Show();
            }
        }

        #endregion Private methods
    }
}

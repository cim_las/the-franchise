﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    /// <summary>
    /// Interaction logic for LootWindow.xaml
    /// </summary>
    public partial class LootWindow : Window
    {
        public LootWindow()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object pSender, RoutedEventArgs pE)
        {
            Close();
        }
    }
}

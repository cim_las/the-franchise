﻿using System;
using System.Windows;
using log4net;
using PresentSoftware.Common.Data.Context;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public App()
        {
            _log.Info("Application started");  

            InitLogging();
        }

        private void InitLogging()
        {
            var fileAppender = new log4net.Appender.RollingFileAppender
            {
                Layout = new log4net.Layout.PatternLayout("%date [%thread] %-5level %logger - %message%newline"),
                RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Once,
                MaxSizeRollBackups = 4,
                ImmediateFlush = true,
                File = "logger.txt",
                AppendToFile = false
            };
            //fileAppender.Layout = new log4net.Layout.PatternLayout("%date [%thread] %-5level %30.30logger %ndc - %message%newline");

            fileAppender.ActivateOptions();

            log4net.Config.BasicConfigurator.Configure(fileAppender);

            var log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            //LogHelper.ChangeLogLevel(log, AppSettings.LogLevel);

            log.Info("Starting Game");
            log.Info(".NET Version: " + Environment.Version.ToString());
            log.Info("OS Version: " + Environment.OSVersion.ToString());
            log.Info("Processor Count: " + Environment.ProcessorCount);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //DataManager.Instance.CurrentGridIronStarsDBContext = new GridIronStarsDBContext(@"Data Source=|DataDirectory|DefaultGridIronStarsDB.sdf;");

            //DataManager.Instance.GetConnection();

            //DataManager.Instance.CurrentGridIronStarsDBContext = new GridIronStarsDBContext("GridIronStarsCodeFirst");

            //DataManager.Instance.CurrentGridIronStarsDBContext2 = DatabaseProject.DataBaseHelper.GetNewDBContext();

            //Database.SetInitializer<TheFranchiseDbContext>(new DropCreateDatabaseAlways<GridIronStarsDBContext>());
            //Database.SetInitializer<GridIronStarsDBContext>(new DropCreateDatabaseIfModelChanges<GridIronStarsDBContext>());

         //   var currentWindow = new MenuWindow();

           // currentWindow.Show();
        }
    }
}

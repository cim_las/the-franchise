﻿using System.Windows;
using log4net;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MainWindow()
        {
            InitializeComponent();
            //DataContext = this;

            ViewModelLocator.MainStatic.Load();
            ViewModelLocator.PeopleStatic.Load();
            ViewModelLocator.DraftingStatic.Load();
            ViewModelLocator.InboxStatic.Load();
            ViewModelLocator.TeamManagementStatic.Load();

            DataContext = ViewModelLocator.MainStatic;
            PeopleX.DataContext = ViewModelLocator.PeopleStatic;
            DraftingX.DataContext = ViewModelLocator.DraftingStatic;
            InboxX.DataContext = ViewModelLocator.InboxStatic;
            TeamManagementX.DataContext = ViewModelLocator.TeamManagementStatic;
        }
    }
}
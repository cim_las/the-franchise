﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    /// <summary>
    /// Interaction logic for CharactersView.xaml
    /// </summary>
    public partial class CharactersView : Window
    {
        public CharactersView()
        {
            InitializeComponent();
        }
    }
}

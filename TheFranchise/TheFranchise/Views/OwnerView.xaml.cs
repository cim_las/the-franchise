﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Factories;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    public partial class OwnerView : Window
    {
        public OwnerView()
        {
            InitializeComponent();

            ViewModelLocator.OwnerStatic.SelectedPerson = ViewModelLocator.PeopleStatic.SelectedPerson;
            // Force a refresh on owner
            ViewModelLocator.OwnerStatic.Owner = null;
            DataContext = ViewModelLocator.OwnerStatic;
        }
    }
}
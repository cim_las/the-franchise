﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;

namespace PresentSoftware.TheFranchise.DesktopUI.Views
{
    public partial class CreateGameView : Window
    {
        public CreateGameView()
        {
            InitializeComponent();
            
            DataContext = ViewModelLocator.CreateGameStatic;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.CreateGameStatic.CreateGame();

            if(ViewModelLocator.CreateGameStatic.Success)
                this.Close();
        }
    }
}
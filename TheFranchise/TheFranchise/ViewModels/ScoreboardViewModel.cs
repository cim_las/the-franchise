﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class ScoreboardViewModel : ViewModelBase
    {
        public ScoreboardViewModel(Team pHomeTeam, Team pAwayTeam)
        {
            HomeTeamName = pHomeTeam.TeamName;
            AwayTeamName = pAwayTeam.TeamName;
            AwayTeamScore = 0;
            HomeTeamScore = 0;
            Quarter = 1;
            TimeRemaining = new TimeSpan(0,0,15);
        }

        private int _awayTeamScore;
        public int AwayTeamScore
        {
            get { return _awayTeamScore; }
            set
            {
                _awayTeamScore = value;
                RaisePropertyChanged("AwayTeamScore");
            }
        }

        private string _awayTeamName;
        public string AwayTeamName
        {
            get { return _awayTeamName; }
            set
            {
                _awayTeamName = value;
                RaisePropertyChanged("AwayTeamName");
            }
        }

        private int _quarter;
        public int Quarter
        {
            get { return _quarter; }
            set
            {
                _quarter = value;
                RaisePropertyChanged("Quarter");
            }
        }

        private TimeSpan _timeRemaining;
        public TimeSpan TimeRemaining
        {
            get { return _timeRemaining; }
            set
            {
                _timeRemaining = value;
                RaisePropertyChanged("TimeRemaining");
            }
        }

        private string _homeTeamName;
        public string HomeTeamName
        {
            get { return _homeTeamName; }
            set
            {
                _homeTeamName = value;
                RaisePropertyChanged("HomeTeamName");
            }
        }

        private int _homeTeamScore;
        public int HomeTeamScore
        {
            get { return _homeTeamScore; }
            set
            {
                _homeTeamScore = value;
                RaisePropertyChanged("HomeTeamScore");
            }
        }
    }
}

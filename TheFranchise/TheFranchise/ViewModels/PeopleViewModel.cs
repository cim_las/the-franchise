﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Documents;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Extensions;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class PeopleViewModel : ViewModelBase, IPersistToDb
    {
        #region Fields

        private List<Person> _localPeople;
        private List<RosterSpot> _localRosterSpots;

        #endregion Fields

        #region Properties

        private bool _findTabIsSelected;

        public bool FindTabIsSelected
        {
            get { return _findTabIsSelected; }
            set
            {
                _findTabIsSelected = value;
                RaisePropertyChanged("FindTabIsSelected");
            }
        }

        private TeamNameEnum? _selectedTeam;

        public TeamNameEnum? SelectedTeam
        {
            get { return _selectedTeam ?? (_selectedTeam = (TeamNameEnum) DbAccessor.ReadTeams(p => p.IsHuman).FirstOrDefault().Id); }
            set
            {
                _selectedTeam = value;
                RaisePropertyChanged("SelectedTeam");

                LoadLocals();

                SelectedPerson = null;
                FilteredPersons = null;
            }
        }

        private int _levelQuery;

        public int LevelQuery
        {
            get { return _levelQuery; }
            set
            {
                _levelQuery = value;
                RaisePropertyChanged("LevelQuery");
            }
        }

        #region RosterPositions

        private Person _selectedQuarterback;

        public Person SelectedQuarterback
        {
            get { return _selectedQuarterback; // ?? (_SelectedQuarterback = new PersonViewModel());
            }
            set
            {
                if (_selectedQuarterback == value)
                    return;

                _selectedQuarterback = value;
                RaisePropertyChanged("SelectedQuarterback");
            }
        }

        private Person _selectedHalfback;

        public Person SelectedHalfback
        {
            get { return _selectedHalfback; }
            set
            {
                if (_selectedHalfback == value)
                    return;

                _selectedHalfback = value;
                RaisePropertyChanged("SelectedHalfback");
            }
        }

        private Person _selectedXReceiver;

        public Person SelectedXReceiver
        {
            get { return _selectedXReceiver; }
            set
            {
                if (_selectedXReceiver == value)
                    return;

                _selectedXReceiver = value;
                RaisePropertyChanged("SelectedXReceiver");
            }
        }

        private Person _selectedYReceiver;

        public Person SelectedYReceiver
        {
            get { return _selectedYReceiver; }
            set
            {
                if (_selectedYReceiver == value)
                    return;

                _selectedYReceiver = value;
                RaisePropertyChanged("SelectedYReceiver");
            }
        }

        private Person _selectedZReceiver;

        public Person SelectedZReceiver
        {
            get { return _selectedZReceiver; }
            set
            {
                if (_selectedZReceiver == value)
                    return;

                _selectedZReceiver = value;
                RaisePropertyChanged("SelectedZReceiver");
            }
        }

        private Person _selectedLeftTackle;

        public Person SelectedLeftTackle
        {
            get { return _selectedLeftTackle; }
            set
            {
                if (_selectedLeftTackle == value)
                    return;

                _selectedLeftTackle = value;
                RaisePropertyChanged("SelectedLeftTackle");
            }
        }

        private Person _selectedLeftGuard;

        public Person SelectedLeftGuard
        {
            get { return _selectedLeftGuard; }
            set
            {
                if (_selectedLeftGuard == value)
                    return;

                _selectedLeftGuard = value;
                RaisePropertyChanged("SelectedLeftGuard");
            }
        }

        private Person _selectedCenter;

        public Person SelectedCenter
        {
            get { return _selectedCenter; }
            set
            {
                if (_selectedCenter == value)
                    return;

                _selectedCenter = value;
                RaisePropertyChanged("SelectedCenter");
            }
        }

        private Person _selectedRightGuard;

        public Person SelectedRightGuard
        {
            get { return _selectedRightGuard; }
            set
            {
                if (_selectedRightGuard == value)
                    return;

                _selectedRightGuard = value;
                RaisePropertyChanged("SelectedRightGuard");
            }
        }

        private Person _selectedRightTackle;

        public Person SelectedRightTackle
        {
            get { return _selectedRightTackle; }
            set
            {
                if (_selectedRightTackle == value)
                    return;

                _selectedRightTackle = value;
                RaisePropertyChanged("SelectedRightTackle");
            }
        }

        private Person _selectedTightEnd;

        public Person SelectedTightEnd
        {
            get { return _selectedTightEnd; }
            set
            {
                if (_selectedTightEnd == value)
                    return;

                _selectedTightEnd = value;
                RaisePropertyChanged("SelectedTightEnd");
            }
        }

        private Person _selectedLeftEnd;

        public Person SelectedLeftEnd
        {
            get { return _selectedLeftEnd; }
            set
            {
                if (_selectedLeftEnd == value)
                    return;

                _selectedLeftEnd = value;
                RaisePropertyChanged("SelectedLeftEnd");
            }
        }

        private Person _selectedRightEnd;

        public Person SelectedRightEnd
        {
            get { return _selectedRightEnd; }
            set
            {
                if (_selectedRightEnd == value)
                    return;

                _selectedRightEnd = value;
                RaisePropertyChanged("SelectedRightEnd");
            }
        }

        private Person _selectedLeftDefensiveTackle;

        public Person SelectedLeftDefensiveTackle
        {
            get { return _selectedLeftDefensiveTackle; }
            set
            {
                if (_selectedLeftDefensiveTackle == value)
                    return;

                _selectedLeftDefensiveTackle = value;
                RaisePropertyChanged("SelectedLeftDefensiveTackle");
            }
        }

        private Person _selectedRightDefensiveTackle;

        public Person SelectedRightDefensiveTackle
        {
            get { return _selectedRightDefensiveTackle; }
            set
            {
                if (_selectedRightDefensiveTackle == value)
                    return;

                _selectedRightDefensiveTackle = value;
                RaisePropertyChanged("SelectedRightDefensiveTackle");
            }
        }

        private Person _selectedWillLinebacker;

        public Person SelectedWillLinebacker
        {
            get { return _selectedWillLinebacker; }
            set
            {
                if (_selectedWillLinebacker == value)
                    return;

                _selectedWillLinebacker = value;
                RaisePropertyChanged("SelectedWillLinebacker");
            }
        }

        private Person _selectedMikeLinebacker;

        public Person SelectedMikeLinebacker
        {
            get { return _selectedMikeLinebacker; }
            set
            {
                if (_selectedMikeLinebacker == value)
                    return;

                _selectedMikeLinebacker = value;
                RaisePropertyChanged("SelectedMikeLinebacker");
            }
        }

        private Person _selectedSamLinebacker;

        public Person SelectedSamLinebacker
        {
            get { return _selectedSamLinebacker; }
            set
            {
                if (_selectedSamLinebacker == value)
                    return;

                _selectedSamLinebacker = value;
                RaisePropertyChanged("SelectedSamLinebacker");
            }
        }

        private Person _selectedLeftCornerback;

        public Person SelectedLeftCornerback
        {
            get { return _selectedLeftCornerback; }
            set
            {
                if (_selectedLeftCornerback == value)
                    return;

                _selectedLeftCornerback = value;
                RaisePropertyChanged("SelectedLeftCornerback");
            }
        }

        private Person _selectedRightCornerback;

        public Person SelectedRightCornerback
        {
            get { return _selectedRightCornerback; }
            set
            {
                if (_selectedRightCornerback == value)
                    return;

                _selectedRightCornerback = value;
                RaisePropertyChanged("SelectedRightCornerback");
            }
        }

        private Person _selectedFreeSafety;

        public Person SelectedFreeSafety
        {
            get { return _selectedFreeSafety; }
            set
            {
                if (_selectedFreeSafety == value)
                    return;

                _selectedFreeSafety = value;
                RaisePropertyChanged("SelectedFreeSafety");
            }
        }

        private Person _selectedStrongSafety;

        public Person SelectedStrongSafety
        {
            get { return _selectedStrongSafety; }
            set
            {
                if (_selectedStrongSafety == value)
                    return;

                _selectedStrongSafety = value;
                RaisePropertyChanged("SelectedStrongSafety");
            }
        }

        private Person _selectedPresident;

        public Person SelectedPresident
        {
            get { return _selectedPresident; }
            set
            {
                if (_selectedPresident == value)
                    return;

                _selectedPresident = value;
                RaisePropertyChanged("SelectedPresident");
            }
        }

        private Person _selectedGeneralManager;

        public Person SelectedGeneralManager
        {
            get { return _selectedGeneralManager; }
            set
            {
                if (_selectedGeneralManager == value)
                    return;

                _selectedGeneralManager = value;
                RaisePropertyChanged("SelectedGeneralManager");
            }
        }

        private Person _selectedHeadCoach;

        public Person SelectedHeadCoach
        {
            get { return _selectedHeadCoach; }
            set
            {
                if (_selectedHeadCoach == value)
                    return;

                _selectedHeadCoach = value;
                RaisePropertyChanged("SelectedHeadCoach");
            }
        }

        private Person _selectedOffensiveCoordinator;

        public Person SelectedOffensiveCoordinator
        {
            get { return _selectedOffensiveCoordinator; }
            set
            {
                if (_selectedOffensiveCoordinator == value)
                    return;

                _selectedOffensiveCoordinator = value;
                RaisePropertyChanged("SelectedOffensiveCoordinator");
            }
        }

        private Person _selectedQuarterbacksCoach;

        public Person SelectedQuarterbacksCoach
        {
            get { return _selectedQuarterbacksCoach; }
            set
            {
                if (_selectedQuarterbacksCoach == value)
                    return;

                _selectedQuarterbacksCoach = value;
                RaisePropertyChanged("SelectedQuarterbacksCoach");
            }
        }

        private Person _selectedRunningbacksCoach;

        public Person SelectedRunningbacksCoach
        {
            get { return _selectedRunningbacksCoach; }
            set
            {
                if (_selectedRunningbacksCoach == value)
                    return;

                _selectedRunningbacksCoach = value;
                RaisePropertyChanged("SelectedRunningbacksCoach");
            }
        }

        private Person _selectedReceiversCoach;

        public Person SelectedReceiversCoach
        {
            get { return _selectedReceiversCoach; }
            set
            {
                if (_selectedReceiversCoach == value)
                    return;

                _selectedReceiversCoach = value;
                RaisePropertyChanged("SelectedReceiversCoach");
            }
        }

        private Person _selectedOffensiveLineCoach;

        public Person SelectedOffensiveLineCoach
        {
            get { return _selectedOffensiveLineCoach; }
            set
            {
                if (_selectedOffensiveLineCoach == value)
                    return;

                _selectedOffensiveLineCoach = value;
                RaisePropertyChanged("SelectedOffensiveLineCoach");
            }
        }

        private Person _selectedDefensiveCoordinator;

        public Person SelectedDefensiveCoordinator
        {
            get { return _selectedDefensiveCoordinator; }
            set
            {
                if (_selectedDefensiveCoordinator == value)
                    return;

                _selectedDefensiveCoordinator = value;
                RaisePropertyChanged("SelectedDefensiveCoordinator");
            }
        }

        private Person _selectedDefensiveLineCoach;

        public Person SelectedDefensiveLineCoach
        {
            get { return _selectedDefensiveLineCoach; }
            set
            {
                if (_selectedDefensiveLineCoach == value)
                    return;

                _selectedDefensiveLineCoach = value;
                RaisePropertyChanged("SelectedDefensiveLineCoach");
            }
        }

        private Person _selectedLinebackersCoach;

        public Person SelectedLinebackersCoach
        {
            get { return _selectedLinebackersCoach; }
            set
            {
                if (_selectedLinebackersCoach == value)
                    return;

                _selectedLinebackersCoach = value;
                RaisePropertyChanged("SelectedLinebackersCoach");
            }
        }

        private Person _selectedDefenisvebacksCoach;

        public Person SelectedDefenisvebacksCoach
        {
            get { return _selectedDefenisvebacksCoach; }
            set
            {
                if (_selectedDefenisvebacksCoach == value)
                    return;

                _selectedDefenisvebacksCoach = value;
                RaisePropertyChanged("SelectedDefenisvebacksCoach");
            }
        }

        private Person _selectedSpecialTeamsCoordinator;

        public Person SelectedSpecialTeamsCoordinator
        {
            get { return _selectedSpecialTeamsCoordinator; }
            set
            {
                if (_selectedSpecialTeamsCoordinator == value)
                    return;

                _selectedSpecialTeamsCoordinator = value;
                RaisePropertyChanged("SelectedSpecialTeamsCoordinator");
            }
        }

        private Person _selectedStrengthAndConditioningCoordinator;

        public Person SelectedStrengthAndConditioningCoordinator
        {
            get { return _selectedStrengthAndConditioningCoordinator; }
            set
            {
                if (_selectedStrengthAndConditioningCoordinator == value)
                    return;

                _selectedStrengthAndConditioningCoordinator = value;
                RaisePropertyChanged("SelectedStrengthAndConditioningCoordinator");
            }
        }

        private Person _selectedNationalScout;
        public Person SelectedNationalScout
        {
            get { return _selectedNationalScout; }
            set
            {
                if (_selectedNationalScout == value)
                    return;

                _selectedNationalScout = value;
                RaisePropertyChanged("SelectedNationalScout");
            }
        }

        private Person _selectedNorhEastScout;
        public Person SelectedNorhEastScout
        {
            get { return _selectedNorhEastScout; }
            set
            {
                if (_selectedNorhEastScout == value)
                    return;

                _selectedNorhEastScout = value;
                RaisePropertyChanged("SelectedNorhEastScout");
            }
        }

        private Person _selectedSouthEastScout;
        public Person SelectedSouthEastScout
        {
            get { return _selectedSouthEastScout; }
            set
            {
                if (_selectedSouthEastScout == value)
                    return;

                _selectedSouthEastScout = value;
                RaisePropertyChanged("SelectedSouthEastScout");
            }
        }

        private Person _selectedCentralScout;
        public Person SelectedCentralScout
        {
            get { return _selectedCentralScout; }
            set
            {
                if (_selectedCentralScout == value)
                    return;

                _selectedCentralScout = value;
                RaisePropertyChanged("SelectedCentralScout");
            }
        }

        private Person _selectedMidWestScout;
        public Person SelectedMidWestScout
        {
            get { return _selectedMidWestScout; }
            set
            {
                if (_selectedMidWestScout == value)
                    return;

                _selectedMidWestScout = value;
                RaisePropertyChanged("SelectedMidWestScout");
            }
        }

        private Person _selectedWestScout;
        public Person SelectedWestScout
        {
            get { return _selectedWestScout; }
            set
            {
                if (_selectedWestScout == value)
                    return;

                _selectedWestScout = value;
                RaisePropertyChanged("SelectedWestScout");
            }
        }

        private Person _selectedQuarterbackRunningbackScout;
        public Person SelectedQuarterbackRunningbackScout
        {
            get { return _selectedQuarterbackRunningbackScout; }
            set
            {
                if (_selectedQuarterbackRunningbackScout == value)
                    return;

                _selectedQuarterbackRunningbackScout = value;
                RaisePropertyChanged("SelectedQuarterbackRunningbackScout");
            }
        }

        private Person _selectedReceiverScout;
        public Person SelectedReceiverScout
        {
            get { return _selectedReceiverScout; }
            set
            {
                if (_selectedReceiverScout == value)
                    return;

                _selectedReceiverScout = value;
                RaisePropertyChanged("SelectedReceiverScout");
            }
        }

        private Person _selectedOffensiveLineScout;
        public Person SelectedOffensiveLineScout
        {
            get { return _selectedOffensiveLineScout; }
            set
            {
                if (_selectedOffensiveLineScout == value)
                    return;

                _selectedOffensiveLineScout = value;
                RaisePropertyChanged("SelectedOffensiveLineScout");
            }
        }

        private Person _selectedDefensiveLineScout;
        public Person SelectedDefensiveLineScout
        {
            get { return _selectedDefensiveLineScout; }
            set
            {
                if (_selectedDefensiveLineScout == value)
                    return;

                _selectedDefensiveLineScout = value;
                RaisePropertyChanged("SelectedDefensiveLineScout");
            }
        }

        private Person _selectedLinebackerScout;
        public Person SelectedLinebackerScout
        {
            get { return _selectedLinebackerScout; }
            set
            {
                if (_selectedLinebackerScout == value)
                    return;

                _selectedLinebackerScout = value;
                RaisePropertyChanged("SelectedLinebackerScout");
            }
        }

        private Person _selectedDefensivebackScout;
        public Person SelectedDefensivebackScout
        {
            get { return _selectedDefensivebackScout; }
            set
            {
                if (_selectedDefensivebackScout == value)
                    return;

                _selectedDefensivebackScout = value;
                RaisePropertyChanged("SelectedDefensivebackScout");
            }
        }

        #endregion RosterPositions

        private PositionEnum _selectedPosition;

        public PositionEnum SelectedPosition
        {
            get { return _selectedPosition; }
            set
            {
                if (_selectedPosition == value)
                    return;

                _selectedPosition = value;
                RaisePropertyChanged("SelectedPosition");
            }
        }

        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get { return _selectedPerson; // ?? (_selectedPerson = new PersonViewModel());
            }
            set
            {
                if (_selectedPerson == value)
                    return;

                _selectedPerson = value;

                SetRosterSpot();

                // Nead to refresh the members list after Setting someone on the roster spot
                LoadMembers();

                RaisePropertyChanged("SelectedPerson");
            }
        }

        private void SetRosterSpot()
        {
            if (SelectedPerson == null)
                return;

            switch (SelectedPosition)
            {
                case PositionEnum.Quarterback:
                {
                    SelectedQuarterback = SelectedPerson;
                    break;
                }
                case PositionEnum.HalfBack:
                {
                    SelectedHalfback = SelectedPerson;
                    break;
                }
                case PositionEnum.WideReceiver:
                {
                    SelectedXReceiver = SelectedPerson;
                    break;
                }
                case PositionEnum.OffensiveLineman:
                {
                    SelectedLeftTackle = SelectedPerson;
                    break;
                }
                case PositionEnum.TightEnd:
                {
                    SelectedTightEnd = SelectedPerson;
                    break;
                }
                case PositionEnum.Defensiveback:
                {
                    SelectedLeftCornerback = SelectedPerson;
                    break;
                }
                case PositionEnum.DefensiveLineman:
                {
                    SelectedLeftEnd = SelectedPerson;
                    break;
                }
                case PositionEnum.Linebacker:
                {
                    SelectedWillLinebacker = SelectedPerson;
                    break;
                }
                case PositionEnum.President:
                {
                    SelectedPresident = SelectedPerson;
                    break;
                }
                case PositionEnum.HeadCoach:
                {
                    SelectedHeadCoach = SelectedPerson;
                    break;
                }
                case PositionEnum.GeneralManager:
                {
                    SelectedGeneralManager = SelectedPerson;
                    break;
                }
                case PositionEnum.OffensiveCoordinator:
                {
                    SelectedOffensiveCoordinator = SelectedPerson;
                    break;
                }
                case PositionEnum.DefensiveCoordinator:
                {
                    SelectedDefensiveCoordinator = SelectedPerson;
                    break;
                }
            }

            var rosterSpot = DbAccessor.UpdateRoster(_selectedTeam, SelectedPerson, SelectedPosition);
            UpdateLocalRosterSpot(rosterSpot);
            //SelectedPerson = null;
        }

        private ObservableCollection<Person> _filteredPersons;

        public ObservableCollection<Person> FilteredPersons
        {
            get { return _filteredPersons ?? (_filteredPersons = new ObservableCollection<Person>()); }
            set
            {
                _filteredPersons = value;
                RaisePropertyChanged("FilteredPersons");
            }
        }

        private ObservableCollection<Person> _players;

        public ObservableCollection<Person> Members
        {
            get { return _players ?? (_players = new ObservableCollection<Person>()); }
            set
            {
                _players = value;
                RaisePropertyChanged("Members");
            }
        }

        #endregion Properties

        #region LoadButtonCommand

        private RelayCommand<bool?> _loadButtonCommand;

        public ICommand LoadButtonCommand
        {
            get { return _loadButtonCommand ?? (_loadButtonCommand = new RelayCommand<bool?>(p => this.LoadMembers())); }
        }

        private void LoadMembers()
        {
            if (SelectedPosition == PositionEnum.None)
            {
                //FilteredPersons = null;
                Members = null;
                //Coaches = null;
                return;
            }

            //Expression<Func<Person, bool>> teamPredicate = p => true;
            Expression<Func<Person, bool>> jobPredicate = p => true;
            Expression<Func<Person, bool>> levelPredicate = p => true;

            //if (SelectedTeam != 0)
            //{
            //    teamPredicate = p => p.TeamId == (int)_selectedTeam;
            //}

            if (SelectedPosition != 0)
            {
                jobPredicate = p => _selectedPosition == (_selectedPosition & p.Position);
            }

            if (LevelQuery != 0)
            {
                levelPredicate = p => p.Level >= LevelQuery;
            }

            //FilteredPersons = new ObservableCollection<Person>(_localPeople.AsQueryable().Where(p => _selectedPosition == (_selectedPosition & p.Position)).Where(levelPredicate));

            Members =
                new ObservableCollection<Person>(_localPeople
                    .AsQueryable()
                    .Where(jobPredicate)
                    .Where(p => !_localRosterSpots.Where(r => r.TeamId == (int) _selectedTeam).Select(i => i.PersonId).Contains(p.Id)));
        }

        private void FillPersonItems(IEnumerable<Person> pPersons)
        {
            foreach (var person in pPersons)
            {
                person.Items = DbAccessor.ReadItems(p => p.ItemOwnerId == person.Id).ToList();
            }
        }

        #endregion LoadButtonCommand

        #region PreviousButtonCommand

        private RelayCommand<bool?> _PreviousButtonCommand;

        public ICommand PreviousButtonCommand
        {
            get { return _PreviousButtonCommand ?? (_PreviousButtonCommand = new RelayCommand<bool?>(p => this.PreviousButton())); }
        }

        private void PreviousButton()
        {
            if (FilteredPersons.Count > 0)
            {
                if (SelectedPerson == null)
                    SelectedPerson = FilteredPersons.First();
                else
                {
                    SelectedPerson = FilteredPersons[Math.Max(FilteredPersons.IndexOf(SelectedPerson) - 1, 0)];
                }
            }
        }

        #endregion PreviousButtonCommand

        #region NextButtonCommand

        private RelayCommand<bool?> _nextButtonCommand;

        public ICommand NextButtonCommand
        {
            get { return _nextButtonCommand ?? (_nextButtonCommand = new RelayCommand<bool?>(p => this.NextButton())); }
        }

        private void NextButton()
        {
            if (FilteredPersons.Count > 0)
            {
                if (SelectedPerson == null)
                    SelectedPerson = FilteredPersons.First();
                else
                    SelectedPerson = FilteredPersons[Math.Min(FilteredPersons.IndexOf(SelectedPerson) + 1, FilteredPersons.Count - 1)];
            }
        }

        #endregion NextButtonCommand

        #region PositionSelectCommand

        private RelayCommand<PositionEnum> _positionSelectCommand;

        public ICommand PositionSelectCommand
        {
            get { return _positionSelectCommand ?? (_positionSelectCommand = new RelayCommand<PositionEnum>(p => this.PositionSelect(p))); }
        }

        private void PositionSelect(PositionEnum pPosition)
        {
            SelectedPosition = pPosition;

            //var rosterSpot = DbAccessor.ReadRosterSpots().FirstOrDefault(p => p.Position == pPosition);
            //if (rosterSpot.PersonId != 0)
            //    SelectedPerson = DbAccessor.ReadPeople(p => p.Id == rosterSpot.PersonId).FirstOrDefault();

            var spot = _localRosterSpots.FirstOrDefault(r => r.Position == pPosition);
            if (spot != null)
            {
                SelectedPerson = _localPeople.FirstOrDefault(p => p.Id == spot.PersonId);
            }

            LoadMembers();
        }

        #endregion PositionSelectCommand

        #region Private Methods

        private void LoadLocals()
        {
            _localRosterSpots = DbAccessor.ReadRosterSpots(p => p.TeamId == (int) SelectedTeam).ToList();
            _localPeople = DbAccessor.ReadPeople(p => p.TeamId == (int) SelectedTeam).ToList();
            FillPersonItems(_localPeople);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.Quarterback))
                SelectedQuarterback = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.Quarterback).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.HalfBack))
                SelectedHalfback = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.HalfBack).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.WideReceiver))
                SelectedXReceiver = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.WideReceiver).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.OffensiveLineman))
                SelectedLeftTackle = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.OffensiveLineman).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.TightEnd))
                SelectedTightEnd = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.TightEnd).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.DefensiveLineman))
                SelectedLeftEnd = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.DefensiveLineman).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.Linebacker))
                SelectedWillLinebacker = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.Linebacker).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.Defensiveback))
                SelectedLeftCornerback = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.Defensiveback).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.President))
                SelectedPresident = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.President).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.GeneralManager))
                SelectedGeneralManager = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.GeneralManager).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.HeadCoach))
                SelectedHeadCoach = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.HeadCoach).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.OffensiveCoordinator))
                SelectedOffensiveCoordinator = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.OffensiveCoordinator).PersonId);

            if (_localRosterSpots.Any(r => r.Position == PositionEnum.DefensiveCoordinator))
                SelectedDefensiveCoordinator = _localPeople.FirstOrDefault(p => p.Id == _localRosterSpots.FirstOrDefault(r => r.Position == PositionEnum.DefensiveCoordinator).PersonId);
        }

        private void UpdateLocalRosterSpot(RosterSpot pRosterSpot)
        {
            var remove = _localRosterSpots.FirstOrDefault(p => p.Position == pRosterSpot.Position);

            _localRosterSpots.Remove(remove);

            _localRosterSpots.Add(pRosterSpot);
        }

        #endregion Private Methods

        #region IPersistToDbImplementation

        public void Load()
        {
            LoadLocals();
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

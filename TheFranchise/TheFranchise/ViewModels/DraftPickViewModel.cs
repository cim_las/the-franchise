﻿using System;
using System.Linq;
using GalaSoft.MvvmLight;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class DraftPickViewModel : ViewModelBase, IPersistToDb
    {
        #region Fields
        private DraftPick _CurrentDraftPick;
        #endregion

        #region Constructors
        public DraftPickViewModel(DraftPick pDraftPick)
        {
            _CurrentDraftPick = pDraftPick;
        }
        #endregion

        #region Properties
        public string TeamName
        {
            get
            {
                if (_CurrentDraftPick == null)
                {
                    return string.Empty;
                }
                return DbAccessor.ReadTeams().First(p => p.Id == _CurrentDraftPick.TeamId).TeamName;
            }
        }

        public string DraftPickText
        {
            get
            {
                if (_CurrentDraftPick == null)
                {
                    return string.Empty;
                }
                int pickNumber = _CurrentDraftPick.DraftPickNumber % TeamHelper.IntNumberOfTeams;
                return String.Format("Pick {0} ({1})", pickNumber != 0 ? pickNumber : TeamHelper.IntNumberOfTeams, _CurrentDraftPick.DraftPickNumber);
            }
            set
            {
                _CurrentDraftPick.DraftPickNumber = Convert.ToInt32(value);
                RaisePropertyChanged("DraftPickNumber");
            }
        }

        public int DraftPickNumber
        {
            get
            {
                return _CurrentDraftPick.DraftPickNumber;
            }
        }
        
        public string Round
        {
            get
            {
                if (_CurrentDraftPick == null)
                {
                    return string.Empty;
                }
                int round = Math.Min(Math.Max((_CurrentDraftPick.DraftPickNumber / TeamHelper.IntNumberOfTeams) + 1, DraftHelper.INT_FirstRound), DraftHelper.INT_MaxDraftRounds);
                return String.Format("Round {0}", round);
            }
        }
 
        public int PersonId
        {
            get
            {
                return _CurrentDraftPick.PersonId;
            }
            set
            {
                _CurrentDraftPick.PersonId = value;
                RaisePropertyChanged("PersonId");
            }
        }
        #endregion Properties

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

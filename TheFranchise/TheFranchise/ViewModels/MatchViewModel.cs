﻿using System.ComponentModel;
using GalaSoft.MvvmLight;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class MatchViewModel : ViewModelBase, IPersistToDb
    {
        private Team _homeTeam;
        private Team _awayTeam;

        public MatchViewModel(Team pHomeTeam, Team pAwayTeam)
        {
            _homeTeam = pHomeTeam;
            _awayTeam = pAwayTeam;
            Load();
        }

        private ScoreboardViewModel _myScoreboardViewModel;
        public ScoreboardViewModel MyScoreboardViewModel
        {
            get { return _myScoreboardViewModel; }
            set
            {
                _myScoreboardViewModel = value;
                RaisePropertyChanged("MyScoreboardViewModel");
            }
        }

        private FieldViewModel _myFieldViewModel;
        public FieldViewModel MyFieldViewModel
        {
            get { return _myFieldViewModel; }
            set
            {
                _myFieldViewModel = value;
                RaisePropertyChanged("MyFieldViewModel");
            }
        }

        private MatchInteractionViewModel _myMatchInteractionViewModel;
        public MatchInteractionViewModel MyMatchInteractionViewModel
        {
            get { return _myMatchInteractionViewModel; }
            set
            {
                _myMatchInteractionViewModel = value;
                RaisePropertyChanged("MyMatchInteractionViewModel");
            }
        }

        #region IPersistToDb Implementation

        public void Load()
        {
            var homeRoster = DbAccessor.ReadPeople(p => p.TeamId == _homeTeam.Id);
            var awayRoster = DbAccessor.ReadPeople(p => p.TeamId == _awayTeam.Id);

            MyScoreboardViewModel = new ScoreboardViewModel(_homeTeam, _awayTeam);
            MyFieldViewModel = new FieldViewModel(homeRoster, awayRoster);
            MyMatchInteractionViewModel = new MatchInteractionViewModel();
        }

        public void Save(object pSender, CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPersistToDb Implementation
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class BuyTeamViewModel : ViewModelBase, IPersistToDb
    {
        #region Properties

        public bool Success { get; set; }

        private IList<Team> _teams;
        public IList<Team> Teams
        {
            get { return _teams ?? (_teams = DbAccessor.ReadTeams()); }
        }

        private Team _selectedTeam;
        public Team SelectedTeam
        {
            get
            {
                return _selectedTeam ?? (_selectedTeam = Teams.FirstOrDefault());
            }
            set
            {
                _selectedTeam = value;
                RaisePropertyChanged("SelectedTeam");
            }
        }

        private string _errorInfo;
        public string ErrorInfo
        {
            get
            {
                return _errorInfo;
            }
            set
            {
                _errorInfo = value;
                RaisePropertyChanged("ErrorInfo");
            }
        }

        #endregion

        #region BuyTeamCommand

        private RelayCommand<bool?> _buyTeamCommand;
        public ICommand BuyTeamCommand
        {
            get { return _buyTeamCommand ?? (_buyTeamCommand = new RelayCommand<bool?>(p => this.BuyTeam())); }
        }

        public void BuyTeam()
        {
            var settings = DbAccessor.ReadSetting();
            var people = DbAccessor.ReadPeople();
            var person = people.First(p => p.Id == settings.HeadCoachId);

            if (person.Cash < SelectedTeam.EstimatedValue)
            {
                ErrorInfo = "Not enough cash!";
                return;
            }

            // Remove old owner from purchased team
            var oldPerson = people.First(p => p.Position == PositionEnum.HeadCoach && p.TeamId == SelectedTeam.Id);
            oldPerson.TeamId = null;
            DbAccessor.SavePerson(oldPerson);

            // Set team as human
            SelectedTeam.IsHuman = true;
            DbAccessor.SaveTeam(SelectedTeam);
            
            // Update new cash
            person.Cash -= SelectedTeam.EstimatedValue;
            person.TeamId = SelectedTeam.Id;
            DbAccessor.SavePerson(person);

            Success = true;
        }

        #endregion BuyTeamCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}
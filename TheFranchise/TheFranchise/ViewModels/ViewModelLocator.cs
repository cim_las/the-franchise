﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:MvvmLight1.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
  
  OR (WPF only):
  
  xmlns:vm="clr-namespace:MvvmLight1.ViewModel"
  DataContext="{Binding Source={x:Static vm:ViewModelLocatorTemplate.ViewModelNameStatic}}"
*/

using GalaSoft.MvvmLight;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// Use the <strong>mvvmlocatorproperty</strong> snippet to add ViewModels
    /// to this locator.
    /// </para>
    /// <para>
    /// In Silverlight and WPF, place the ViewModelLocatorTemplate in the App.xaml resources:
    /// </para>
    /// <code>
    /// &lt;Application.Resources&gt;
    ///     &lt;vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:MvvmLight1.ViewModel"
    ///                                  x:Key="Locator" /&gt;
    /// &lt;/Application.Resources&gt;
    /// </code>
    /// <para>
    /// Then use:
    /// </para>
    /// <code>
    /// DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
    /// </code>
    /// <para>
    /// You can also use Blend to do all this with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// <para>
    /// In <strong>*WPF only*</strong> (and if databinding in Blend is not relevant), you can delete
    /// the Main property and bind to the ViewModelNameStatic property instead:
    /// </para>
    /// <code>
    /// xmlns:vm="clr-namespace:MvvmLight1.ViewModel"
    /// DataContext="{Binding Source={x:Static vm:ViewModelLocatorTemplate.ViewModelNameStatic}}"
    /// </code>
    /// </summary>
    public class ViewModelLocator
    {
        #region Properties
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view models
            }
            else
            {
                // Create run time view models
                CreateViewModels();
            }
        }

        #endregion

        #region MenuViewModel

        private static MenuViewModel _MenuViewModel;
        /// <summary>
        /// Gets the Menu property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MenuViewModel Menu
        {
            get
            {
                return MenuStatic;
            }
        }

        /// <summary>
        /// Gets the Menu property.
        /// </summary>
        public static MenuViewModel MenuStatic
        {
            get
            {
                //if (_MenuViewModel == null)
                //{
                //    CreateMenu();
                //}

                return _MenuViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Menu property.
        /// </summary>
        public static void CreateMenu()
        {
            if (_MenuViewModel == null)
            {
                _MenuViewModel = new MenuViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Menu property.
        /// </summary>
        public static void ClearMenu()
        {
            if (_MenuViewModel != null)
            {
                _MenuViewModel.Cleanup();
                _MenuViewModel = null;
            }
        }

        #endregion MenuViewModel

        #region MainViewModel

        private static MainViewModel _MainViewModel;
        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return MainStatic;
            }
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public static MainViewModel MainStatic
        {
            get
            {
                //if (_MainViewModel == null)
                //{
                //    CreateMain();
                //}

                return _MainViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Main property.
        /// </summary>
        public static void CreateMain()
        {
            if (_MainViewModel == null)
            {
                _MainViewModel = new MainViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Main property.
        /// </summary>
        public static void ClearMain()
        {
            if (_MainViewModel != null)
            {
                _MainViewModel.Cleanup();
                _MainViewModel = null;
            }
        }

        #endregion MainViewModel

        #region PeopleViewModel

        private static PeopleViewModel _PeopleViewModel;
        /// <summary>
        /// Gets the People property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public PeopleViewModel People
        {
            get
            {
                return PeopleStatic;
            }
        }

        /// <summary>
        /// Gets the People property.
        /// </summary>
        public static PeopleViewModel PeopleStatic
        {
            get
            {
                //if (_PeopleViewModel == null)
                //{
                //    CreatePeople();
                //}

                return _PeopleViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the People property.
        /// </summary>
        public static void CreatePeople()
        {
            if (_PeopleViewModel == null)
            {
                _PeopleViewModel = new PeopleViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the People property.
        /// </summary>
        public static void ClearPeople()
        {
            if (_PeopleViewModel != null)
            {
                _PeopleViewModel.Cleanup();
                _PeopleViewModel = null;
            }
        }

        #endregion PeopleViewModel

        #region TeamManagementViewModel

        private static TeamManagementViewModel _TeamManagementViewModel;
        /// <summary>
        /// Gets the TeamManagement property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public TeamManagementViewModel TeamManagement
        {
            get
            {
                return TeamManagementStatic;
            }
        }

        /// <summary>
        /// Gets the TeamManagement property.
        /// </summary>
        public static TeamManagementViewModel TeamManagementStatic
        {
            get
            {
                //if (_TeamManagementViewModel == null)
                //{
                //    CreateTeamManagement();
                //}

                return _TeamManagementViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TeamManagement property.
        /// </summary>
        public static void CreateTeamManagement()
        {
            if (_TeamManagementViewModel == null)
            {
                _TeamManagementViewModel = new TeamManagementViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TeamManagement property.
        /// </summary>
        public static void ClearTeamManagement()
        {
            if (_TeamManagementViewModel != null)
            {
                _TeamManagementViewModel.Cleanup();
                _TeamManagementViewModel = null;
            }
        }

        #endregion TeamManagementViewModel

        #region DraftingViewModel

        private static DraftingViewModel _DraftingViewModel;
        /// <summary>
        /// Gets the Drafting property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DraftingViewModel Drafting
        {
            get
            {
                return DraftingStatic;
            }
        }

        /// <summary>
        /// Gets the Drafting property.
        /// </summary>
        public static DraftingViewModel DraftingStatic
        {
            get
            {
                //if (_DraftingViewModel == null)
                //{
                //    CreateDrafting();
                //}

                return _DraftingViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Drafting property.
        /// </summary>
        public static void CreateDrafting()
        {
            if (_DraftingViewModel == null)
            {
                _DraftingViewModel = new DraftingViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Drafting property.
        /// </summary>
        public static void ClearDrafting()
        {
            if (_DraftingViewModel != null)
            {
                _DraftingViewModel.Cleanup();
                _DraftingViewModel = null;
            }
        }

        #endregion DraftingViewModel

        #region InboxViewModel

        private static InboxViewModel _InboxViewModel;
        /// <summary>
        /// Gets the Inbox property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public InboxViewModel Inbox
        {
            get
            {
                return InboxStatic;
            }
        }

        /// <summary>
        /// Gets the Inbox property.
        /// </summary>
        public static InboxViewModel InboxStatic
        {
            get
            {
                //if (_InboxViewModel == null)
                //{
                //    CreateInbox();
                //}

                return _InboxViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Inbox property.
        /// </summary>
        public static void CreateInbox()
        {
            if (_InboxViewModel == null)
            {
                _InboxViewModel = new InboxViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Inbox property.
        /// </summary>
        public static void ClearInbox()
        {
            if (_InboxViewModel != null)
            {
                _InboxViewModel.Cleanup();
                _InboxViewModel = null;
            }
        }

        #endregion InboxViewModel

        #region BuyTeamViewModel

        private static BuyTeamViewModel _BuyTeamViewModel;
        /// <summary>
        /// Gets the BuyTeam property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public BuyTeamViewModel BuyTeam
        {
            get
            {
                return BuyTeamStatic;
            }
        }

        /// <summary>
        /// Gets the BuyTeam property.
        /// </summary>
        public static BuyTeamViewModel BuyTeamStatic
        {
            get
            {
                //if (_BuyTeamViewModel == null)
                //{
                //    CreateBuyTeam();
                //}

                return _BuyTeamViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BuyTeam property.
        /// </summary>
        public static void CreateBuyTeam()
        {
            if (_BuyTeamViewModel == null)
            {
                _BuyTeamViewModel = new BuyTeamViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BuyTeam property.
        /// </summary>
        public static void ClearBuyTeam()
        {
            if (_BuyTeamViewModel != null)
            {
                _BuyTeamViewModel.Cleanup();
                _BuyTeamViewModel = null;
            }
        }

        #endregion BuyTeamViewModel

        #region CreateHeadCoachViewModel

        private static CreateHeadCoachViewModel _CreateHeadCoachViewModel;
        /// <summary>
        /// Gets the CreateHeadCoach property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public CreateHeadCoachViewModel CreateHeadCoach
        {
            get
            {
                return CreateHeadCoachStatic;
            }
        }

        /// <summary>
        /// Gets the CreateHeadCoach property.
        /// </summary>
        public static CreateHeadCoachViewModel CreateHeadCoachStatic
        {
            get
            {
                //if (_CreateHeadCoachViewModel == null)
                //{
                //    CreateCreateHeadCoach();
                //}

                return _CreateHeadCoachViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the CreateHeadCoach property.
        /// </summary>
        public static void CreateCreateHeadCoach()
        {
            if (_CreateHeadCoachViewModel == null)
            {
                _CreateHeadCoachViewModel = new CreateHeadCoachViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the CreateHeadCoach property.
        /// </summary>
        public static void ClearCreateHeadCoach()
        {
            if (_CreateHeadCoachViewModel != null)
            {
                _CreateHeadCoachViewModel.Cleanup();
                _CreateHeadCoachViewModel = null;
            }
        }

        #endregion CreateHeadCoachViewModel

        #region CreateGameViewModel

        private static CreateGameViewModel _CreateGameViewModel;
        /// <summary>
        /// Gets the CreateGame property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public CreateGameViewModel CreateGame
        {
            get
            {
                return CreateGameStatic;
            }
        }

        /// <summary>
        /// Gets the CreateGame property.
        /// </summary>
        public static CreateGameViewModel CreateGameStatic
        {
            get
            {
                //if (_CreateGameViewModel == null)
                //{
                //    CreateCreateGame();
                //}

                return _CreateGameViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the CreateGame property.
        /// </summary>
        public static void CreateCreateGame()
        {
            if (_CreateGameViewModel == null)
            {
                _CreateGameViewModel = new CreateGameViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the CreateGame property.
        /// </summary>
        public static void ClearCreateGame()
        {
            if (_CreateGameViewModel != null)
            {
                _CreateGameViewModel.Cleanup();
                _CreateGameViewModel = null;
            }
        }

        #endregion CreateGameViewModel

        #region OwnerViewModel

        private static OwnerViewModel _OwnerViewModel;
        /// <summary>
        /// Gets the Owner property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public OwnerViewModel Owner
        {
            get
            {
                return OwnerStatic;
            }
        }

        /// <summary>
        /// Gets the Owner property.
        /// </summary>
        public static OwnerViewModel OwnerStatic
        {
            get
            {
                //if (_OwnerViewModel == null)
                //{
                //    CreateHeadCoach();
                //}

                return _OwnerViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Owner property.
        /// </summary>
        public static void CreateOwnerViewModel()
        {
            if (_OwnerViewModel == null)
            {
                _OwnerViewModel = new OwnerViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Owner property.
        /// </summary>
        public static void ClearOwner()
        {
            if (_OwnerViewModel != null)
            {
                _OwnerViewModel.Cleanup();
                _OwnerViewModel = null;
            }
        }

        #endregion OwnerViewModel

        #region GameMenuViewModel

        private static GameMenuViewModel _GameMenuViewModel;
        /// <summary>
        /// Gets the GameMenu property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public GameMenuViewModel GameMenu
        {
            get
            {
                return GameMenuStatic;
            }
        }

        /// <summary>
        /// Gets the GameMenu property.
        /// </summary>
        public static GameMenuViewModel GameMenuStatic
        {
            get
            {
                //if (_GameMenuViewModel == null)
                //{
                //    CreateGameMenu();
                //}

                return _GameMenuViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the GameMenu property.
        /// </summary>
        public static void CreateGameMenuViewModel()
        {
            if (_GameMenuViewModel == null)
            {
                _GameMenuViewModel = new GameMenuViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the GameMenu property.
        /// </summary>
        public static void ClearGameMenu()
        {
            if (_GameMenuViewModel != null)
            {
                _GameMenuViewModel.Cleanup();
                _GameMenuViewModel = null;
            }
        }

        #endregion GameMenuViewModel

        #region CharactersViewModel

        private static CharactersViewModel _CharactersViewModel;
        /// <summary>
        /// Gets the Characters property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public CharactersViewModel Characters
        {
            get
            {
                return CharactersStatic;
            }
        }

        /// <summary>
        /// Gets the Characters property.
        /// </summary>
        public static CharactersViewModel CharactersStatic
        {
            get
            {
                //if (_CharactersViewModel == null)
                //{
                //    CreateCharacters();
                //}

                return _CharactersViewModel;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Characters property.
        /// </summary>
        public static void CreateCharactersViewModel()
        {
            if (_CharactersViewModel == null)
            {
                _CharactersViewModel = new CharactersViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Characters property.
        /// </summary>
        public static void ClearCharacters()
        {
            if (_CharactersViewModel != null)
            {
                _CharactersViewModel.Cleanup();
                _CharactersViewModel = null;
            }
        }

        #endregion CharactersViewModel

        /// <summary>
        /// Create all the view models
        /// </summary>
        private void CreateViewModels()
        {
            // Cleanup in case this is not the first time we are creating the view models
            Cleanup();

            CreateDrafting();
            CreateInbox();
            CreateMain();
            CreateMenu();
            CreatePeople();
            CreateTeamManagement();
            CreateBuyTeam();
            CreateCreateHeadCoach();
            CreateCreateGame();
            CreateOwnerViewModel();
            CreateGameMenuViewModel();
            CreateCharactersViewModel();
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
            ClearDrafting();
            ClearInbox();
            ClearMain();
            ClearMenu();
            ClearPeople();
            ClearTeamManagement();
            ClearBuyTeam();
            ClearCreateHeadCoach();
            ClearCreateGame();
            ClearOwner();
            ClearGameMenu();
            ClearCharacters();
        }
    }
}
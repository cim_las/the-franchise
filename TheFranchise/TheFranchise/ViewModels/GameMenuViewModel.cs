﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Random;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class GameMenuViewModel : ViewModelBase, IPersistToDb
    {
        #region Properties

        private string _gameYear;
        public string GameYear
        {
            get { return _gameYear; }
            set
            {
                _gameYear = value;
                RaisePropertyChanged("GameYear");
            }
        }

        private string _record;
        public string Record
        {
            get { return _record; }
            set
            {
                _record = value;
                RaisePropertyChanged("Record");
            }
        }

        private bool _draftIsEnabled;
        public bool DraftIsEnabled
        {
            get { return _draftIsEnabled; }
            set
            {
                _draftIsEnabled = value;
                RaisePropertyChanged("DraftIsEnabled");
            }
        }

        #endregion Properties


        #region CharactersCommand

        private RelayCommand<bool?> _charactersCommand;
        public ICommand CharactersCommand
        {
            get { return _charactersCommand ?? (_charactersCommand = new RelayCommand<bool?>(p => this.Characters())); }
        }

        private CharactersView _charactersView;
        private void Characters()
        {
            if (_charactersView != null)
            {
                _charactersView.Close();
                _charactersView = null;
            }

            _charactersView = new CharactersView();
            _charactersView.DataContext = ViewModelLocator.CharactersStatic;
            ViewModelLocator.CharactersStatic.Load();

            _charactersView.Show();
        }

        #endregion CharactersCommand

        #region PlayGameCommand

        private RelayCommand<bool?> _playGameCommand;
        public ICommand PlayGameCommand
        {
            get { return _playGameCommand ?? (_playGameCommand = new RelayCommand<bool?>(p => this.PlayGame())); }
        }

        private MatchView _matchView;
        private void PlayGame()
        {
            if (_matchView != null)
            {
                _matchView.Close();
                _matchView = null;
            }

            _matchView = new MatchView();
            //TODO: Pull the teams from the schedule, currently human team is always set as home team
            var id = RandomHelper.Instance.Next(31);
            _matchView.DataContext = new MatchViewModel(DbAccessor.ReadTeams(p => p.IsHuman).FirstOrDefault(), DbAccessor.ReadTeams(p => p.Id == id).FirstOrDefault());

            _matchView.Show();
        }

        #endregion PlayGameCommand

        #region DraftCommand

        private RelayCommand<bool?> _draftCommand;
        public ICommand DraftCommand
        {
            get { return _draftCommand ?? (_draftCommand = new RelayCommand<bool?>(p => this.Draft())); }
        }

        private void Draft()
        {

        }

        #endregion DraftCommand

        #region IPersistToDbImplementation

        public void Load()
        {

        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

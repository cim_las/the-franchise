﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using log4net;
using PresentSoftware.Common.Controls.ProgressDialog;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class LootViewModel : ViewModelBase, IPersistToDb
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties

        private ObservableCollection<Loot> _phatLoot;
        public ObservableCollection<Loot> PhatLoot
        {
            get
            {
                return _phatLoot;
            }
            set
            {
                _phatLoot = value;
                RaisePropertyChanged("PhatLoot");
            }
        }

        private string _Result;
        public string Result
        {
            get
            {
                return _Result;
            }
            set
            {
                _Result = value;
                RaisePropertyChanged("Result");
            }
        }

        #endregion

        public void SetResultTitle(string pTitle)
        {
            Result = pTitle;
        }
        
        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}
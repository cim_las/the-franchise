﻿using System.Collections.ObjectModel;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using ViewModelBase = GalaSoft.MvvmLight.ViewModelBase;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class TeamManagementViewModel : ViewModelBase, IPersistToDb
    {
        private ObservableCollection<Team> _teams;
        public ObservableCollection<Team> Teams
        {
            get
            {
                return _teams ?? (_teams = new ObservableCollection<Team>(DbAccessor.ReadTeams()));
            }
            set
            {
                _teams = value;
                RaisePropertyChanged("Teams");
            }
        }

        private Team _selectedTeam;
        public Team SelectedTeam
        {
            get
            {
                return _selectedTeam;// ?? (_selectedTeam = new TeamViewModel());
            }
            set
            {
                _selectedTeam = value;
                RaisePropertyChanged("SelectedTeam");
            }
        }

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

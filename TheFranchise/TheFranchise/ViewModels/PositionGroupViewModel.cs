﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class PositionGroupViewModel : ViewModelBase
    {
        public PositionGroupViewModel(CharacterViewModel pCharacterViewModel)
        {
            _myCharacterViewModel = pCharacterViewModel;
            GroupVisibility = Visibility.Visible;
        }

        private readonly CharacterViewModel _myCharacterViewModel;

        public bool Modified { get; set; }

        public PositionEnum Position { get; set; }

        public Visibility GroupVisibility { get; set; }

        private bool _moveIsEnabled;
        public bool MoveIsEnabled
        {
            get { return _moveIsEnabled; }
            set
            {
                _moveIsEnabled = value;
                RaisePropertyChanged("MoveIsEnabled");
            }
        }

        private ObservableCollection<PersonViewModel> _group;
        public ObservableCollection<PersonViewModel> Group
        {
            get { return _group; }
            set
            {
                _group = value;
                RaisePropertyChanged("Group");
            }
        }

        private PersonViewModel _selectedPerson;
        public PersonViewModel SelectedPerson
        {
            get
            {
                return _selectedPerson;
            }
            set
            {
                _selectedPerson = value;
                RaisePropertyChanged("SelectedPerson");

                MoveIsEnabled = true;
                _myCharacterViewModel.SelectedPerson = value;
            }
        }
    }
}

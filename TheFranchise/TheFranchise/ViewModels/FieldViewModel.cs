﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class FieldViewModel : ViewModelBase
    {
        public FieldViewModel(List<Person> pOffensiveRoster, List<Person> pDefensiveRoster  )
        {
            Quarterback = new PersonViewModel(pOffensiveRoster.Where(p => p.Position == PositionEnum.Quarterback).OrderBy(p => p.DepthOrder).FirstOrDefault());
            RightDefensiveTackle = new PersonViewModel(pDefensiveRoster.Where(p => p.Position == PositionEnum.DefensiveLineman).OrderBy(p => p.DepthOrder).FirstOrDefault());
        }

        private PersonViewModel _quarterback;
        public PersonViewModel Quarterback
        {
            get { return _quarterback; }
            set
            {
                _quarterback = value;
                RaisePropertyChanged("Quarterback");
            }
        }

        private PersonViewModel _rightDefensiveTackle;
        public PersonViewModel RightDefensiveTackle
        {
            get { return _rightDefensiveTackle; }
            set
            {
                _rightDefensiveTackle = value;
                RaisePropertyChanged("RightDefensiveTackle");
            }
        }
    }
}

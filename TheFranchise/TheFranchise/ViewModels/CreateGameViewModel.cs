﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using log4net;
using PresentSoftware.Common.Controls.ProgressDialog;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class CreateGameViewModel : ViewModelBase, IPersistToDb
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties

        public string GameName { get; set; }

        public bool Success { get; set; }

        private string _errorInfo;
        public string ErrorInfo
        {
            get
            {
                return _errorInfo;
            }
            set
            {
                _errorInfo = value;
                RaisePropertyChanged("ErrorInfo");
            }
        }
        
        #endregion

        #region CreateGameCommand

        private RelayCommand<bool?> _createGameCommand;
        public ICommand CreateGameCommand
        {
            get { return _createGameCommand ?? (_createGameCommand = new RelayCommand<bool?>(p => this.CreateGame())); }
        }
        
        public void CreateGame()
        {
            if (string.IsNullOrEmpty(GameName))
            {
                ErrorInfo = "Please enter a name for this game.";
                Success = false;
            }

            ProgressDialogResult pdr = ProgressDialog.Execute(null, "Creating New Game", (Action<BackgroundWorker>)((pWorker) =>
            {
                _log.InfoFormat("Start of Create Game...");
                ProgressDialog.Report(pWorker, "Start of Create Game...");

                DbAccessor.CreateDb(string.Format("{0}({1})", GameName, DateTime.Now.ToString(Constants.FILE_TIME_STAMP_FORMAT)), GameName);

                ProgressDialog.Report(pWorker, "Creating teams...");
                TeamHelper.CreateTeams();

                PersonHelper.CreateInitialPersonsAndAddToTeam();

                ProgressDialog.Report(pWorker, "Creating new season...");
                _log.InfoFormat("Creating new season...");

                SeasonHelper.CreateNewSeason();

                ProgressDialog.Report(pWorker, "Setting up initial draft...");
                _log.InfoFormat("Setting up initial draft...");
                DraftHelper.InitialDraftSetup();

                TeamHelper.SetTeamValue();

                ProgressDialog.Report(pWorker, "End of Create Game...");
                _log.InfoFormat("End of Create Game...");
                //System.Threading.Thread.Sleep(new TimeSpan(0, 0, 1));

                Success = true;
            }), ProgressDialogSettings.WithSubLabel);
        }

        #endregion CreateGameCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}
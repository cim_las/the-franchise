﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class DraftingViewModel : ViewModelBase, IPersistToDb
    {
        #region Fields
        private Draft _Draft;
        #endregion

        #region Constructors
        #endregion

        #region Properties

        private bool _draftTabIsSelected;
        public bool DraftTabIsSelected
        {
            get { return _draftTabIsSelected; }
            set
            {
                _draftTabIsSelected = value;
                RaisePropertyChanged("DraftTabIsSelected");
            }
        }

        private bool _draftTabIsEnabled;
        public bool DraftTabIsEnabled
        {
            get { return _draftTabIsEnabled; }
            set
            {
                _draftTabIsEnabled = value;
                RaisePropertyChanged("DraftTabIsEnabled");
            }
        }

        public bool _AutoDraft;
        public bool AutoDraft 
        { 
            get
            {
                return _AutoDraft;
            }
            set
            {
                _AutoDraft = value;
                RaisePropertyChanged("AutoDraft");
            }
        }

        public int DraftId
        {
            get
            {
                return _Draft.Id;
            }
        }

        private DraftPickViewModel _currentPickViewModel;
        public DraftPickViewModel CurrentPickViewModel
        {
            get
            {
                return _currentPickViewModel;
            }
            set
            {
                _currentPickViewModel = value;
                RaisePropertyChanged("CurrentPickViewModel");
            }
        }

        public string Year
        {
            get
            {
                return _Draft.DraftDate.Year + " Draft";
            }
        }

        public DateTime DraftClass
        {
            get
            {
                return _Draft.DraftDate;
            }
            set
            {
                _Draft.DraftDate = value;
                RaisePropertyChanged("DraftClass");
            }
        }

        private Person _selectedDraftProspect;
        public Person SelectedDraftProspect
        {
            get
            {
                return _selectedDraftProspect ?? (_selectedDraftProspect = new Person());
            }
            set
            {
                _selectedDraftProspect = value;
                RaisePropertyChanged("SelectedDraftProspect");
            }
        }

        public IEnumerable<Person> DraftableProspects
        {
            get
            {
                return DbAccessor.ReadPeople().Where(p => p.IsProspect == true);
            }
        }

        public bool IsComplete
        {
            get
            {
                return _Draft.IsComplete;
            }
            set
            {
                _Draft.IsComplete = value;
                RaisePropertyChanged("IsComplete");
            }
        }
        #endregion Properties

        #region FastDraftCommand
        private RelayCommand<bool?> _fastDraftCommand;
        public ICommand FastDraftCommand
        {
            get { return _fastDraftCommand ?? (_fastDraftCommand = new RelayCommand<bool?>(p => this.FastDraft())); }
        }

        private void FastDraft()
        {
            if (AutoDraft)
            {
                Task.Factory.StartNew(() => DraftHelper.FastDraft(_Draft));
            }
        }
        #endregion FastDraftCommand
        
        #region DraftPersonCommand

        private RelayCommand<bool?> _draftPersonCommand;
        public ICommand DraftPersonCommand
        {
            get { return _draftPersonCommand ?? (_draftPersonCommand = new RelayCommand<bool?>(p => this.DraftPerson())); }
        }

        private void DraftPerson()
        {
            var nextPick = DbAccessor.ReadDraftPicks().Where(p => p.IsUsed == false && p.DraftId == _Draft.Id).OrderBy(p => p.DraftPickNumber).FirstOrDefault();

            DraftHelper.DraftPerson(nextPick,SelectedDraftProspect);
        }

        #endregion DraftPersonCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            var drafts = DbAccessor.ReadDrafts();
            var year = DbAccessor.ReadSetting().LeagueDate.Year;
            _Draft = drafts.FirstOrDefault(pDraft => pDraft.DraftDate.Year == year);
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }

    
}

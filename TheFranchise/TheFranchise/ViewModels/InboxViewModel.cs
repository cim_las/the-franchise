﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class InboxViewModel:ViewModelBase, IPersistToDb
    {
        #region Properties
        private ObservableCollection<EventViewModel> _eventViewModelCollection;
        public ObservableCollection<EventViewModel> EventViewModelCollection
        {
            get 
            {
                return _eventViewModelCollection ?? (_eventViewModelCollection = new ObservableCollection<EventViewModel>());
            }
            set
            {
                _eventViewModelCollection = value;
                RaisePropertyChanged("EventViewModelCollection");
            }
        }

        private EventViewModel _selectedEventViewModel;
        public EventViewModel SelectedEventViewModel
        {
            get
            {
                return _selectedEventViewModel;
            }
            set
            {
                _selectedEventViewModel = value;
                RaisePropertyChanged("SelectedEventViewModel");
            }
        }
        #endregion Properties

        #region CompleteCommand
        private RelayCommand<bool?> _completeCommand;
        public ICommand CompleteCommand
        {
            get { return _completeCommand ?? (_completeCommand = new RelayCommand<bool?>(p => this.Complete())); }
        }

        private void Complete()
        {
            SelectedEventViewModel.IsComplete = true;

            SeasonHelper.ProcessSeasonEvent();
        }
        #endregion CompleteCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class PersonViewModel : ViewModelBase
    {
        #region Fields

        #endregion Fields

        public PersonViewModel(Person pPerson)
        {
            if (pPerson == null)
                return;

            MyPerson = pPerson;
            _myPerson.Items = DbAccessor.ReadItems(p => p.ItemOwnerId == pPerson.Id).ToList();
            _myPerson.AvailableAbilities = DbAccessor.ReadAbilities(p => p.PersonId == pPerson.Id).Select(p=>AbilityDictionary.DictionaryOfAbilities[p.TypeOfAbility]).ToList();
        }

        //private PersonViewModel _myPersonViewModel;
        //public PersonViewModel MyPersonViewModel
        //{
        //    get { return _myPersonViewModel; }
        //    set
        //    {
        //        _myPersonViewModel = value;
        //        RaisePropertyChanged("MyPersonViewModel");
        //    }
        //}

        private Person _myPerson;

        public Person MyPerson
        {
            get { return _myPerson; }
            private set
            {
                _myPerson = value;
                RaisePropertyChanged("MyPerson");
            }
        }

        #region ViewPersonInfoCommand

        private RelayCommand<bool?> _viewPersonInfoCommand;

        public ICommand ViewPersonInfoCommand
        {
            get { return _viewPersonInfoCommand ?? (_viewPersonInfoCommand = new RelayCommand<bool?>(p => this.ViewPersonInfo())); }
        }

        private PersonView _myPersonView;

        private void ViewPersonInfo()
        {
            if (_myPersonView != null)
            {
                _myPersonView.Close();
                _myPersonView = null;
            }

            _myPersonView = new PersonView {DataContext = this};

            _myPersonView.Show();
        }

        #endregion ViewPersonInfoCommand
    }
}

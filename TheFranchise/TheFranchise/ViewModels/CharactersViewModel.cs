﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class CharactersViewModel : ViewModelBase, IPersistToDb
    {
        #region Properties

        private PersonViewModel _headCoach;
        public PersonViewModel HeadCoach
        {
            get { return _headCoach; }
            set
            {
                _headCoach = value;
                RaisePropertyChanged("HeadCoach");
            }
        }

        private PersonViewModel _offensiveCoordinator;
        public PersonViewModel OffensiveCoordinator
        {
            get { return _offensiveCoordinator; }
            set
            {
                _offensiveCoordinator = value;
                RaisePropertyChanged("OffensiveCoordinator");
            }
        }

        private PersonViewModel _defensiveCoordinator;
        public PersonViewModel DefensiveCoordinator
        {
            get { return _defensiveCoordinator; }
            set
            {
                _defensiveCoordinator = value;
                RaisePropertyChanged("DefensiveCoordinator");
            }
        }

        private PersonViewModel _generalManager;
        public PersonViewModel GeneralManager
        {
            get { return _generalManager; }
            set
            {
                _generalManager = value;
                RaisePropertyChanged("GeneralManager");
            }
        }

        #endregion Properties


        #region ViewCoordinatorCommand

        private RelayCommand<PositionEnum> _viewCoordinatorCommand;
        public ICommand ViewCoordinatorCommand
        {
            get { return _viewCoordinatorCommand ?? (_viewCoordinatorCommand = new RelayCommand<PositionEnum>(p => this.ViewCoordinator(p))); }
        }

        private CharacterView _characterView;
        private void ViewCoordinator(PositionEnum pEnum)
        {
            if (_characterView != null)
            {
                _characterView.Close();
                _characterView = null;
            }
            var viewModel = new CharacterViewModel(pEnum);
            _characterView = new CharacterView(viewModel);

            _characterView.Show();
        }

        #endregion ViewCoordinatorCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            var team = DbAccessor.ReadTeams(p => p.IsHuman).FirstOrDefault();
            OffensiveCoordinator = new PersonViewModel(DbAccessor.ReadPeople(p => p.TeamId == team.Id && p.Position == PositionEnum.OffensiveCoordinator).FirstOrDefault());
            DefensiveCoordinator = new PersonViewModel(DbAccessor.ReadPeople(p => p.TeamId == team.Id && p.Position == PositionEnum.DefensiveCoordinator).FirstOrDefault());
            HeadCoach = new PersonViewModel(DbAccessor.ReadPeople(p => p.TeamId == team.Id && p.Position == PositionEnum.HeadCoach).FirstOrDefault());
            GeneralManager = new PersonViewModel(DbAccessor.ReadPeople(p => p.TeamId == team.Id && p.Position == PositionEnum.GeneralManager).FirstOrDefault());
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

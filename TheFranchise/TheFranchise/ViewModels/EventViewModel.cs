﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using Common.Types;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class EventViewModel : ViewModelBase, IPersistToDb
    {        
        #region Constructors
        public EventViewModel()
        {
            _GameEvent = new GameEvent();
        }

        public EventViewModel(GameEvent pGameEvent, MainWindow pMainWindow)
        {
            _GameEvent = pGameEvent;

            SetViewCommand(pGameEvent, this);
        }
        #endregion Constructors

        #region Fields
        private GameEvent _GameEvent;
        #endregion Fields

        #region Properties
        public string Info
        {
            get { return _GameEvent.Info; }
        }

        public int EventType
        {
            get { return _GameEvent.EventType; }
        }

        public ICommand ViewResultsCommand { get; set; }

        public bool IsComplete
        {
            get { return _GameEvent.IsComplete; }
            set
            {
                _GameEvent.IsComplete = value;
                RaisePropertyChanged("IsComplete");
                RaisePropertyChanged("ButtonVisibility");
            }
        }

        public string EventName
        {
            get { return _GameEvent.EventName; }
        }

        public Visibility ButtonVisibility
        {
            get 
            { 
                if(IsComplete)
                {
                    return Visibility.Hidden;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
        }

        private bool _findTabIsSelected;
        public bool FindTabIsSelected
        {
            get { return _findTabIsSelected; }
            set
            {
                _findTabIsSelected = value;
                RaisePropertyChanged("FindTabIsSelected");
            }
        }

        private bool _detailTabIsSelected;
        public bool DetailTabIsSelected
        {
            get { return _detailTabIsSelected; }
            set
            {
                _detailTabIsSelected = value;
                RaisePropertyChanged("DetailTabIsSelected");
            }
        }

        //private SolidColorBrush _BackGroundColor;
        //public SolidColorBrush BackgroundColor
        //{
        //    get 
        //    { 
        //        if(IsComplete)
        //        {
        //            return Brushes.LightGray;
        //        }
        //        else
        //        {
        //            return Brushes.Cyan;
        //        }
        //    }
        //    set
        //    {
        //        _BackGroundColor = value;
        //        RaisePropertyChanged("BackgroundColor);
        //    }
        //}
        
        #endregion Properties

        public void SetViewCommand(GameEvent pGameEvent, EventViewModel pNewEventViewModel)
        {
            switch ((GameEventEnum)pGameEvent.EventType)
            {
                case (int)GameEventEnum.PlayerTestingResults:
                    pNewEventViewModel.ViewResultsCommand = ViewPlayerTestingCommand;
                    break;

                case GameEventEnum.FacilityVisits:
                    pNewEventViewModel.ViewResultsCommand = ViewFacilityVisitsCommand;
                    break;
                case GameEventEnum.EarlyFreeAgency:
                    break;
                case GameEventEnum.Draft:
                    pNewEventViewModel.ViewResultsCommand = ViewDraftCommand;
                    break;
                case GameEventEnum.LateFreeAgency:
                    break;
                case GameEventEnum.Camp:
                    break;
                case GameEventEnum.PreSeason:
                    break;
                case GameEventEnum.Season:
                    break;
                case GameEventEnum.Playoffs:
                    break;
                default:
                    break;
            }
        }

        #region ViewDraftCommand
        private RelayCommand<bool?> _viewDraftCommand;
        public ICommand ViewDraftCommand
        {
            get { return _viewDraftCommand ?? (_viewDraftCommand = new RelayCommand<bool?>(p => this.ViewDraft())); }
        }

        private void ViewDraft()
        {
            ViewModelLocator.DraftingStatic.DraftTabIsSelected = true;
            ViewModelLocator.DraftingStatic.DraftTabIsEnabled = true;
        }

        private bool CanViewDraft
        {
            get { return true; }
        }
        #endregion ViewDraftCommand

        #region ViewPlayerTestingCommand
        private RelayCommand<bool?> _ViewPlayerTestingCommand;
        public ICommand ViewPlayerTestingCommand
        {
            get
            {
                return _ViewPlayerTestingCommand ?? (_ViewPlayerTestingCommand = new RelayCommand<bool?>(p => this.ViewPlayerTesting()));
            }
        }

        private void ViewPlayerTesting()
        {
            ViewModelLocator.MainStatic.PersonTabIsSelected = true;
            FindTabIsSelected = true;
            ViewModelLocator.PeopleStatic.SelectedTeam = TeamNameEnum.None;
            if (ViewModelLocator.PeopleStatic.SelectedPosition == PositionEnum.None)
            {
                ViewModelLocator.PeopleStatic.SelectedPosition = PositionEnum.Quarterback;
            }
        }

        private bool CanViewPlayerTesting
        {
            get { return true; }
        }
        #endregion ViewPlayerTestingCommand

        #region ViewFacilityVisitsCommand
        private RelayCommand<bool?> _viewFacilityVisitsCommand;
        public ICommand ViewFacilityVisitsCommand
        {
            get { return _viewFacilityVisitsCommand ?? (_viewFacilityVisitsCommand = new RelayCommand<bool?>(o => this.ViewFacilityVisits())); }
        }

        private void ViewFacilityVisits()
        {
            ViewModelLocator.MainStatic.PersonTabIsSelected = true;
            DetailTabIsSelected = true;
            ViewModelLocator.PeopleStatic.SelectedTeam = (TeamNameEnum)DbAccessor.ReadTeams(p => p.IsHuman).First().Id;
        }

        #endregion ViewFacilityVisitsCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

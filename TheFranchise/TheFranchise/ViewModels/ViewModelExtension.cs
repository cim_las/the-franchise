﻿using System;
using System.Linq.Expressions;
using GalaSoft.MvvmLight;

namespace GridIronStars.ViewModels
{
    /// <summary>
    /// ViewModelExtension provides support for strongly-typed property changed notification
    /// </summary>

    public static class ViewModelExtension
    {
        public static void OnPropertyChanged<T, TProperty>(this T observableBase, Expression<Func<T, TProperty>> expression) where T : ViewModelBase
        {
            observableBase.OnPropertyChanged(observableBase.GetPropertyName(expression));
        }

        public static string GetPropertyName<T, TProperty>(this T owner, Expression<Func<T, TProperty>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;

            if (memberExpression == null)
            {
                var unaryExpression = expression.Body as UnaryExpression;

                if (unaryExpression == null)
                {
                    throw new NotImplementedException();
                }

                memberExpression = unaryExpression.Operand as MemberExpression;

                if (memberExpression == null)
                {
                    throw new NotImplementedException();
                }
            }

            return memberExpression.Member.Name;
        }
    }
}

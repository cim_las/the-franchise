﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class MatchInteractionViewModel : ViewModelBase
    {
        private Team _humanTeam;

        public MatchInteractionViewModel()
        {
            _humanTeam = DbAccessor.ReadTeams(p => p.IsHuman).FirstOrDefault();
        }

        private string _matchUpdates;
        public string MatchUpdates
        {
            get { return _matchUpdates; }
            set
            {
                _matchUpdates = value;
                RaisePropertyChanged("MatchUpdates");
            }
        }

        #region SubstituteCommand

        private RelayCommand<bool?> _substituteCommand;
        public ICommand SubstituteCommand
        {
            get { return _substituteCommand ?? (_substituteCommand = new RelayCommand<bool?>(p => this.Substitute())); }
        }

        private void Substitute()
        {

        }

        #endregion SubstituteCommand

        #region RunCommand

        private RelayCommand<bool?> _RunCommand;
        public ICommand RunCommand
        {
            get { return _RunCommand ?? (_RunCommand = new RelayCommand<bool?>(p => this.Run())); }
        }

        private void Run()
        {
            
        }

        #endregion RunCommand

        #region PassCommand

        private RelayCommand<bool?> _passCommand;
        public ICommand PassCommand
        {
            get { return _passCommand ?? (_passCommand = new RelayCommand<bool?>(p => this.Pass())); }
        }

        private void Pass()
        {

        }

        #endregion PassCommand

        #region PuntCommand

        private RelayCommand<bool?> _puntCommand;
        public ICommand PuntCommand
        {
            get { return _puntCommand ?? (_puntCommand = new RelayCommand<bool?>(p => this.Punt())); }
        }

        private void Punt()
        {

        }

        #endregion PuntCommand

        #region KickCommand

        private RelayCommand<bool?> _kickCommand;
        public ICommand KickCommand
        {
            get { return _kickCommand ?? (_kickCommand = new RelayCommand<bool?>(p => this.Kick())); }
        }

        private void Kick()
        {

        }

        #endregion KickCommand
    }
}

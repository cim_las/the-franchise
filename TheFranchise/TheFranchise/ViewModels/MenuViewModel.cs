﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using log4net;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class MenuViewModel : ViewModelBase, IPersistToDb
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties

        private Visibility _createGameVisibility = Visibility.Collapsed;
        public Visibility CreateGameVisibility
        {
            get
            {
                return _createGameVisibility;
            }
            set
            {
                _createGameVisibility = value;
                RaisePropertyChanged("CreateGameVisibility");
            }
        }

        private Visibility _loadGameVisibility = Visibility.Collapsed;
        public Visibility LoadGameVisibility
        {
            get
            {
                return _loadGameVisibility;
            }
            set
            {
                _loadGameVisibility = value;
                RaisePropertyChanged("LoadGameVisibility");
            }
        }
        
        #endregion Properties

        #region LoginCommand

        private RelayCommand<bool?> _loginCommand;
        public ICommand LoginCommand
        {
            get { return _loginCommand ?? (_loginCommand = new RelayCommand<bool?>(p => this.Login())); }
        }

        private void Login()
        {
            // TODO: Connect to web service and validate user

            CreateGameVisibility = Visibility.Visible;
            LoadGameVisibility = Visibility.Visible;
        }

        #endregion LoginCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

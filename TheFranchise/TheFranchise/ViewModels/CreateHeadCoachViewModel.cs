﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Content;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class CreateHeadCoachViewModel : ViewModelBase, IPersistToDb
    {
        #region Properties

        public string PersonName { get; set; }
        public bool Success { get; set; }

        private PersonViewModel _selectedPerson;
        public PersonViewModel SelectedPerson
        {
            get
            {
                return _selectedPerson;
            }
            set
            {
                _selectedPerson = value;
                RaisePropertyChanged("SelectedPerson");
            }
        }

        private string _selectedPortrait;
        public string SelectedPortrait
        {
            get { return _selectedPortrait ?? (_selectedPortrait = FaceDictionary.ListOfFacePaths[0]); }
            set
            {
                _selectedPortrait = value;
                RaisePropertyChanged("SelectedPortrait");
            }
        }

        private string _errorInfo;
        public string ErrorInfo
        {
            get
            {
                return _errorInfo;
            }
            set
            {
                _errorInfo = value;
                RaisePropertyChanged("ErrorInfo");
            }
        }

        private Item _randomItem1;
        public Item RandomItem1
        {
            get
            {
                return _randomItem1;
            }
            set
            {
                _randomItem1 = value;
                RaisePropertyChanged("RandomItem1");
            }
        }

        private Item _randomItem2;
        public Item RandomItem2
        {
            get
            {
                return _randomItem2;
            }
            set
            {
                _randomItem2 = value;
                RaisePropertyChanged("RandomItem2");
            }
        }

        private Item _randomItem3;
        public Item RandomItem3
        {
            get
            {
                return _randomItem3;
            }
            set
            {
                _randomItem3 = value;
                RaisePropertyChanged("RandomItem3");
            }
        }

        private bool _item1Checked;
        public bool Item1Checked
        {
            get { return _item1Checked; }
            set
            {
                _item1Checked = value;
                if (value)
                {
                    SelectedPerson.MyPerson.Items.Clear();
                    SelectedPerson.MyPerson.Items.Add(RandomItem1);

                    var person = SelectedPerson;
                    SelectedPerson = null;
                    SelectedPerson = person;

                    RaisePropertyChanged("Item1Checked");
                }
            }
        }

        private bool _item2Checked;
        public bool Item2Checked
        {
            get { return _item2Checked; }
            set
            {
                _item2Checked = value;
                if (value)
                {
                    SelectedPerson.MyPerson.Items.Clear();
                    SelectedPerson.MyPerson.Items.Add(RandomItem2);
                    
                    var person = SelectedPerson;
                    SelectedPerson = null;
                    SelectedPerson = person;

                    RaisePropertyChanged("Item2Checked");
                }
            }
        }

        private bool _item3Checked;
        public bool Item3Checked
        {
            get { return _item3Checked; }
            set
            {
                _item3Checked = value;
                if (value)
                {
                    SelectedPerson.MyPerson.Items.Clear();
                    SelectedPerson.MyPerson.Items.Add(RandomItem3);
                    
                    var person = SelectedPerson;
                    SelectedPerson = null;
                    SelectedPerson = person;

                    RaisePropertyChanged("Item3Checked");
                }
            }
        }

        #endregion

        #region CreateHeadCoachCommand

        private RelayCommand<bool?> _createHeadCoachCommand;
        public ICommand CreateHeadCoachCommand
        {
            get { return _createHeadCoachCommand ?? (_createHeadCoachCommand = new RelayCommand<bool?>(p => this.CreateHeadCoach())); }
        }

        public void CreateHeadCoach()
        {
            if (string.IsNullOrEmpty(PersonName))
            {
                ErrorInfo = "Please enter a name for this coach.";
                Success = false;
                return;
            }

            SelectedPerson.MyPerson.Name = PersonName;
            SelectedPerson.MyPerson.Portrait = SelectedPortrait;
            DbAccessor.SavePerson(SelectedPerson.MyPerson);

            var item = SelectedPerson.MyPerson.Items.First();
            item.ItemOwnerId = SelectedPerson.MyPerson.Id;
            DbAccessor.SaveItem(item);

            var settings = DbAccessor.ReadSetting();
            settings.HeadCoachId = SelectedPerson.MyPerson.Id;
            DbAccessor.SaveSettings(settings);

            Success = true;
        }

        #endregion CreateHeadCoachCommand


        #region ReRollCommand

        private RelayCommand<bool?> _ReRollCommand;
        public ICommand ReRollCommand
        {
            get { return _ReRollCommand ?? (_ReRollCommand = new RelayCommand<bool?>(p => this.ReRoll())); }
        }

        public void ReRoll()
        {
            var setting = DbAccessor.ReadSetting();
            if (setting == null)
                return;

            var person = new Person() {Position = PositionEnum.HeadCoach, BodyType = BodyTypeEnum.Basic};
            PersonHelper.FillHumanHeadCoach(person, setting.LeagueDate);

            SelectedPerson = new PersonViewModel(person);

            RandomItem1 = ItemHelper.CreateRandomItemWithExclusions(ItemEnum.MegaBowlRing);
            RandomItem2 = ItemHelper.CreateRandomItemWithExclusions(ItemEnum.MegaBowlRing, RandomItem1.ItemName);
            RandomItem3 = ItemHelper.CreateRandomItemWithExclusions(ItemEnum.MegaBowlRing, RandomItem1.ItemName, RandomItem2.ItemName);

            Item1Checked = true;
        }

        #endregion ReRollCommand

        private int _portraitIndex = 0;
        #region PreviousPortraitCommand

        private RelayCommand<bool?> _previousPortraitCommand;
        public ICommand PreviousPortraitCommand
        {
            get { return _previousPortraitCommand ?? (_previousPortraitCommand = new RelayCommand<bool?>(p => this.PreviousPortrait())); }
        }

        public void PreviousPortrait()
        {
            _portraitIndex = Math.Max(0, --_portraitIndex);
            SelectedPortrait = FaceDictionary.ListOfFacePaths[_portraitIndex];
        }

        #endregion PreviousPortraitCommand

        #region NextPortraitCommand

        private RelayCommand<bool?> _nextPortraitCommand;
        public ICommand NextPortraitCommand
        {
            get { return _nextPortraitCommand ?? (_nextPortraitCommand = new RelayCommand<bool?>(p => this.NextPortrait())); }
        }

        public void NextPortrait()
        {
            _portraitIndex = Math.Min(FaceDictionary.ListOfFacePaths.Count - 1, ++_portraitIndex);
            SelectedPortrait = FaceDictionary.ListOfFacePaths[_portraitIndex];
        }

        #endregion NextPortraitCommand
        

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}
﻿using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using log4net;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.TheFranchise.DesktopUI.Controls;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class MainViewModel : ViewModelBase, IPersistToDb
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties

        private string _titleInfo;
        public string TitleInfo
        {
            get
            {
                if (string.IsNullOrEmpty(_titleInfo))
                {
                    var settings = DbAccessor.ReadSetting();

                    _titleInfo = string.Format("Owner:\t{0}Team:\t{1}", DbAccessor.ReadPeople().First(p=> p.Id == settings.HeadCoachId).Name, DbAccessor.ReadTeams(p => p.IsHuman).First().TeamName);
                }
                return _titleInfo;
            }
        }

        private bool _inboxTabIsSelected;
        public bool InboxTabIsSelected
        {
            get { return _inboxTabIsSelected; }
            set 
            { 
                _inboxTabIsSelected = value;
                RaisePropertyChanged("InboxTabIsSelected");
            }
        }

        private bool _personTabIsSelected;
        public bool PersonTabIsSelected
        {
            get { return _personTabIsSelected; }
            set
            {
                _personTabIsSelected = value;
                RaisePropertyChanged("PersonTabIsSelected");
            }
        }

        private bool _teamTabIsSelected;
        public bool TeamTabIsSelected
        {
            get { return _teamTabIsSelected; }
            set
            {
                _teamTabIsSelected = value;
                RaisePropertyChanged("TeamTabIsSelected");
            }
        }

        private bool _seasonTabIsSelected;
        public bool SeasonTabIsSelected
        {
            get { return _seasonTabIsSelected; }
            set
            {
                _seasonTabIsSelected = value;
                RaisePropertyChanged("SeasonTabIsSelected");
            }
        }

        #endregion Properties
        
        public override void Cleanup()
        {
            //throw new NotImplementedException();
        }

        #region ViewOwnerCommand

        private RelayCommand<bool?> _ViewOwnerCommand;
        public ICommand ViewOwnerCommand
        {
            get { return _ViewOwnerCommand ?? (_ViewOwnerCommand = new RelayCommand<bool?>(p => this.ViewOwner())); }
        }

        private OwnerView _view;
        private void ViewOwner()
        {
            if (_view != null)
            {
                _view.Close();
                _view = null;
            }

            _view = new OwnerView();

            _view.Show();   
        }

        #endregion ViewOwnerCommand

        #region ViewGameMenuCommand

        private RelayCommand<bool?> _viewGameMenuCommand;
        public ICommand ViewGameMenuCommand
        {
            get { return _viewGameMenuCommand ?? (_viewGameMenuCommand = new RelayCommand<bool?>(p => this.ViewGameMenu())); }
        }

        private GameMenuView _gameMenuView;
        private void ViewGameMenu()
        {
            if (_gameMenuView != null)
            {
                _gameMenuView.Close();
                _gameMenuView = null;
            }

            _gameMenuView = new GameMenuView();
            _gameMenuView.DataContext = ViewModelLocator.GameMenuStatic;

            _gameMenuView.Show();
        }

        #endregion ViewGameMenuCommand

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}

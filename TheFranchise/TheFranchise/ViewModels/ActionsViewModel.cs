﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Factories;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class ActionsViewModel:ViewModelBase
    {
        #region Constructors

        public ActionsViewModel(CharacterViewModel pModel)
        {
            MyCharacterViewModel = pModel;
        }

        #endregion

        #region Properties

        private CharacterViewModel _myCharacterViewModel;
        public CharacterViewModel MyCharacterViewModel
        {
            get
            {
                return _myCharacterViewModel;
            }
            set
            {
                _myCharacterViewModel = value;
                RaisePropertyChanged("MyCharacterViewModel");
            }
        }

        private ActionDefinition _selectedAction;
        public ActionDefinition SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                _selectedAction = value;
                RaisePropertyChanged("SelectedAction");
            }
        }

        private int _selectedActionIndex;
        public int SelectedActionIndex
        {
            get { return _selectedActionIndex; }
            set
            {
                _selectedActionIndex = value;
                RaisePropertyChanged("SelectedActionIndex");
            }
        }

        #endregion

        #region ProcessActionCommand

        private RelayCommand<bool?> _processActionCommand;
        public ICommand ProcessActionCommand
        {
            get { return _processActionCommand ?? (_processActionCommand = new RelayCommand<bool?>(p => this.ProcessAction())); }
        }

        private void ProcessAction()
        {
            if (SelectedAction == null || MyCharacterViewModel.SelectedPerson == null)
                return;

            string resultTitle = string.Empty;
            var l = new LootViewModel();
            var generalLoot = ActionHelper.ProcessAction(MyCharacterViewModel.Character.MyPerson, MyCharacterViewModel.SelectedPerson.MyPerson, SelectedAction, ref resultTitle);
            l.PhatLoot = new ObservableCollection<Loot>(generalLoot);
            l.SetResultTitle(resultTitle);
            var w = new LootWindow { DataContext = l };
            w.ShowDialog();

            var ownerLevelLoot = LevelGainedHelper.ProcessLevelGained(MyCharacterViewModel.Character.MyPerson, true);
            if (ownerLevelLoot != null)
            {
                l.PhatLoot = new ObservableCollection<Loot>(ownerLevelLoot);
                l.SetResultTitle("You gained a level !");
                var levelWindow = new LootWindow { DataContext = l };
                levelWindow.ShowDialog();
            }

            var targetLevelLoot = LevelGainedHelper.ProcessLevelGained(MyCharacterViewModel.SelectedPerson.MyPerson, false);
            if (targetLevelLoot != null)
            {
                l.PhatLoot = new ObservableCollection<Loot>(targetLevelLoot);
                l.SetResultTitle("Level up !");
                var targetWindow = new LootWindow { DataContext = l };
                targetWindow.ShowDialog();
            }

            Refresh();
        }

        #endregion ProcessActionCommand

        #region MoreAPCommand

        private RelayCommand<bool?> _moreApCommand;
        public ICommand MoreApCommand
        {
            get { return _moreApCommand ?? (_moreApCommand = new RelayCommand<bool?>(p => this.MoreAP())); }
        }

        private void MoreAP()
        {
            MyCharacterViewModel.Character.MyPerson.ActionPoints += 30;
            DbAccessor.SavePerson(MyCharacterViewModel.Character.MyPerson);
            Refresh();
        }

        #endregion MoreAPCommand

        public void Refresh()
        {
            // Force a refresh on character
            var id = MyCharacterViewModel.Character.MyPerson.Id;

            var person = DbAccessor.ReadPeople(p => p.Id == id).FirstOrDefault();
            ActionFactory.FactoryActions(person, MyCharacterViewModel.SelectedPerson.MyPerson);

            MyCharacterViewModel.Character = new PersonViewModel(person);
        }

        public void Initialize(PersonViewModel pCharacter, PersonViewModel pSelectedPerson)
        {
            //MyCharacterViewModel.SelectedPerson = pSelectedPerson;

            ActionFactory.FactoryActions(pCharacter.MyPerson, pSelectedPerson.MyPerson);

            // Refresh
            MyCharacterViewModel.Character = pCharacter;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using log4net;
using PresentSoftware.Common.Controls.ProgressDialog;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Factories;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class OwnerViewModel : ViewModelBase, IPersistToDb
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties

        private Person _owner;
        public Person Owner
        {
            get
            {
                if (_owner == null)
                {
                    Owner = InitializeOwner();
                }

                return _owner;
            }
            set
            {
                _owner = value;
                RaisePropertyChanged("Owner");
            }
        }


        private Visibility _actionsVisibility;
        public Visibility ActionsVisibility
        {
            get { return _actionsVisibility; }
            set
            {
                _actionsVisibility = value;
                RaisePropertyChanged("ActionsVisibility");
            }
        }

        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set
            {
                _selectedPerson = value;
                ActionsVisibility = _selectedPerson == null ? Visibility.Collapsed : Visibility.Visible;

                RaisePropertyChanged("SelectedPerson");
            }
        }

        private ActionDefinition _selectedAction;
        public ActionDefinition SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                _selectedAction = value;
                RaisePropertyChanged("SelectedAction");
            }
        }

        private int _selectedActionIndex;
        public int SelectedActionIndex
        {
            get { return _selectedActionIndex; }
            set
            {
                _selectedActionIndex = value;
                RaisePropertyChanged("SelectedActionIndex");
            }
        }

        #endregion

        private Person InitializeOwner()
        {
            var id = DbAccessor.ReadSetting().HeadCoachId;
            var person = DbAccessor.ReadPeople(p => p.Id == id).FirstOrDefault();
            person.Items = DbAccessor.ReadItems(p => p.ItemOwnerId == person.Id).ToList();
            ActionFactory.FactoryActions(person, SelectedPerson);

            return person;
        }

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            throw new System.NotImplementedException();
        }

        #endregion IPerssistToDbImplementation
    }
}
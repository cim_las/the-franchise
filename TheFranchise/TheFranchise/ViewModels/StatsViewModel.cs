﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;

namespace GridIronStars.ViewModels
{
    public class StatsViewModel : ViewModelBase, IPersistToDb
    {
        #region Constructors
        public StatsViewModel()
        {
            _OffensiveStat = new OffensiveStat();
        }

        public StatsViewModel(OffensiveStat pOffensiveStat)
        {
            _OffensiveStat = pOffensiveStat;
        }
        #endregion Constructors

        #region Fields
        private OffensiveStat _OffensiveStat;
        #endregion Fields

        #region Properties
        #endregion
        public bool IsCollegeStat
        {
            get
            {
                return _OffensiveStat.IsCollegeStat;
            }
        }

        public int Pancakes
        {
            get
            {
                return _OffensiveStat.Pancakes;
            }
        }

        public int RushingAttempts
        {
            get
            {
                return _OffensiveStat.RushingAttempts;
            }
        }

        public int RushYards
        {
            get
            {
                return _OffensiveStat.RushYards;
            }
        }

        public int RushingTouchdowns
        {
            get
            {
                return _OffensiveStat.RushingTouchdowns;
            }
        }

        public int Receptions
        {
            get
            {
                return _OffensiveStat.Receptions;
            }
        }

        public int ReceivingAttempts
        {
            get
            {
                return _OffensiveStat.ReceivingAttempts;
            }
        }

        public int ReceivingYards
        {
            get
            {
                return _OffensiveStat.ReceivingYards;
            }
        }

        public int ReceivingTouchdowns
        {
            get
            {
                return _OffensiveStat.ReceivingTouchdowns;
            }
        }

        public int PassYards
        {
            get
            {
                return _OffensiveStat.PassYards;
            }
        }

        public int PassCompletions
        {
            get
            {
                return _OffensiveStat.PassCompletions;
            }
        }

        public int PassAttempts
        {
            get
            {
                return _OffensiveStat.PassAttempts;
            }
        }

        public int PassingTouchdowns
        {
            get
            {
                return _OffensiveStat.PassingTouchdowns;
            }
        }

        public int FieldGoalAttempts
        {
            get
            {
                return _OffensiveStat.FieldGoalAttempts;
            }
        }

        public int FieldGoalsMade
        {
            get
            {
                return _OffensiveStat.FieldGoalsMade;
            }
        }

        #region IPersistToDbImplementation

        public void Load()
        {
            
        }

        public void Save()
        {
            
        }

        #endregion IPerssistToDbImplementation
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Extensions;
using PresentSoftware.Common.Helpers.Factories;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.ViewModels
{
    public class CharacterViewModel : ViewModelBase, IPersistToDb
    {
        #region Fields

        private readonly PositionEnum _characterPosition;

        #endregion Fields

        #region Constructors

        public CharacterViewModel(PositionEnum pEnum)
        {
            _characterPosition = pEnum;

            PositionGroup1 = new PositionGroupViewModel(this);
            PositionGroup2 = new PositionGroupViewModel(this);
            PositionGroup3 = new PositionGroupViewModel(this);
            PositionGroup4 = new PositionGroupViewModel(this);
            PositionGroup5 = new PositionGroupViewModel(this);
            MyActionsViewModel = new ActionsViewModel(this);

            Load();
        }

        #endregion Constructors

        #region Properties

        private PersonViewModel _character;
        public PersonViewModel Character
        {
            get { return _character; }
            set
            {
                _character = value;
                RaisePropertyChanged("Character");
            }
        }

        private PersonViewModel _selectedPerson;
        public PersonViewModel SelectedPerson
        {
            get { return _selectedPerson; }
            set
            {
                if (_selectedPerson == value)
                    return;

                _selectedPerson = value;
                RaisePropertyChanged("SelectedPerson");

                if(Character != null && SelectedPerson != null)
                    MyActionsViewModel.Initialize(Character, SelectedPerson);
            }
        }

        public PositionGroupViewModel PositionGroup1 { get; set; }
        public PositionGroupViewModel PositionGroup2 { get; set; }
        public PositionGroupViewModel PositionGroup3 { get; set; }
        public PositionGroupViewModel PositionGroup4 { get; set; }
        public PositionGroupViewModel PositionGroup5 { get; set; }
        public ActionsViewModel MyActionsViewModel { get; set; }

        #endregion Properties

        #region MoveUpCommand

        private RelayCommand<PositionEnum> _moveUpCommand;
        public ICommand MoveUpCommand
        {
            get { return _moveUpCommand ?? (_moveUpCommand = new RelayCommand<PositionEnum>(p => this.MoveUp(p))); }
        }

        private void MoveUp(PositionEnum pEnum)
        {
            Move(-1, pEnum);
        }

        #endregion MoveUpCommand

        #region MoveDownCommand

        private RelayCommand<PositionEnum> _moveDownCommand;
        public ICommand MoveDownCommand
        {
            get { return _moveDownCommand ?? (_moveDownCommand = new RelayCommand<PositionEnum>(p => this.MoveDown(p))); }
        }

        private void MoveDown(PositionEnum pEnum)
        {
            Move(1, pEnum);
        }

        #endregion MoveDownCommand

        private void Move(int pDirection, PositionEnum pEnum)
        {
            if (PositionGroup1.Position == pEnum)
            {
                MoveItem(pDirection, PositionGroup1);
                PositionGroup1.Group = new ObservableCollection<PersonViewModel>(PositionGroup1.Group.OrderBy(p => p.MyPerson.DepthOrder));
                PositionGroup1.Modified = true;
            }
            else if (PositionGroup2.Position == pEnum)
            {
                MoveItem(pDirection, PositionGroup2);
                PositionGroup2.Group = new ObservableCollection<PersonViewModel>(PositionGroup2.Group.OrderBy(p => p.MyPerson.DepthOrder));
                PositionGroup2.Modified = true;
            }
            else if (PositionGroup3.Position == pEnum)
            {
                MoveItem(pDirection, PositionGroup3);
                PositionGroup3.Group = new ObservableCollection<PersonViewModel>(PositionGroup3.Group.OrderBy(p => p.MyPerson.DepthOrder));
                PositionGroup3.Modified = true;
            }
            else if (PositionGroup4.Position == pEnum)
            {
                MoveItem(pDirection, PositionGroup4);
                PositionGroup4.Group = new ObservableCollection<PersonViewModel>(PositionGroup4.Group.OrderBy(p => p.MyPerson.DepthOrder));
                PositionGroup4.Modified = true;
            }
            else if (PositionGroup5.Position == pEnum)
            {
                MoveItem(pDirection, PositionGroup5);
                PositionGroup5.Group = new ObservableCollection<PersonViewModel>(PositionGroup5.Group.OrderBy(p => p.MyPerson.DepthOrder));
                PositionGroup5.Modified = true;
            }
        }

        public void MoveItem(int pDirection, PositionGroupViewModel pPositionGroupViewModel )
        {
            // Checking selected item
            if (pPositionGroupViewModel.SelectedPerson == null)
                return; // No selected

            // Calculate new index using move direction
            int newIndex = pPositionGroupViewModel.SelectedPerson.MyPerson.DepthOrder + pDirection;

            // Checking bounds of the range
            if (newIndex < 1 || newIndex > pPositionGroupViewModel.Group.Count)
                return; // Index out of range - nothing to do

            var personToFlipWith = pPositionGroupViewModel.Group.FirstOrDefault(p => p.MyPerson.DepthOrder == newIndex);

            if (personToFlipWith != null)
                personToFlipWith.MyPerson.DepthOrder = pPositionGroupViewModel.SelectedPerson.MyPerson.DepthOrder;

            pPositionGroupViewModel.SelectedPerson.MyPerson.DepthOrder = newIndex;
        }

        #region IPersistToDbImplementation

        public void Load()
        {
            var team = DbAccessor.ReadTeams(p => p.IsHuman).FirstOrDefault();
            var roster = DbAccessor.ReadPeople(p => p.TeamId == team.Id).Select(p => new PersonViewModel(p)).ToList();
            Character = roster.FirstOrDefault(p => p.MyPerson.Position == _characterPosition);

            if (Character == null)
                return;

            SetRosterDepthOrder(roster);

            switch (_characterPosition)
            {
                case PositionEnum.HeadCoach:
                    {
                        break;
                    }
                case PositionEnum.GeneralManager:
                    {
                        PositionGroup1.Position = PositionEnum.Scout;
                        PositionGroup1.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.Scout).OrderBy(p => p.MyPerson.DepthOrder));
                        PositionGroup2.GroupVisibility = Visibility.Collapsed;
                        PositionGroup3.GroupVisibility = Visibility.Collapsed;
                        PositionGroup4.GroupVisibility = Visibility.Collapsed;
                        PositionGroup5.GroupVisibility = Visibility.Collapsed;
                        break;
                    }
                case PositionEnum.OffensiveCoordinator:
                {
                    PositionGroup1.Position = PositionEnum.Quarterback;
                    PositionGroup2.Position = PositionEnum.HalfBack;
                    PositionGroup3.Position = PositionEnum.TightEnd;
                    PositionGroup4.Position = PositionEnum.WideReceiver;
                    PositionGroup5.Position = PositionEnum.OffensiveLineman;
                    PositionGroup1.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.Quarterback).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup2.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.HalfBack).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup3.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.TightEnd).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup4.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.WideReceiver).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup5.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.OffensiveLineman).OrderBy(p => p.MyPerson.DepthOrder));
                    break;
                }
                case PositionEnum.DefensiveCoordinator:
                {
                    PositionGroup1.Position = PositionEnum.Defensiveback;
                    PositionGroup2.Position = PositionEnum.Linebacker;
                    PositionGroup3.Position = PositionEnum.DefensiveLineman;
                    PositionGroup1.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.Defensiveback).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup2.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.Linebacker).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup3.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.DefensiveLineman).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup4.GroupVisibility = Visibility.Collapsed;
                    PositionGroup5.GroupVisibility = Visibility.Collapsed;
                    break;
                }
                case PositionEnum.SpecialTeamsCoordinator:
                {
                    PositionGroup1.Position = PositionEnum.Kicker;
                    PositionGroup2.Position = PositionEnum.Punter;
                    PositionGroup1.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.Kicker).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup2.Group = new ObservableCollection<PersonViewModel>(roster.Where(p => p.MyPerson.Position == PositionEnum.Punter).OrderBy(p => p.MyPerson.DepthOrder));
                    PositionGroup3.GroupVisibility = Visibility.Collapsed;
                    PositionGroup4.GroupVisibility = Visibility.Collapsed;
                    PositionGroup5.GroupVisibility = Visibility.Collapsed;
                    break;
                }
            }
        }

        /// <summary>
        /// Set Depth order for players that have never been set
        /// </summary>
        /// <param name="pRoster"></param>
        private void SetRosterDepthOrder(List<PersonViewModel> pRoster)
        {
            foreach (var group in pRoster.GroupBy(p=>p.MyPerson.Position))
            {
                var maxDepthOrder = group.Max(p => p.MyPerson.DepthOrder);
                foreach (var member in group)
                {
                    if (member.MyPerson.DepthOrder <= 0)
                        member.MyPerson.DepthOrder = ++maxDepthOrder;
                }
            }
        }

        public void Save(object pSender, CancelEventArgs pE)
        {
            if (PositionGroup1.Modified)
            {
                foreach (var personViewModel in PositionGroup1.Group)
                {
                    DbAccessor.SavePerson(personViewModel.MyPerson);
                }
            }
            if (PositionGroup2.Modified)
            {
                foreach (var personViewModel in PositionGroup2.Group)
                {
                    DbAccessor.SavePerson(personViewModel.MyPerson);
                }
            }
            if (PositionGroup3.Modified)
            {
                foreach (var personViewModel in PositionGroup3.Group)
                {
                    DbAccessor.SavePerson(personViewModel.MyPerson);
                }
            }
            if (PositionGroup4.Modified)
            {
                foreach (var personViewModel in PositionGroup4.Group)
                {
                    DbAccessor.SavePerson(personViewModel.MyPerson);
                }
            }
            if (PositionGroup5.Modified)
            {
                foreach (var personViewModel in PositionGroup5.Group)
                {
                    DbAccessor.SavePerson(personViewModel.MyPerson);
                }
            }
        }

        #endregion IPerssistToDbImplementation
    }
}
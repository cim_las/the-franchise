﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.TheFranchise.DesktopUI.ViewModels;
using PresentSoftware.TheFranchise.DesktopUI.Views;

namespace PresentSoftware.TheFranchise.DesktopUI.Controls
{
    /// <summary>
    /// Interaction logic for PeopleControl.xaml
    /// </summary>
    public partial class PeopleControl : UserControl
    {
        public PeopleControl()
        {
            InitializeComponent();

            uiScaleSlider.MouseDoubleClick += RestoreScalingFactor;
        }

        /// <summary>
        /// Reverts the scaling factor to 1. 
        /// </summary>
        /// <param name="sender">The Slider object which generated the event</param>
        /// <param name="args"></param>
        void RestoreScalingFactor(object sender, MouseButtonEventArgs args)
        {
            ((Slider)sender).Value = 1.0;
        }

        /// <summary>
        /// The user can scale up/down the UI by using the mouse wheel while holding down
        /// the Ctrl key.
        /// </summary>
        protected override void OnPreviewMouseWheel(MouseWheelEventArgs args)
        {
            base.OnPreviewMouseWheel(args);
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                uiScaleSlider.Value += (args.Delta > 0) ? 0.1 : -0.1;
            }
        }

        /// <summary>
        /// Reverts the scaling factor to 1, when the user presses the mouse wheel while 
        /// holding down the Ctrl key.
        /// </summary>
        protected override void OnPreviewMouseDown(MouseButtonEventArgs args)
        {
            base.OnPreviewMouseDown(args);
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                if (args.MiddleButton == MouseButtonState.Pressed)
                {
                    RestoreScalingFactor(uiScaleSlider, args);
                }
            }
        }

        private void ButtonBase_OnClick(object pSender, RoutedEventArgs pE)
        {
            ViewModelLocator.PeopleStatic.SelectedPosition = PositionEnum.None;
        }
    }
}

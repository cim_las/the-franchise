﻿//
// Parago Media GmbH & Co. KG, Jürgen Bäurle (jbaurle@parago.de)
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.
//

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("ProgressDialog")]
[assembly: AssemblyCompany("Wave Software")]
[assembly: AssemblyCopyright("Copyright © Wave Software 2012")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("7.2.71.3670")]
[assembly: AssemblyFileVersion("7.2.71.3670")]

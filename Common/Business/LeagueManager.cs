﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.Common.Business
{
    public class LeagueManager
    {
        #region Constructors

        public LeagueManager()
        {
            Initialize();
        }

        public static void Initialize()
        {
            _leagueDate = null;
        }

        #endregion Constructors

        #region Properties 

        private static DateTime? _leagueDate;
        public static DateTime? LeagueDate 
        {
            get { return _leagueDate ?? (_leagueDate = DbAccessor.ReadSettings().LeagueDate); }
            set { _leagueDate = value; }
        }

        #endregion Properties

        #region Update Methods

        public void UpdateLeagueDate(double pDaysToAdd)
        {
            var settings = DbAccessor.ReadSettings();
            _leagueDate = settings.LeagueDate.AddDays(pDaysToAdd);

            DbAccessor.SaveSettings(settings);
        }

        #endregion Update Methods
    }
}

﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using PresentSoftware.Common.Data.Initializers;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.Common.Data.Context
{
    public class TheFranchiseDbContext : DbContext
    {
        //public FootballProDbContext()
            //: base(nameOrConnectionString: "CodeCamper") { }

        private const string _contextName = "TheFranchiseDbContext";
        public static string ContextName { get { return _contextName; } }
        public TheFranchiseDbContext() : base(ContextName )
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder pModelBuilder)
        {
            // Use singular table names
            //pModelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            Database.SetInitializer<TheFranchiseDbContext>(new TheFranchiseDbInitializer());

            base.OnModelCreating(pModelBuilder);

            //modelBuilder.Configurations.Add(new SessionConfiguration());
            //modelBuilder.Configurations.Add(new AttendanceConfiguration());
        }

        public DbSet<Setting> Settings { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<DefensiveStat> DefensiveStats { get; set; }
        public DbSet<Draft> Drafts { get; set; }
        public DbSet<DraftPick> DraftPicks { get; set; }
        public DbSet<GameEvent> GameEvents { get; set; }
        public DbSet<Injury> Injurys { get; set; }
        public DbSet<OffensiveStat> OffensiveStats { get; set; }
        public DbSet<Play> Plays { get; set; }
        public DbSet<PlayBook> PlayBooks { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamAssessment> TeamAssessments { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<RosterSpot> RosterSpots { get; set; }
        public DbSet<Ability> Abilities { get; set; }
    }
}
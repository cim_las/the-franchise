﻿
namespace PresentSoftware.Common.Data.Models
{
    public class Play
    {
        public int PlayID { get; set; }
        public int PlayBookID { get; set; }
        public int Weakness { get; set; }
        public int Strength { get; set; }
    }
}

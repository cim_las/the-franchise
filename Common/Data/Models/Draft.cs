﻿using System;

namespace PresentSoftware.Common.Data.Models
{
    public class Draft
    {
        public int Id { get; set; }
        public DateTime DraftDate { get; set; }
        public bool IsComplete { get; set; }
        public bool AutoDraft { get; set; }
    }
}

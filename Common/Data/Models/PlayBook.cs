﻿using System.Collections.Generic;

namespace PresentSoftware.Common.Data.Models
{
    public class PlayBook
    {
        public int PlayBookID { get; set; }
        public int TeamID { get; set; }

        public virtual ICollection<Play> Plays { get; set; }
    }
}

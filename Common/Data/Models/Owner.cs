﻿
namespace PresentSoftware.Common.Data.Models
{
    public class Owner
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public bool IsHuman { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Portrait { get; set; }

        //Mental
        public int Aggresiveness { get; set; }
        public int Determination { get; set; }
        public int Honesty { get; set; }
        public int Leadership { get; set; }
        public int UnSelfishness { get; set; }
        public int Faith { get; set; }
        public int Loyalty { get; set; }
        public int WorkEthic { get; set; }
        public int Clutch { get; set; }
        public int Competitiveness { get; set; }
        public int Skill { get; set; }

        //Items
        public int Item1 { get; set; }
        public int Item2 { get; set; }
        public int Item3 { get; set; }
    }
}

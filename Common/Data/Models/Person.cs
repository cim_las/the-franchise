﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Types;

namespace PresentSoftware.Common.Data.Models
{
    public class Person : INonPhysicalAttributes
    {
        public Person()
        {
            Portrait = string.Empty;
            Items = new List<Item>();
        }

        public int Id { get; set; }
        public int DraftPickId { get; set; }

        //Bio
        [Required, MaxLength(50)]
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
        public PositionEnum Position { get; set; }
        public string College { get; set; }
        public BodyTypeEnum BodyType { get; set; }
        public string Portrait { get; set; }
        public bool IsPlayer { get; set; }
        public bool IsHuman { get; set; }
        public int Cash { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int ExpForNextLevel { get; set; }
        public int MaxExperienceForLevel { get; set; }
        public int CurrentExperienceForLevel { get; set; }
        public int ActionPoints { get; set; }
        public int MaximumActionPoints { get; set; }
        public int HitPoints { get; set; }
        public int MaximumHitPoints { get; set; }

        public int Physical { get; set; }

        //Mental *Mental attributes can be boosted with Items and share an Interface with Items*
        public int Mental { get; set; }
        public int Skill { get; set; }
        public int Intangibles { get; set; }

        //Info
        public int? TeamId { get; set; }
        public bool IsProspect { get; set; }
        public DateTime DraftClass { get; set; }
        public bool IsFreeAgent { get; set; }
        public bool IsNewlyDiscovered { get; set; }
        public bool IsPhysicalKnown { get; set; }
        public bool IsMentalKnown { get; set; }
        public bool IsSkillKnown { get; set; }
        public bool IsIntangiblesKnown { get; set; }
        public string ScoutedBy { get; set; }
        public int DraftPickNumber { get; set; }
        public int PersonValue { get; set; }
        public int DepthOrder { get; set; }

        //Achievements
        public int StarBowls { get; set; }

        public List<Item> Items { get; set; }

        [NotMapped]
        public List<ActionDefinition> AvailableActions { get; set; }

        [NotMapped]
        public List<AbilityDefinition> AvailableAbilities { get; set; }

        [NotMapped]
        public int MentalWithItems
        {
            get { return Items != null ? Items.Sum(p => p.Mental) + Mental : Mental; }
        }

        [NotMapped]
        public int SkillWithItems
        {
            get { return Items != null ? Items.Sum(p => p.Skill) + Skill : Skill; }
        }

        [NotMapped]
        public int IntangiblesWithItems
        {
            get { return Items != null ? Items.Sum(p => p.Intangibles) + Intangibles : Intangibles; }
        }
    }
}
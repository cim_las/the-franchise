﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Types;

namespace PresentSoftware.Common.Data.Models
{
    [Serializable]
    public class Item : INonPhysicalAttributes
    {
        #region Db

        public int Id { get; set; }
        public int ItemOwnerId { get; set; }

        public string Portrait { get; set; }
        public int ItemValue { get; set; }
        public ItemEnum ItemName { get; set; }

        //Mental
        public int Mental { get; set; }
        public int Skill { get; set; }
        public int Intangibles { get; set; }

        #endregion Db

        [NotMapped]
        public string ItemDescription
        {
            get { return this.ToString(); }
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            
            if (Mental > 0)
                builder.AppendLine(string.Format("{0} {1}", AttributeEnum.Mental.ToString(), Mental));
            if (Skill > 0)
                builder.AppendLine(string.Format("{0} {1}", AttributeEnum.Skill.ToString(), Skill));
            if (Intangibles > 0)
                builder.AppendLine(string.Format("{0} {1}", AttributeEnum.Intangibles.ToString(), Intangibles));

            return builder.ToString();
        }
    }
}

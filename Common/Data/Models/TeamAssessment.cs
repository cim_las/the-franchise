﻿using System;

namespace PresentSoftware.Common.Data.Models
{
    public class TeamAssessment
    {
        public int TeamAssessmentID { get; set; }
        public int TeamID { get; set; }
        public int PersonID { get; set; }
        public DateTime DateOfAssessment { get; set; }
        public int OffenseRating { get; set; }
        public int DefenseRating { get; set; }
        public int CoachRating { get; set; }
        public int ScoutRating { get; set; }
        public int FanRating { get; set; }
        public int AdministratorRating { get; set; }
        public int TrainerRating { get; set; }
        public int MedicalRating { get; set; }
        public int ReputationRating { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Models
{
    public class PersonAction
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int ActionId { get; set; }
        public int TargetId { get; set; }
    }
}

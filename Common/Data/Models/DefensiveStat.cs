﻿using System;

namespace PresentSoftware.Common.Data.Models
{
    public class DefensiveStat
    {
        public int DefensiveStatID { get; set; }
        public int Sacks { get; set; }
        public int Interceptions { get; set; }
        public int ForcedFumbles { get; set; }
        public int Tackles { get; set; }
        public int PassesDefensed { get; set; }
        public int Touchdowns { get; set; }
        public double PuntAvg { get; set; }
        public int PersonID { get; set; }
        public DateTime GraduationDate { get; set; }
        public bool IsCollegeStat { get; set; }
    }
}
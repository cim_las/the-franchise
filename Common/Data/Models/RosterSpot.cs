﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Data.Models
{
    public class RosterSpot
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int PersonId { get; set; }
        public PositionEnum Position { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace PresentSoftware.Common.Data.Models
{
    public class Season
    {
        public int Id { get; set; }
        public int LeagueId { get; set; }
        public DateTime SeasonStartDate { get; set; }
        public int SeasonStage { get; set; }
        public List<GameEvent> SeasonEvents { get; set; }
        public List<int> FinalStandings { get; set; }
    }
}

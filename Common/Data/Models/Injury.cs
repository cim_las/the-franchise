﻿
namespace PresentSoftware.Common.Data.Models
{
    public class Injury
    {
        public int InjuryID { get; set; }
        public string InjuryName { get; set; }
        public int Duration { get; set; }
        public int PersonID { get; set; }
    }
}

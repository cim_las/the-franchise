﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string GameName { get; set; }
        public int HeadCoachId { get; set; }
        public string DbPath { get; set; }
        public DateTime LeagueDate { get; set; }
    }
}

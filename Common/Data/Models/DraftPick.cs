﻿
namespace PresentSoftware.Common.Data.Models
{
    public class DraftPick
    {
        public int Id { get; set; }
        public int DraftId { get; set; }
        public int DraftPickNumber { get; set; }
        public int TeamId { get; set; }
        public int PersonId { get; set; }
        public bool IsHumanPick { get; set; }
        public bool IsUsed { get; set; }
    }
}

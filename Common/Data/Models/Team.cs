﻿namespace PresentSoftware.Common.Data.Models
{
    public class Team
    {
        public int Id { get; set; }
        
        public string TeamName { get; set; }
        public string DivisionName { get; set; }
        public string ConferenceName { get; set; }
        public int MegaBowls { get; set; }
        public int MegaBowlsWon { get; set; }
        public int Prestige { get; set; }
        public int HallOfFamers { get; set; }
        public int FanLoyalty { get; set; }
        public bool IsHuman { get; set; }
        public int EstimatedValue { get; set; }
    }
}

﻿using System;

namespace PresentSoftware.Common.Data.Models
{
    public class OffensiveStat
    {
        public int OffensiveStatID { get; set; }
        public DateTime GraduationDate { get; set; }
        public bool IsCollegeStat { get; set; }
        public int PersonID { get; set; }
        public int Pancakes { get; set; }
        public int RushingAttempts { get; set; }
        public int RushYards { get; set; }
        public int RushingTouchdowns { get; set; }
        public int Receptions { get; set; }
        public int ReceivingAttempts { get; set; }
        public int ReceivingYards { get; set; }
        public int ReceivingTouchdowns { get; set; }
        public int PassYards { get; set; }
        public int PassCompletions { get; set; }
        public int PassAttempts { get; set; }
        public int PassingTouchdowns { get; set; }
        public int FieldGoalAttempts { get; set; }
        public int FieldGoalsMade { get; set; }
    }
}

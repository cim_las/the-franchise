﻿using System;

namespace PresentSoftware.Common.Data.Models
{
    public class GameEvent
    {
        public int Id { get; set; }
        public int SeasonID { get; set; }
        public int EventType { get; set; }
        public string EventName { get; set; }
        public string Info { get; set; }
        public DateTime EventTime { get; set; }
        public bool IsComplete { get; set; }
    }
}

﻿namespace PresentSoftware.Common.Data.Enums
{
    public enum ExperienceLevelEnum
    {
        Level1 = 1,
        Level2, 
        Level3, 
        Level4, 
        Level5, 
        Level6, 
        Level7, 
        Level8, 
        Level9, 
        Level10,
        Level11,
        Level12,
        Level13,
        Level14,
        Level15,
    }
}
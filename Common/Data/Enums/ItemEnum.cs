﻿using System.ComponentModel;

namespace PresentSoftware.Common.Data.Enums
{
    public enum ItemEnum
    {
        Begin,
        [Description("Mega Bowl Ring")]
        MegaBowlRing,
        [Description("Trophy")]
        Trophy,
        [Description("CoffeePot")]
        CoffeePot,
        [Description("Books")]
        Books,
        [Description("Cross And Crown")]
        CrossAndCrown,
        [Description("Gold Crown")]
        GoldCrown,
        [Description("Leo Medallion")]
        LeoMedallion,
        [Description("Black Helmet")]
        BlackHelmet,
        [Description("Boulder Figure")]
        BoulderFigure,
        [Description("Target Board")]
        TargetBoard,
        [Description("Cherry And Ice Cube")]
        CherryAndIceCube,
        [Description("Ice Cube")]
        IceCube,
        [Description("Tree Emblem")]
        TreeEmblem,
        [Description("Colorful Dove")]
        ColorfulDove,
        [Description("Gold Ring")]
        GoldRing,
        [Description("Dual Silver Rings")]
        DualSilverRings,
        End,
    }
}
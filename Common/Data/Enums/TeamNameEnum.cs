﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Enums
{
    public enum TeamNameEnum
    {
        [Description("")]
        None = 0,

        [Description("New York Titans")]
        NewYorkTitans = 1,

        [Description("Philadelphia Turkeys")]
        PhiladelphiaTurkeys , 

        [Description("Washington Memorials")]
        WashingtonMemorials , 

        [Description("Dallas Americans")]
        DallasAmericans , 

        [Description("New Orleans Cajuns")]
        NewOrleansCajuns , 

        [Description("Atlanta Fowls")]
        AtlantaFowls , 

        [Description("Carolina Cats")]
        CarolinaCats , 

        [Description("Tampa Bay Pirates")]
        TampaBayPirates , 

        [Description("Chicago Grizzlies")]
        ChicagoGrizzlies , 

        [Description("Green Bay Backers")]
        GreenBayBackers , 

        [Description("Detroit Cheetahs")]
        DetroitCheetahs , 

        [Description("Minnesota Crusaders")]
        MinnesotaCrusaders , 

        [Description("Sanfrancisco 1999ers")]
        Sanfrancisco1999ers , 

        [Description("St Louis Goats")]
        StLouisGoats , 

        [Description("Seattle Seagulls")]
        SeattleSeagulls , 

        [Description("Arizona Red Birds")]
        ArizonaRedBirds , 

        [Description("New Jersey Migs")]
        NewJerseyMigs , 

        [Description("New England Tea Bags")]
        NewEnglandTeaBags , 

        [Description("Miami Sharks")]
        MiamiSharks , 

        [Description("Buffalos")]
        Buffalos , 

        [Description("Tennesse Mountains")]
        TennesseMountains , 

        [Description("Indianapolis Thoroughbreds")]
        IndianapolisThoroughbreds , 

        [Description("Houston Astronauts")]
        HoustonAstronauts , 

        [Description("Jacksonville Lynx")]
        JacksonvilleLynx , 

        [Description("Pittsburgh Miners")]
        PittsburghMiners , 

        [Description("Cincinnati Tigers")]
        CincinnatiTigers , 

        [Description("Baltimore Vultures")]
        BaltimoreVultures , 

        [Description("Cleveland Blues")]
        ClevelandBlues , 

        [Description("Kansas City Natives")]
        KansasCityNatives , 

        [Description("Oakland Pillagers")]
        OaklandPillagers , 

        [Description("San diego Power")]
        SanDiegoPower ,

        [Description("Denver Mustangs")]
        DenverMustangs , 
    }
}

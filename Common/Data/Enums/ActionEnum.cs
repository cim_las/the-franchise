﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Enums
{
    public enum ActionEnum
    {
        DoNothing,
        Encourage,
        LeadByExample,
        LunchPail,
        GetInFace,
        BeHonest,
        Lie,
        Challenge,
        Mentor,
    }
}

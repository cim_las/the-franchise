﻿

namespace Common.Types
{
    public enum GameEventEnum
    {
        PlayerTestingResults = 0,
        FacilityVisits,
        EarlyFreeAgency,
        Draft,
        LateFreeAgency,
        Camp,
        PreSeason,
        Season,
        Playoffs,
        BigBowl,
        CreateNewSeason
    };
}


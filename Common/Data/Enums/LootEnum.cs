﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Enums
{
    public enum LootEnum
    {
        ExperienceGainOwner,
        ExperienceGainTarget,
        AttributeChangeOwner,
        AttributeGainTarget,
        ActionPointRecoveryOwner,
        ActionPointRecoveryTarget,
        Item,
        Athlete,
        Coach,
        Prospect,
        MaximumActionPoints
    }
}

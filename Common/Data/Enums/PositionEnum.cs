﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Enums
{
    [Flags]
    public enum PositionEnum:long
    {
        [Description("")]
        None = 0,

        [Description("QB")]
        Quarterback = 1,

        [Description("HB")]
        HalfBack = 2,

        [Description("WR")]
        WideReceiver = 4,

        [Description("TE")]
        TightEnd = 8,

        [Description("OL")]
        OffensiveLineman = 16,

        [Description("DL")]
        DefensiveLineman = 32,

        [Description("LB")]
        Linebacker = 64,

        [Description("DB")]
        Defensiveback = 128,

        [Description("K")]
        Kicker = 256,

        [Description("P")]
        Punter = 512,

        [Description("Pres")]
        President = 1024,

        [Description("GM")]
        GeneralManager = 2048,

        [Description("HC")]
        HeadCoach = 4096,

        [Description("OC")]
        OffensiveCoordinator = HeadCoach * 2,

        [Description("DC")]
        DefensiveCoordinator = OffensiveCoordinator * 2,

        [Description("STC")]
        SpecialTeamsCoordinator = DefensiveCoordinator * 2,

        [Description("SC")]
        Scout = SpecialTeamsCoordinator * 2,
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Enums
{
    public enum AttributeEnum
    {
        Mental,
        Skill,
        Intangibles,
        Physical
    }
}

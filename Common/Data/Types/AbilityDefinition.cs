﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Data.Types
{
    [Serializable]
    public class AbilityDefinition
    {
        public string Portrait { get; set; }
        public AbilityEnum Name { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public int Value { get; set; }
    }
}

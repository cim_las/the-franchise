﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Data.Types
{
    public class Loot
    {
        public string Portrait { get; set; }
        public LootEnum Name { get; set; }
        public string Description { get; set; }

        // This is the Id of the Person/Item/etc
        public int? LootedObjectId { get; set; }

        // This is the Amount i.e. 1000 exp, 1 Coffee, 20 Action Points Recovered, 1 Point Ambition
        public int LootedValue { get; set; }
    }
}

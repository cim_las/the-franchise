﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Types
{
    public interface INonPhysicalAttributes
    {
        //Mental
        int Mental { get; set; }
        int Skill { get; set; }
        int Intangibles { get; set; }
    }
}

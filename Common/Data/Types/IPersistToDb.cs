﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Types
{
    public interface IPersistToDb
    {
        void Load();
        void Save(object pSender, CancelEventArgs pE);
    }
}

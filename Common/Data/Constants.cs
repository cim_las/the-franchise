﻿namespace PresentSoftware.Common.Data
{
    public static class Constants
    {
        public const int NUMBEROFROOKIES = 2500;
        public const int SHOWMESSAGEBOXES = 1;
        public const int ZERO = 0;
        public const int FIFTEEN = 15;
        public const int FIFTY = 50;
        public const int BUTTONGRIDCOLUMNS = 17;
        public const int BUTTONGRIDROWS = 11;
        public const int MAXNUMBERPLAYS = 10;
        public const int MINNUMBERPLAYS = 0;
        public const int MAXNUMBERFORMATIONS = 15;
        public const int MINNUMBERFORMATIONS = 0;
        public const int DEPTHCHARTSIZE = 15;
        public const int NUMBEROFJERSEYNUMBERS = 100;

        public const int DaysInYear = 365;

        //a few useful constants
        public const int MAXINT = 7777777;
        public const double MAXDOUBLE = 7777777.77;
        public const double MINDOUBLE = 0.0;

        public const double PI = 3.14159;
        public const double TWOPI = PI * 2;
        public const double HALFPI = PI / 2;
        public const double QUARTERPI = PI / 4;

        
        public static int NumberOfLeagues = 1;
        public static int NumberOfConferences = 2;
        public static int NumberOfDivisions = 4;
        public static int NumberOfTeamsPerDivision = 4;

        public const int INT_MaxAbility = 1000;

        public const int INT_SevenStar = 900;
        public const int INT_SixStar = 750;
        public const int INT_FiveStar = 600;
        public const int INT_FourStar = 450;
        public const int INT_ThreeStar = 300;
        public const int INT_TwoStar = 150;
        public const int INT_OneStar = 0;
        public const string STR_UnknownStar = "?";

        public const int INT_NonFocusedFactor = 10;
        public const int INT_FocusedFactor = 2;

        public const int INT_MaxCategories = 5;

        public const int NumberOfStates = 50;
        public const double SizeToPoundsFactor = 2.5;
        public const int December = 12;
        public const int January = 1;
        public const int YoungestAgeAllowed = 20;
        public const int YoungestAgeAllowedStaff = 33;
        public const int InchesInAFoot = 12;
        public const int SizeHeightFactor = 50;
        public const int BaseHeight = 67;
        public const double BigWeightFactor = 2.25;
        public const double NormalWeightFactor = 3.25;
        public const int WeightFluctuation = 10;
        public const int BigWeightFluctuation = 25;
        public const int MaxHeightAllowedInInches = 81;
        public const int OldestAgeForGeneratedPlayer = 33;
        public const int OldestAgeForGeneratedStaff = 70;
        public const int ProspectLowDeduction = 50;
        public const int ProspectHighDeduction = 150;

        public const string ERROR_MESSAGE_FORMAT = "\nError:\n{0}\nMessage:\n{1}";

        public const int ATTRIBUTE_LOW = 100;
        public const int ATTRIBUTE_HIGH = 900;

        public const string FILE_TIME_STAMP_FORMAT = "yyyyMMdd-hhmmss";
        public const int LOW_RANDOM_LEVEL = 1;
        public const int HIGH_RANDOM_LEVEL = 5;
        public const int LOW_ATTRIBUTE_GAIN = 15;
        public const int MAXIMUM_ATTRIBUTE_GAIN = 30;
        public const int ATTRIBUTE_SEGMENT_SIZE = 25;
        public const string ATTRIBUTE_SEGMENT_SYMBOL = "+";

        public const int TRIVIAL_DIFFICULTY = 4;
        public const int EASY_DIFFICULTY = 8;
        public const int MEDIUM_DIFFICULTY = 16;
        public const int HARD_DIFFICULTY = 17;
    }
}

﻿using System.Collections.Generic;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class ExperienceDictionary
    {
        private static Dictionary<ExperienceLevelEnum, int> _levelDictionary;
        public static Dictionary<ExperienceLevelEnum, int> LevelDictionary
        {
            get
            {
                if (_levelDictionary == null)
                {
                    _levelDictionary = new Dictionary<ExperienceLevelEnum, int>
                    {
                        {ExperienceLevelEnum.Level1, 0},
                        {ExperienceLevelEnum.Level2, 1500},
                        {ExperienceLevelEnum.Level3, 3500},
                        {ExperienceLevelEnum.Level4, 7500},
                        {ExperienceLevelEnum.Level5, 15500},
                        {ExperienceLevelEnum.Level6, 31000},
                        {ExperienceLevelEnum.Level7, 65000},
                        {ExperienceLevelEnum.Level8, 125000},
                        {ExperienceLevelEnum.Level9, 250000},
                        {ExperienceLevelEnum.Level10, 500000},
                        {ExperienceLevelEnum.Level11, 1000000},
                        {ExperienceLevelEnum.Level12, 2000000},
                        {ExperienceLevelEnum.Level13, 4000000},
                        {ExperienceLevelEnum.Level14, 8000000},
                        {ExperienceLevelEnum.Level15, 15000000}
                    };
                }
                return _levelDictionary;
            }
        }
        
    }
}

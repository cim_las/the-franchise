﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Types;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class ActionDictionary
    {
        private static object _lock = new object();
        private const string _IMAGE_BASE_PATH = "pack://application:,,,/Content/Actions";

        private static Dictionary<ActionEnum, ActionDefinition> _dictionaryOfActions;
        public static Dictionary<ActionEnum, ActionDefinition> DictionaryOfActions
        {
            get
            {
                lock (_lock)
                {
                    if (_dictionaryOfActions == null)
                    {
                        _dictionaryOfActions = new Dictionary<ActionEnum, ActionDefinition>
                        {
                            {ActionEnum.BeHonest, new ActionDefinition() {Name = ActionEnum.BeHonest , Cost = 5, Description = "Tell them the truth.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.BeHonest.ToString().ToLower())}},
                            {ActionEnum.Challenge, new ActionDefinition() {Name = ActionEnum.Challenge , Cost = 5, Description = "Challenge them to push themselves.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.Challenge.ToString().ToLower())}},
                            //{ActionEnum.DoNothing, new ActionDefinition() {Name = ActionEnum.DoNothing , Cost = 5, Description = "Sometimes there's nothing you can do.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.DoNothing.ToString().ToLower())}},
                            {ActionEnum.Encourage, new ActionDefinition() {Name = ActionEnum.Encourage , Cost = 5, Description = "Ra Ra talk", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.Encourage.ToString().ToLower())}},
                            {ActionEnum.GetInFace, new ActionDefinition() {Name = ActionEnum.GetInFace , Cost = 5, Description = "Get in their face and make them change.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.GetInFace.ToString().ToLower())}},
                            {ActionEnum.LeadByExample, new ActionDefinition() {Name = ActionEnum.LeadByExample , Cost = 5, Description = "Actions speak louder than words.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.LeadByExample.ToString().ToLower())}},
                            {ActionEnum.Lie, new ActionDefinition() {Name = ActionEnum.Lie , Cost = 10, Description = "What they don't know wont hurt you.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.Lie.ToString().ToLower())}},
                            {ActionEnum.LunchPail, new ActionDefinition() {Name = ActionEnum.LunchPail , Cost = 5, Description = "Work is what we do.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.LunchPail.ToString().ToLower())}},
                            {ActionEnum.Mentor, new ActionDefinition() {Name = ActionEnum.Mentor , Cost = 15, Description = "Mentoring is a two way street.", Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ActionEnum.Mentor.ToString().ToLower())}},
                        };
                    }
                }
                return _dictionaryOfActions;
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class DivisionDictionary
    {
        //private static List<Conference> _ConferenceList;

        private static List<string> _divisionList;
        public static List<string> DivisionList
        {
            get
            {
                if (_divisionList == null)
                {
                    _divisionList = new List<string>();
                    _divisionList.Add("National East");
                    _divisionList.Add("National South");
                    _divisionList.Add("National Central");
                    _divisionList.Add("National West");
                    _divisionList.Add("American East");
                    _divisionList.Add("American South");
                    _divisionList.Add("American Central");
                    _divisionList.Add("American West");
                }
                return _divisionList;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Types;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class AbilityDictionary
    {
        private static readonly object _lock = new object();
        private const string _IMAGE_BASE_PATH = "pack://application:,,,/Content/Abilities";

        private static Dictionary<AbilityEnum, AbilityDefinition> _dictionaryOfAbilities;
        public static Dictionary<AbilityEnum, AbilityDefinition> DictionaryOfAbilities
        {
            get
            {
                lock (_lock)
                {
                    if (_dictionaryOfAbilities == null)
                    {
                        _dictionaryOfAbilities = new Dictionary<AbilityEnum, AbilityDefinition>
                        {
                            {AbilityEnum.Burst, new AbilityDefinition() {Name = AbilityEnum.Burst , Cost = 5, Description = string.Format("Physical is increased by {0} percent.",25), Value = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, AbilityEnum.Burst.ToString().ToLower())}},
                            {AbilityEnum.IronMan, new AbilityDefinition() {Name = AbilityEnum.IronMan, Cost = 5, Description = string.Format("Recover {0} percent HP.",25), Value = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, AbilityEnum.IronMan.ToString().ToLower())}},
                            {AbilityEnum.Motivated, new AbilityDefinition() {Name = AbilityEnum.Motivated, Cost = 5, Description = string.Format("Recover {0} percent AP.",25), Value = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, AbilityEnum.Motivated.ToString().ToLower())}},
                        };
                    }
                }
                return _dictionaryOfAbilities;
            }
        }
        
    }
}

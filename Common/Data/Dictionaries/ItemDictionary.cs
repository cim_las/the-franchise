﻿using System.Collections.Generic;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class ItemDictionary
    {
        private static object _lock = new object();
        private const string _IMAGE_BASE_PATH = "pack://application:,,,/Content/Items";

        private static Dictionary<ItemEnum, Item> _dictionaryOfItems;
        public static Dictionary<ItemEnum, Item> DictionaryOfItems
        {
            get
            {
                lock (_lock)
                {
                    if (_dictionaryOfItems == null)
                    {
                        _dictionaryOfItems = new Dictionary<ItemEnum, Item>
                        {
                            {ItemEnum.BlackHelmet, new Item() {ItemName = ItemEnum.BlackHelmet ,Skill = 25, Mental=25, Intangibles = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.BlackHelmet.ToString().ToLower())}},
                            {ItemEnum.Books, new Item() {ItemName = ItemEnum.Books , Skill = 50, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.Books.ToString().ToLower())}},
                            {ItemEnum.BoulderFigure, new Item() {ItemName = ItemEnum.BoulderFigure, Mental = 50, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.BoulderFigure.ToString().ToLower())}},
                            {ItemEnum.CherryAndIceCube, new Item() {ItemName = ItemEnum.CherryAndIceCube , Mental = 25, Intangibles = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.CherryAndIceCube.ToString().ToLower())}},
                            {ItemEnum.CoffeePot, new Item() {ItemName = ItemEnum.CoffeePot , Mental = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.CoffeePot.ToString().ToLower())}},
                            {ItemEnum.ColorfulDove, new Item() {ItemName = ItemEnum.ColorfulDove, Intangibles = 50, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.ColorfulDove.ToString().ToLower())}},
                            {ItemEnum.CrossAndCrown, new Item() {ItemName = ItemEnum.CrossAndCrown , Intangibles = 50, Mental = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.CrossAndCrown.ToString().ToLower())}},
                            {ItemEnum.DualSilverRings, new Item() {ItemName = ItemEnum.DualSilverRings ,Intangibles = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.DualSilverRings.ToString().ToLower())}},
                            {ItemEnum.GoldCrown, new Item() {ItemName = ItemEnum.GoldCrown , Mental = 25, Skill = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.GoldCrown.ToString().ToLower())}},
                            {ItemEnum.GoldRing, new Item() {ItemName = ItemEnum.GoldRing ,Intangibles = 25, Skill = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.GoldRing.ToString().ToLower())}},
                            {ItemEnum.IceCube, new Item() {ItemName = ItemEnum.IceCube, Mental = 50, Intangibles = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.IceCube.ToString().ToLower())}},
                            {ItemEnum.LeoMedallion, new Item() {ItemName = ItemEnum.LeoMedallion ,Mental = 50, Intangibles = 50, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.LeoMedallion.ToString().ToLower())}},
                            {ItemEnum.MegaBowlRing, new Item() {ItemName = ItemEnum.MegaBowlRing ,Mental = 50, Intangibles = 75, Skill = 50, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.MegaBowlRing.ToString().ToLower())}},
                            {ItemEnum.TargetBoard, new Item() {ItemName = ItemEnum.TargetBoard ,Skill = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.TargetBoard.ToString().ToLower())}},
                            {ItemEnum.TreeEmblem, new Item() {ItemName = ItemEnum.TreeEmblem ,Mental= 50, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.TreeEmblem.ToString().ToLower())}},
                            {ItemEnum.Trophy, new Item() {ItemName = ItemEnum.Trophy ,Skill = 50, Intangibles = 25, Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, ItemEnum.Trophy.ToString().ToLower())}},
                        };
                    }
                }
                return _dictionaryOfItems;
            }
        }
        
    }
}

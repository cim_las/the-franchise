﻿using System.Collections.Generic;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class StateDictionary
    {
        #region Fields

        private static object _lock = new object();
        private static List<string> _stateList;
        
        #endregion Fields
                
        public static List<string> StateList
        {
            get
            {
                lock (_lock)
                {
                    if (_stateList == null || _stateList.Count <= 0)
                    {
                        _stateList = new List<string>
                        {
                            "New York",
                            "Pennsylvania",
                            "Washington",
                            "Maryland",
                            "Texas",
                            "Louisiana",
                            "Alabama",
                            "Mississippi",
                            "Georgia",
                            "North Carolina",
                            "South Carolina",
                            "Florida",
                            "Illinois",
                            "Wisconsin",
                            "Michigan",
                            "Minnesota",
                            "California",
                            "Missouri",
                            "Oregon",
                            "Arizona",
                            "New Jersey",
                            "Vermont",
                            "Massachusetts",
                            "Conneticut",
                            "New Hampshire",
                            "Delaware",
                            "Maine",
                            "Tennesse",
                            "Indiana",
                            "Kansas",
                            "Iowa",
                            "Hawaii",
                            "Alaska",
                            "Virginia",
                            "Ohio",
                            "Nevada",
                            "Kentucky",
                            "New Mexico",
                            "Oklahoma",
                            "Idaho",
                            "Wyoming",
                            "Utah",
                            "Colorado",
                            "North Dakota",
                            "South Dakota",
                            "Nebraska",
                            "West Virginia",
                            "Rhode Island",
                            "Montana",
                            "Arkansas"
                        };
                    }
                }
                return _stateList;
            }
        }
    }
}

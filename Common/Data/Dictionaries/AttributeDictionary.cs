﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class AttributeDictionary
    {
        private static readonly object _Lock = new object();

        private static List<AttributeEnum> _mentalAttributes;
        public static List<AttributeEnum> MentalAttributes
        {
            get
            {
                lock (_Lock)
                {
                    if (_mentalAttributes == null)
                    {
                        _mentalAttributes = new List<AttributeEnum>
                        {
                            AttributeEnum.Mental,
                            AttributeEnum.Intangibles,
                            AttributeEnum.Skill,
                        };
                    }
                }
                return _mentalAttributes;
            }
        }

        private static List<AttributeEnum> _physicalAttributes;
        public static List<AttributeEnum> PhysicalAttributes
        {
            get
            {
                lock (_Lock)
                {
                    if (_physicalAttributes == null)
                    {
                        _physicalAttributes = new List<AttributeEnum> {AttributeEnum.Physical};
                    }
                }
                return _physicalAttributes;
            }
        }
    }
}

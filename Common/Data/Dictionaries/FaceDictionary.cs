﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Data.Dictionaries
{
    public static class FaceDictionary
    {
        private static object _lock = new object();
        private const string _IMAGE_BASE_PATH = "pack://application:,,,/Content/Faces";

        private static Dictionary<BodyTypeEnum, int> _numFacesDictionary;
        public static Dictionary<BodyTypeEnum, int> NumFacesDictionary
        {
            get
            {
                lock (_lock)
                {
                    if (_numFacesDictionary == null)
                    {
                        _numFacesDictionary = new Dictionary<BodyTypeEnum, int>
                        {
                            {BodyTypeEnum.Basic, 10},
                            {BodyTypeEnum.Big, 8},
                            {BodyTypeEnum.Small, 5},
                            {BodyTypeEnum.Stocky, 5},
                            {BodyTypeEnum.Old, 10}
                        };
                    }
                }
                return _numFacesDictionary;
            }
        }

        private static Dictionary<BodyTypeEnum, List<string>> _dictionaryOfFaces;
        public static Dictionary<BodyTypeEnum, List<string>> DictionaryOfFaces
        {
            get
            {
                lock (_lock)
                {
                    if (_dictionaryOfFaces == null)
                    {
                        _dictionaryOfFaces = new Dictionary<BodyTypeEnum, List<string>>();
                        ListOfFacePaths = new List<string>();

                        foreach (var kvp in NumFacesDictionary)
                        {
                            var list = new List<string>();
                            for (int i = 1; i <= kvp.Value; i++)
                            {
                                list.Add(string.Format("{0}/{1}{2}.jpg", _IMAGE_BASE_PATH, kvp.Key.ToString().ToLower(), i));
                            }
                            _dictionaryOfFaces.Add(kvp.Key, list);
                            ListOfFacePaths.AddRange(list);
                        }
                    }
                }

                return _dictionaryOfFaces;
            }
        }

        public static List<string> ListOfFacePaths { get; set; }
    }
}

﻿using System;
using System.Data.Entity;
using PresentSoftware.Common.Data.Context;

namespace PresentSoftware.Common.Data.Initializers
{
    public class TheFranchiseDbInitializer : CreateDatabaseIfNotExists<TheFranchiseDbContext>
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //protected override void Seed(ProjectContext pContext)
        //{
        //    // create some sample data
        //}

        protected override void Seed(TheFranchiseDbContext pContext)
        {
            try
            {
                base.Seed(pContext);

                using (var context = new TheFranchiseDbContext())
                {
                    // We create database indexes
                    //CreateIndex(context, "Name", Pluralizer.Pluralize(typeof(Person).Name));

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Unable to create indexes", ex);
                throw;
            }
        }

        private static void CreateIndex(TheFranchiseDbContext pContext, string pField, string pTableName)
        {
            pContext.Database.ExecuteSqlCommand(String.Format("CREATE INDEX IX_{0} ON {1} ({0})", pField, pTableName));
        }
    }
}

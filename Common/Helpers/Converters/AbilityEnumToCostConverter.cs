﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Data;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class AbilityEnumToCostConverter : IValueConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public object Convert(object pValue, Type pTargetType, object pParameter, System.Globalization.CultureInfo pCulture)
        {
            var abilityEnum = (AbilityEnum)pValue;

            return AbilityDictionary.DictionaryOfAbilities[abilityEnum].Cost;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

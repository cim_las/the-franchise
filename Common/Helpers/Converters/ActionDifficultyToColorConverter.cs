﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using log4net;
using PresentSoftware.Common.Data;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class ActionDifficultyToColorConverter : IValueConverter
    {
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static int times;

        public object Convert(object pValue, Type pTargetType, object pParameter, System.Globalization.CultureInfo pCulture)
        {
            int difficulty = (int)pValue;

            if (difficulty <= Constants.TRIVIAL_DIFFICULTY)
            {
                return Brushes.LightGray;
            }
            else if (difficulty > Constants.TRIVIAL_DIFFICULTY &&  difficulty <= Constants.EASY_DIFFICULTY)
            {
                return Brushes.Green;
            }
            else if (difficulty > Constants.EASY_DIFFICULTY && difficulty <= Constants.MEDIUM_DIFFICULTY)
            {
                return Brushes.Gold;
            }
            else if (difficulty >= Constants.HARD_DIFFICULTY)
            {
                return Brushes.DarkRed;
            }
            return Brushes.LightGray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

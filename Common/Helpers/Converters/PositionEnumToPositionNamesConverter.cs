﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Data;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Extensions;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class PositionEnumToPositionNamesConverter : IValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public object Convert(object pValue, Type pTargetType, object pParameter, System.Globalization.CultureInfo pCulture)
        {
            var positionEnum = (PositionEnum) pValue;
            var some = positionEnum.GetFlags().Where(p => (PositionEnum)p != PositionEnum.None).ToList();
            string positions = string.Join(",", some.Select(p => p.GetDescription()));
            
            return positions;
        }

        public object ConvertBack(object pValue, Type pTargetType, object pArameter, System.Globalization.CultureInfo pCulture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.Windows.Data;
using log4net;
using PresentSoftware.Common.Data;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class NumberToGradeConverter : IValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static int times;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Debug.Assert(value is int, "value should be an int");
            //Debug.Assert(targetType == typeof(string), "targetType should be string");

            log.InfoFormat("Value = {0}\t Called {1} times.", (int)value, ++times);

            int intValue = (int)value;

            if (intValue >= Constants.INT_SevenStar)
            {
                return "*******";
            }
            else if (intValue >= Constants.INT_SixStar)
            {
                return "******";
            }
            else if (intValue >= Constants.INT_FiveStar)
            {
                return "*****";
            }
            else if (intValue >= Constants.INT_FourStar)
            {
                return "****";
            }
            else if (intValue >= Constants.INT_ThreeStar)
            {
                return "***";
            }
            else if (intValue >= Constants.INT_TwoStar)
            {
                return "**";
            }
            else if (intValue >= Constants.INT_OneStar)
            {
                return "*";
            }
            return "*";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

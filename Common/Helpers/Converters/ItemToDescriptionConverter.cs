﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Data;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class ItemToDescriptionConverter : IValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public object Convert(object pValue, Type pTargetType, object pParameter, System.Globalization.CultureInfo pCulture)
        {
            Debug.Assert(pValue is Item, "value should be an item");

            var item = (Item)pValue;

            var builder = new StringBuilder();
            //return base.ToString();
            //builder.AppendLine(new DescriptionAttribute(ItemName.ToString()).Description);

            if (item.Mental > 0)
                builder.AppendLine(string.Format("{0} {1}", AttributeEnum.Mental.ToString(), ItemHelper.ConvertValueToSymbol(item.Mental)));
            if (item.Intangibles > 0)
                builder.AppendLine(string.Format("{0} {1}", AttributeEnum.Intangibles.ToString(), ItemHelper.ConvertValueToSymbol(item.Intangibles)));
            if (item.Skill > 0)
                builder.AppendLine(string.Format("{0} {1}", AttributeEnum.Skill.ToString(), ItemHelper.ConvertValueToSymbol(item.Skill)));

            return builder.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

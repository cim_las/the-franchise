﻿using System;
using System.Diagnostics;
using System.Windows.Data;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class TeamIdToNameConverter : IValueConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            Debug.Assert(value is int, "value should be an int");
            //Debug.Assert(targetType == typeof(string), "targetType should be string");

            var intValue = (int)value;
            return TeamHelper.TeamList[intValue - 1]; // zero based
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

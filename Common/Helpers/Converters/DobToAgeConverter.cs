﻿using System;
using System.Diagnostics;
using System.Windows.Data;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Helpers.Database;

namespace PresentSoftware.Common.Helpers.Converters
{
    public class DobToAgeConverter : IValueConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            Debug.Assert(value is DateTime, "value should be an date");

            var dateTime = (DateTime)value;
            return (DbAccessor.ReadSetting().LeagueDate - dateTime).Days/365;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

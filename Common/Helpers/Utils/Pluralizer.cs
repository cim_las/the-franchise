﻿using System;
using System.Data.Entity;
using System.Reflection;

namespace PresentSoftware.Common.Helpers.Utils
{
    public static class Pluralizer
    {
        private static object _pluralizer;
        private static MethodInfo _pluralizationMethod;

        public static string Pluralize(string pWord)
        {
            CreatePluralizer();
            return (string)_pluralizationMethod.Invoke(_pluralizer, new object[] { pWord });
        }

        public static void CreatePluralizer()
        {
            if (_pluralizer == null)
            {
                var assembly = typeof(DbContext).Assembly;
                var type =
                    assembly.GetType(
                        "System.Data.Entity.ModelConfiguration.Design.PluralizationServices.EnglishPluralizationService");
                _pluralizer = Activator.CreateInstance(type, true);
                _pluralizationMethod = _pluralizer.GetType().GetMethod("Pluralize");
            }
        }
    }
}

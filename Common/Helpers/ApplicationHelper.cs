﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace PresentSoftware.Common.Helpers
{
    public static class ApplicationHelper
    {
        private static object ThreadLock = new object();                
        /// <summary>
        /// DbSetToList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="pCurrentDbSet"></param>
        /// <returns></returns>
        internal static List<T> DbSetToList<T>(DbSet<T> pCurrentDbSet)
            where T : class, new()
        {
            var currentList = new List<T>();
            foreach (T t in pCurrentDbSet)
            {
                currentList.Add(t);
            }
            return currentList;
        }
                
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="numberOfT"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        internal static List<T> CreateModels<T>(int numberOfT, Delegate d)
        {
            // Create list of models
            List<T> current_ListT = new List<T>();
            for (int i = 0; i < numberOfT; i++)
            {
                current_ListT.Add((T)d.DynamicInvoke(i));
            }
            return current_ListT;
        }

        /*public bool SetJerseyNumber(Person e)
        {
            List<int> availableNumbers = new List<int>();
            int jerseyLow = 0, jerseyHigh = 0;
            switch (e.EntityNaturalPosition)
            {
                case (int)Enums.NaturalPosition.QuarterBack:
                    {
                        jerseyLow = 1;
                        jerseyHigh = 16;
                        break;
                    }
                case (int)Enums.NaturalPosition.RunningBack:
                    {
                        jerseyLow = 20;
                        jerseyHigh = 40;
                        break;
                    }
                case (int)Enums.NaturalPosition.Receiver:
                    {
                        jerseyLow = 80;
                        jerseyHigh = 90;
                        break;
                    }
                case (int)Enums.NaturalPosition.OffensiveLineman:
                    {
                        jerseyLow = 60;
                        jerseyHigh = 80;
                        break;
                    }
                case (int)Enums.NaturalPosition.DefensiveLineman:
                    {
                        jerseyLow = 90;
                        jerseyHigh = 100;
                        break;
                    }
                case (int)Enums.NaturalPosition.Linebacker:
                    {
                        jerseyLow = 50;
                        jerseyHigh = 60;
                        break;
                    }
                case (int)Enums.NaturalPosition.Defensiveback:
                    {
                        jerseyLow = 20;
                        jerseyHigh = 50;
                        break;
                    }
                default:
                    e.JerseyNumber = Constants.ZERO;
                    return false;
            }
            for (int i = jerseyLow; i < jerseyHigh; i++)
            {
                if (GameManager.Instance.Teams[e.EntityTeamIdentification].PlayersOnTeam[i].JerseyNumber == 0)
                {
                    availableNumbers.Add(i);
                }
            }
            if (availableNumbers.Count > 0)
            {
                e.JerseyNumber = MyRandom.Instance.RandomOutOfList(availableNumbers);
                //GameManager.Instance.Teams[e.EntityTeamIdentification].PlayersOnTeam[e.JerseyNumber] = 1;
                return true;
            }
            else
            {
                return false;
            }
        }
*/
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Extensions;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Factories
{
    public static class LootFactory
    {
        private const int _MINIMUM_GAIN = 50;
        private const int _MAXIMUM_GAIN = 100;
        private const int _MAXROLL = 100;

        private const string _IMAGE_BASE_PATH = "pack://application:,,,/Content/Loot";

        public static List<Loot> Factory(Person pOwner, Person pTarget, List<AttributeEnum> pAttributesUsed, int pChallengeLevel, ActionEnum pAction)
        {
            var loot = new List<Loot>();

            int roll = RandomHelper.Instance.Next(_MAXROLL);
            bool negativeAction = pAction == ActionEnum.Lie;

            // Cost increases your roll
            roll += ActionDictionary.DictionaryOfActions[pAction].Cost;

            if(roll > 1)
                loot.Add(FactoryOwnerExperience(pOwner, pChallengeLevel));

            if (roll > 25)
            {
                loot.Add(FactoryTargetExperience(pTarget, pChallengeLevel));
            }

            else if(roll > 45 && roll <= 70)
            {
                loot.Add(FactoryActionPointRecoveryOwner(pOwner, pChallengeLevel));
            }
            else if (roll > 70 && roll <= 75)
            {
                loot.Add(FactoryActionPointRecoveryOwner(pOwner, pChallengeLevel));
                loot.Add(FactoryActionPointRecoveryTarget(pTarget, pChallengeLevel));
            }
            else if (roll > 75 && roll <= 80)
            {
                loot.AddRange(FactoryAttributeChangeTarget(pOwner, pAttributesUsed, pChallengeLevel, negativeAction));
            }
            
            if (roll > 80)
            {
                loot.AddRange(FactoryAttributeChangeTarget(pOwner, pAttributesUsed, pChallengeLevel, negativeAction));
                loot.AddRange(FactoryAttributeGainTarget(pTarget, pAttributesUsed, pChallengeLevel, negativeAction));
            }

            if (roll > 85 && roll <= 90)
            {
                loot.Add(FactoryItem(pOwner, pTarget, pChallengeLevel));
            }
            else if (roll > 90 && roll <= 93)
            {
                loot.Add(FactoryAthlete(pOwner, pTarget, pChallengeLevel));  
            }
            else if (roll > 93 && roll <= 96)
            {
                loot.Add(FactoryCoach(pOwner, pTarget, pChallengeLevel));
            }
            else if (roll > 97 && roll <= 99)
            {
                loot.Add(FactoryProspect(pOwner, pTarget, pChallengeLevel)); 
            }
            else if (roll >= _MAXROLL)
            {
                loot.Add(FactoryAthlete(pOwner, pTarget, pChallengeLevel));
                loot.Add(FactoryProspect(pOwner, pTarget, pChallengeLevel));
                loot.Add(FactoryCoach(pOwner, pTarget, pChallengeLevel));
            }

            foreach (var v in loot)
            {
                v.Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, v.Name.ToString().ToLower());
            }

            return loot;
        }

        public static Loot FactoryActionPointRecoveryOwner(Person pPerson, int pChallengeLevel)
        {
            var description = new StringBuilder();
            int pointsRecovered = Math.Max(pPerson.MaximumActionPoints, RandomHelper.Instance.Next(_MINIMUM_GAIN/10, _MAXIMUM_GAIN/10)*pChallengeLevel);

            description.AppendLine(string.Format("You recovered {0} action points", pointsRecovered));

            pPerson.ActionPoints += pointsRecovered;

            return new Loot()
            {
                Description = description.ToString(),
                LootedObjectId = null,
                LootedValue = pointsRecovered,
                Name = LootEnum.ActionPointRecoveryOwner,
                
            };
        }

        public static Loot FactoryActionPointRecoveryTarget(Person pTarget, int pChallengeLevel)
        {
            int pointsRecovered = Math.Max(pTarget.MaximumActionPoints, RandomHelper.Instance.Next(_MINIMUM_GAIN/10, _MAXIMUM_GAIN/10)*pChallengeLevel);

            pTarget.ActionPoints += pointsRecovered;

            return new Loot()
            {
                Description = string.Format("{0} recovered {1} action points", pTarget.Name, pointsRecovered),
                LootedObjectId = null,
                LootedValue = pointsRecovered,
                Name = LootEnum.ActionPointRecoveryTarget,
            };
        }

        public static Loot FactoryAthlete(Person pOwner, Person pTarget, int pChallengeLevel)
        {
            var person = DbAccessor.ReadPeople(p => p.IsPlayer && p.IsProspect == false && p.IsPhysicalKnown == false && p.College == pTarget.College).FirstOrDefault();
            
            // No more athletes to find then return some action points instead
            if (person == null)
            {
                return FactoryActionPointRecoveryOwner(pOwner, pChallengeLevel);
            }

            // Update the person discovered
            person.IsNewlyDiscovered = true;
            person.IsPhysicalKnown = true;
            DbAccessor.SavePerson(person);

            return new Loot()
            {
                Description = string.Format("{0} tells you about someone he knows...", pTarget.Name),
                LootedObjectId = person.Id,
                LootedValue = 1,
                Name = LootEnum.Athlete,
            };
        }

        public static List<Loot> FactoryAttributeChangeTarget(Person pTarget, List<AttributeEnum> pAttributesUsed, int pChallengeLevel, bool pNegativeAction)
        {
            var loot = new List<Loot>();

            foreach (var attributeEnum in pAttributesUsed)
            {
                AttributeHelper.AdjustSelectedAttribute(pTarget, attributeEnum, pChallengeLevel);

                loot.Add(new Loot()
                {
                    Description = string.Format("{0} {1} {2} by {3}", pTarget.IsHuman ? "Your" : pTarget.Name + "'s", attributeEnum.GetDescription(), pNegativeAction ? "decreased" : "increased", pChallengeLevel),
                    LootedObjectId = null,
                    LootedValue = pNegativeAction ? -pChallengeLevel : pChallengeLevel,
                    Name = LootEnum.AttributeChangeOwner,
                });
            }

            return loot;
        }

        public static List<Loot> FactoryAttributeGainTarget(Person pTarget, List<AttributeEnum> pAttributesUsed, int pChallengeLevel, bool pNegativeAction)
        {
            var loot = new List<Loot>();

            foreach (var attributeEnum in pAttributesUsed)
            {
                AttributeHelper.AdjustSelectedAttribute(pTarget, attributeEnum, pChallengeLevel);

                loot.Add(new Loot()
                {
                    Description = string.Format("{0}'s {1} increased by {2}", pTarget.Name, attributeEnum.GetDescription(), pChallengeLevel),
                    LootedObjectId = null,
                    LootedValue = pChallengeLevel,
                    Name = LootEnum.AttributeGainTarget,
                });
            }

            return loot;
        }

        public static Loot FactoryCoach(Person pOwner, Person pTarget, int pChallengeLevel)
        {
            var person = DbAccessor.ReadPeople(p => p.IsPlayer == false && p.IsMentalKnown == false && p.College == pTarget.College).FirstOrDefault();

            // No more coaches to find then return some action points instead
            if (person == null)
            {
                return FactoryActionPointRecoveryOwner(pOwner, pChallengeLevel);
            }

            // Update the person discovered
            person.IsNewlyDiscovered = true;
            person.IsMentalKnown = true;
            DbAccessor.SavePerson(person);

            return new Loot()
            {
                Description = string.Format("{0} tells you about someone he knows...", pTarget.Name),
                LootedObjectId = person.Id,
                LootedValue = 1,
                Name = LootEnum.Coach,
            };
        }

        public static Loot FactoryOwnerExperience(Person pOwner, int pChallengeLevel)
        {
            int experienceGained = RandomHelper.Instance.Next(_MINIMUM_GAIN, _MAXIMUM_GAIN)*pChallengeLevel;
            
            pOwner.Experience += experienceGained;
            pOwner.ExpForNextLevel = Math.Max(0, pOwner.ExpForNextLevel - experienceGained);
            pOwner.CurrentExperienceForLevel += experienceGained;

            return new Loot()
            {
                Description = string.Format("Congratulations You gained {0} Experience!", experienceGained),
                LootedObjectId = null,
                LootedValue = experienceGained,
                Name = LootEnum.ExperienceGainOwner,
            };
        }

        public static Loot FactoryTargetExperience(Person pTarget, int pChallengeLevel)
        {
            int experienceGained = RandomHelper.Instance.Next(_MINIMUM_GAIN, _MAXIMUM_GAIN)*pChallengeLevel;

            pTarget.Experience += experienceGained;
            pTarget.ExpForNextLevel = Math.Max(0, pTarget.ExpForNextLevel - experienceGained);
            pTarget.CurrentExperienceForLevel += experienceGained;

            return new Loot()
            {
                Description = string.Format("{0} gained {1} experience", pTarget.Name, experienceGained),
                LootedObjectId = null,
                LootedValue = experienceGained,
                Name = LootEnum.ExperienceGainTarget,
            };
        }

        public static Loot FactoryItem(Person pOwner, Person pTarget, int pChallengeLevel)
        {
            var item = ItemHelper.CreateRandomItemWithExclusions(ItemEnum.MegaBowlRing);

            item.ItemOwnerId = pOwner.Id;
            DbAccessor.SaveItem(item);
            
            return new Loot()
            {
                Description = string.Format("{0} gives you a {1}", pTarget.Name, item.ItemName.GetDescription()),
                LootedObjectId = item.Id,
                LootedValue = 1,
                Name = LootEnum.Item,
            };
        }

        public static Loot FactoryProspect(Person pOwner, Person pTarget, int pChallengeLevel)
        {
            var person = DbAccessor.ReadPeople(p => p.IsPlayer && p.IsProspect && p.IsPhysicalKnown == false && p.College == pTarget.College).FirstOrDefault();

            // No more prospects to find then return some action points instead
            if (person == null)
            {
                return FactoryActionPointRecoveryOwner(pOwner, pChallengeLevel);
            }

            // Update the person discovered
            person.IsNewlyDiscovered = true;
            person.IsPhysicalKnown = true;
            DbAccessor.SavePerson(person);

            return new Loot()
            {
                Description = string.Format("{0} tells you about a college prospect he knows...", pTarget.Name),
                LootedObjectId = person.Id,
                LootedValue = 1,
                Name = LootEnum.Prospect,
            };
        }

        public static List<Loot> FactoryPersonGainedLevelAttributeIncreases(Person pPerson, bool pIsOwner)
        {
            var loot = new List<Loot>();
            LootEnum lootName;

            lootName = pIsOwner ? LootEnum.AttributeChangeOwner : LootEnum.AttributeGainTarget;

            foreach (var v in AttributeDictionary.MentalAttributes)
            {
                int gain = AttributeHelper.GetRandomAttributeGain(Constants.LOW_ATTRIBUTE_GAIN, Constants.MAXIMUM_ATTRIBUTE_GAIN);

                AttributeHelper.AdjustSelectedAttribute(pPerson, v, gain);

                loot.Add(
                    new Loot()
                    {
                        Description = string.Format("{0} {1} increased by {2} ", pIsOwner ? "Your" : pPerson.Name, v, gain),
                        LootedObjectId = (int) v,
                        Name = lootName,
                        LootedValue = gain,
                        Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, lootName.ToString().ToLower()),
                    });
            }

            return loot;
        }

        public static Loot FactoryMaxApIncrease(Person pPerson, bool pIsHeadCoach)
        {
            var newMaxActionPoints = PersonHelper.CalculateMaximumActionPoints(pPerson);

            var diff = newMaxActionPoints - pPerson.MaximumActionPoints;
            pPerson.MaximumActionPoints = newMaxActionPoints;
            pPerson.ActionPoints = newMaxActionPoints;

            return new Loot()
            {
                Description = string.Format("{0} {1} increased by {2} ", pIsHeadCoach ? "Your" : pPerson.Name, LootEnum.MaximumActionPoints, diff),
                LootedObjectId = (int)LootEnum.MaximumActionPoints,
                Name = LootEnum.MaximumActionPoints,
                LootedValue = diff,
                Portrait = string.Format("{0}/{1}.jpg", _IMAGE_BASE_PATH, LootEnum.MaximumActionPoints.ToString().ToLower()),
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;

namespace PresentSoftware.Common.Helpers.Factories
{
    public static class AttributesUsedFactory
    {
        public static List<AttributeEnum> FactoryAttributesUsed(ActionEnum? pEnum)
        {
            var attributesUsed = new List<AttributeEnum>();

            switch (pEnum)
            {
                case ActionEnum.BeHonest:
                {
                    attributesUsed.Add(AttributeEnum.Intangibles);
                    break;
                }
                case ActionEnum.Challenge:
                {
                    attributesUsed.Add(AttributeEnum.Mental);
                    break;
                }
                case ActionEnum.DoNothing:
                {
                    // Do Nothing
                    break;
                }
                case ActionEnum.Encourage:
                {
                    attributesUsed.Add(AttributeEnum.Intangibles);
                    break;
                }
                case ActionEnum.GetInFace:
                {
                    attributesUsed.Add(AttributeEnum.Intangibles);
                    attributesUsed.Add(AttributeEnum.Mental);
                    break;
                }
                case ActionEnum.LeadByExample:
                {
                    attributesUsed.Add(AttributeEnum.Intangibles);
                    attributesUsed.Add(AttributeEnum.Mental);
                    break;
                }
                case ActionEnum.Lie:
                {
                    attributesUsed.Add(AttributeEnum.Intangibles);
                    // You Lied, maybe you should lose Honesty?
                    break;
                }
                case ActionEnum.LunchPail:
                {
                    attributesUsed.Add(AttributeEnum.Mental);
                    break;
                }
                case ActionEnum.Mentor:
                {
                    attributesUsed.Add(AttributeEnum.Mental);
                    attributesUsed.Add(AttributeEnum.Intangibles);
                    attributesUsed.Add(AttributeEnum.Skill);
                    break;
                }
            }

            return attributesUsed;
        }
    }
}

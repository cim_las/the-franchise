﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Extensions;
using PresentSoftware.Common.Helpers.Game;

namespace PresentSoftware.Common.Helpers.Factories
{
    public static class ActionFactory
    {
        private const int MID = 500;
        private const int UPPER = 750;

        public static void FactoryActions(Person pPerson, Person pTarget)
        {
            pPerson.AvailableActions = new List<ActionDefinition>();

            if (pPerson.MentalWithItems > MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.Challenge].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.Challenge].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.IntangiblesWithItems > MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.BeHonest].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.BeHonest].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.IntangiblesWithItems > MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.Encourage].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.Encourage].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.MentalWithItems > UPPER && pPerson.IntangiblesWithItems > MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.GetInFace].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.GetInFace].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.IntangiblesWithItems > MID && pPerson.MentalWithItems > MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.LeadByExample].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.LeadByExample].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.IntangiblesWithItems < MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.Lie].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.Lie].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.MentalWithItems > MID && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.LunchPail].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.LunchPail].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }

            if (pPerson.MentalWithItems > MID 
                && pPerson.IntangiblesWithItems > MID 
                && pPerson.SkillWithItems > MID 
                && pPerson.ActionPoints >= ActionDictionary.DictionaryOfActions[ActionEnum.Mentor].Cost)
            {
                var action = ActionDictionary.DictionaryOfActions[ActionEnum.Mentor].Clone();
                action.Difficulty = ChallengeHelper.DetermineChallengeLevel(pPerson, pTarget, AttributesUsedFactory.FactoryAttributesUsed(action.Name));
                pPerson.AvailableActions.Add(action);
            }
        }
    }
}

﻿using System.Collections.Generic;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Factories
{
    public static class AbilityFactory
    {
        public static List<Ability> FactoryAbilities(Person pPerson)
        {
            var abilities = new List<Ability>();

            var abilityType = RandomHelper.Instance.Next((int)AbilityEnum.Motivated + 1);

            abilities.Add(new Ability() {PersonId = pPerson.Id, TypeOfAbility = (AbilityEnum) abilityType});

            return abilities;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using PresentSoftware.Common.Data;

namespace PresentSoftware.Common.Helpers.Random
{
    public static class RandomHelper
    {
        public static System.Random Instance = new System.Random();

        public static int GenerateAbilityValue(int pLow, int pHigh)
        {
            return Instance.Next(pLow, pHigh);
        }

        /// <summary>
        /// Deducts from ability value if this is a prospect from college
        /// Lowest value returned is 1
        /// </summary>
        /// <param name="pLowHigh"></param>
        /// <param name="pIsProspect"></param>
        /// <returns></returns>
        public static int GenerateAbilityValue(int pLow, int pHigh, bool pIsProspect)
        {
            int a = Instance.Next(pLow, pHigh);
            int b = Instance.Next(pLow, pHigh) / 2;
            int c = Instance.Next(pLow, pHigh);

            return ((a + b + c) / 3) - (!pIsProspect ? 0 : RandomHelper.Instance.Next(Constants.ProspectLowDeduction, Constants.ProspectHighDeduction));
        }

        public static int RandomOutOfList(List<int> pListContainingNumbers)
        {
            if (pListContainingNumbers.Count > 0)
            {
                int i = RandomHelper.Instance.Next(Constants.ZERO, pListContainingNumbers.Count - 1);
                return pListContainingNumbers[i];
            }
            return 0;
        }

        public static string GetRandomHeight(int size)
        {
            int heightInInches = Math.Min(Instance.Next(size / Constants.SizeHeightFactor) + Constants.BaseHeight, Constants.MaxHeightAllowedInInches);
            return String.Format("{0}' {1}\"", heightInInches / Constants.InchesInAFoot, heightInInches % Constants.InchesInAFoot);
        }

        public static int GetRandomWeight(int size, bool big)
        {
            int weight;
            if (!big)
            {
                weight = Convert.ToInt32(size / Constants.NormalWeightFactor);
                return Instance.Next(weight - Constants.WeightFluctuation, weight + Constants.WeightFluctuation);
            }
            else
            {
                weight = Convert.ToInt32(size / Constants.BigWeightFactor);
                return Instance.Next(weight - Constants.BigWeightFluctuation, weight + Constants.BigWeightFluctuation);
            }
            
        }

        public static int GetRandomCollegeIndex()
        {
            return Instance.Next(1, Constants.NumberOfStates) - 1;
        }

        public static DateTime GetPlayerRandomDOB(int pCurrentYear)
        {
            int month = Instance.Next(Constants.January, Constants.December);
            int year = Instance.Next(pCurrentYear - Constants.OldestAgeForGeneratedPlayer, pCurrentYear - Constants.YoungestAgeAllowed);
            int day = Instance.Next(1, 28);
            return new DateTime(year, month, day);
        }

        public static DateTime GetStaffRandomDOB(int pCurrentYear)
        {
            int month = Instance.Next(Constants.January, Constants.December);
            int year = Instance.Next(pCurrentYear - Constants.OldestAgeForGeneratedStaff, pCurrentYear - Constants.YoungestAgeAllowedStaff);
            int day = Instance.Next(1, 28);
            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Randomly scrambles a list of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pList"></param>
        /// <returns></returns>
        public static List<T> ScrambleAList<T>(List<T> pList)
        {
            var scrambledList = new List<T>();
            do
            {
                int index = RandomHelper.Instance.Next(pList.Count - 1);
                scrambledList.Add(pList[index]);
                pList.Remove(pList[index]);
            } while (pList.Count > 0);

            return scrambledList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.Common.Helpers.Content
{
    public static class FaceHelper
    {
        public static void SetPersonImagePath(Person pPerson)
        {
            pPerson.Portrait = FaceDictionary.DictionaryOfFaces[pPerson.BodyType][Random.RandomHelper.Instance.Next(FaceDictionary.NumFacesDictionary[pPerson.BodyType])];
        }
    }
}

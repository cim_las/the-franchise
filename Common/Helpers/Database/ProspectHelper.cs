﻿using System;
using System.Collections.Generic;
using log4net;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class ProspectHelper
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const double DBL_FASTEST40TIME = 4.1;
        private const double DBL_FASTEST3CONETIME = 6.3;
        private const double DBL_HIGHESTVERTJUMP = 48;
        private const double DBL_LONGESTLONGJUMP = 138;
        private const double DBL_MOSTBENCHPRESS = 138;
        private const double DBL_BESTWONDERLIC = 50;        
        private const int VARIANCE = 50;

        public static void CreateRookies(DateTime pSeasonStartDate)
        {
            log.Info("Creating rookies");

            // General population
            int generalPopulation = RandomHelper.Instance.Next(700, 900);
            int quarterbacks = RandomHelper.Instance.Next(20,30);
            int kickersPunters = RandomHelper.Instance.Next(10, 20);

            var persons = new List<Person>();

            for (int i = 0; i < generalPopulation; i++)
            {
                var p = new Person
                {  
                    Position = (PositionEnum) RandomHelper.Instance.Next((int) PositionEnum.HalfBack, (int) PositionEnum.Defensiveback)
                };

                persons.Add(p);
            }


            for (int i = 0; i < quarterbacks; i++)
            {
                var p = new Person
                {
                    Position = PositionEnum.Quarterback
                };

                persons.Add(p);
            }

            for (int i = 0; i < kickersPunters; i++)
            {
                var p = new Person
                {
                    Position = (PositionEnum)RandomHelper.Instance.Next((int)PositionEnum.Kicker, (int)PositionEnum.Punter)
                };

                persons.Add(p);
            }

            foreach (var person in persons)
            {
                person.IsProspect = true;
                person.IsPlayer = true;
                person.DraftClass = pSeasonStartDate;

                PersonHelper.FillPerson(person, pSeasonStartDate);
            }

            DbAccessor.BulkSave(persons);
        }
    }
}
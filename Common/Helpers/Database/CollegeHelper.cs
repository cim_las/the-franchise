﻿using System;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class CollegeHelper
    {
        private const int INT_MaxCollegeGames = 48;
        private const int INT_CollegeGamesPerYear = 12;

        public static void GenerateCollegeStats(Data.Models.Person pPerson)
        {
            if(pPerson.IsProspect == false)
            {
                pPerson.DraftClass = new DateTime(pPerson.DateOfBirth.Year + 20, 2, 1);
            }

            var offensiveCollegeStats = new OffensiveStat()
            {
                PersonID = pPerson.Id,
                GraduationDate = pPerson.DraftClass,
                IsCollegeStat = true,
            };

            var defensiveCollegeStats = new DefensiveStat()
            {
                PersonID = pPerson.Id,
                GraduationDate = pPerson.DraftClass,
                IsCollegeStat = true,
            };
        }
    }
}

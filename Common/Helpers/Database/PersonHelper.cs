﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Documents;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Content;
using PresentSoftware.Common.Helpers.Factories;
using PresentSoftware.Common.Helpers.Game;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class PersonHelper
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static object _lock = new object();

        public static void CreateInitialPersonsAndAddToTeam()
        {
            try
            {
                var teams = DbAccessor.ReadTeams();
                var leagueDate = DbAccessor.ReadSetting().LeagueDate;

                var s = new Stopwatch();
                s.Start();
                _log.Info("Start of Create players on teams");

                Parallel.ForEach(teams, pTeam =>
                {
                    _log.DebugFormat("Creating persons for: {0}", pTeam.TeamName);
                    ConcurrentBag<Person> persons = CreatePersonsOnTeamAndAssignJob();

                    foreach (Person p in persons)
                    {
                        // About half the players not on your team you start out knowing their physical
                        p.IsPhysicalKnown = RandomHelper.Instance.Next(1) == 1;

                        p.IsMentalKnown = pTeam.IsHuman;
                        p.IsIntangiblesKnown = pTeam.IsHuman;
                        p.IsSkillKnown = pTeam.IsHuman;
                        p.IsPhysicalKnown = pTeam.IsHuman;
                        p.TeamId = pTeam.Id;
                        FillPerson(p, leagueDate);
                    }

                    // Save to set Primary key
                    DbAccessor.BulkSave(persons);

                    var items = new List<Item>();
                    var abilities = new List<Ability>();
                    foreach (var person in DbAccessor.ReadPeople(p => p.TeamId == pTeam.Id))
                    {
                        items.AddRange(SetPersonInitialItems(person));
                        abilities.AddRange(AbilityFactory.FactoryAbilities(person));
                    }

                    DbAccessor.BulkSave(items);
                    DbAccessor.BulkSave(abilities);
                });

                s.Stop();
                _log.InfoFormat("End of Create players on teams \t\t\tTime:{0}", s.Elapsed);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static void FillPerson(Person pPerson, DateTime pLeagueDate)
        {
            if (pPerson.IsPlayer)
            {
                AssignPlayerAbilities(pPerson);
            }
            else
            {
                AssignNonPlayerAbilities(pPerson);
            }

            AssignPersonInfo(pPerson, pLeagueDate);

            FaceHelper.SetPersonImagePath(pPerson);
            CollegeHelper.GenerateCollegeStats(pPerson);
            pPerson.Name = String.Format("{0} {1}", GenerateFirstName(), GenerateLastName());
            RandomallySetLevel(pPerson);
            SetExperience(pPerson);
            SetInitialPersonSkillsBasedOnLevel(pPerson);
            _log.InfoFormat("Person Name: {0}\tPosition: {1}\tValue: {2}", pPerson.Name, (PositionEnum)pPerson.Position, pPerson.PersonValue);
        }

        private static List<Item> SetPersonInitialItems(Person pPerson)
        {
            var items = new List<Item>();

            int numberOfItems = pPerson.Level/5;
            for (int i = 0; i < numberOfItems; i++)
            {
                var item = ItemHelper.CreateRandomItem();
                item.ItemOwnerId = pPerson.Id;
                items.Add(item);
            }
            return items;
        }

        public static void FillHumanHeadCoach(Person pPerson, DateTime pLeagueDate)
        {
            AssignNonPlayerAbilities(pPerson);
            AssignPersonInfo(pPerson, pLeagueDate);
            CollegeHelper.GenerateCollegeStats(pPerson);

            pPerson.Level = 1;
            pPerson.ExpForNextLevel = ExperienceDictionary.LevelDictionary[ExperienceLevelEnum.Level2];
            pPerson.MaxExperienceForLevel = ExperienceDictionary.LevelDictionary[ExperienceLevelEnum.Level2];
            int attributeIncrease = RandomHelper.Instance.Next(Constants.LOW_ATTRIBUTE_GAIN, Constants.MAXIMUM_ATTRIBUTE_GAIN) * pPerson.Level;
            pPerson.MaximumActionPoints = CalculateMaximumActionPoints(pPerson);
            pPerson.ActionPoints = pPerson.MaximumActionPoints;
            pPerson.IsHuman = true;
            
            _log.InfoFormat("Person Name: {0}\tPosition: {1}\tValue: {2}", pPerson.Name, (PositionEnum)pPerson.Position, pPerson.PersonValue);
        }

        public static int CalculateMaximumActionPoints(Person pPerson)
        {
            return (pPerson.Mental/80 + pPerson.Intangibles/100);
        }

        private static void SetInitialPersonSkillsBasedOnLevel(Person pPerson)
        {
            int attributeIncrease = AttributeHelper.GetRandomAttributeGain(Constants.LOW_ATTRIBUTE_GAIN, Constants.MAXIMUM_ATTRIBUTE_GAIN) * pPerson.Level;

            pPerson.MaximumActionPoints = CalculateMaximumActionPoints(pPerson);
            pPerson.ActionPoints = pPerson.MaximumActionPoints;

            if (pPerson.Level == (int) ExperienceLevelEnum.Level1)
            {
                return;
            }

            pPerson.Intangibles += attributeIncrease;
            pPerson.Mental += attributeIncrease;
            pPerson.Skill += attributeIncrease;
            
            //pPerson.Durability += attributeIncrease;
            //pPerson.Strength += attributeIncrease;
            //pPerson.Size += attributeIncrease;
            //pPerson.Speed += attributeIncrease;
        }

        private static void SetExperience(Person pPerson)
        {
            pPerson.Experience = ExperienceDictionary.LevelDictionary[(ExperienceLevelEnum)pPerson.Level];

            if (pPerson.Level < (int) ExperienceLevelEnum.Level15)
            {
                pPerson.ExpForNextLevel = ExperienceDictionary.LevelDictionary[(ExperienceLevelEnum)pPerson.Level + 1] - pPerson.Experience;
                pPerson.MaxExperienceForLevel = ExperienceDictionary.LevelDictionary[ExperienceLevelEnum.Level2];
                pPerson.CurrentExperienceForLevel = pPerson.MaxExperienceForLevel - pPerson.ExpForNextLevel;
            }
            else
            {
                pPerson.ExpForNextLevel = 0;
                pPerson.MaxExperienceForLevel = 0;
                pPerson.CurrentExperienceForLevel = 0;
            }
        }

        private static void RandomallySetLevel(Person pPerson)
        {
            pPerson.Level = RandomHelper.Instance.Next(Constants.LOW_RANDOM_LEVEL, Constants.HIGH_RANDOM_LEVEL);
            if (pPerson.Level < 4)
                return;
            
            pPerson.Level += RandomHelper.Instance.Next(Constants.LOW_RANDOM_LEVEL, Constants.HIGH_RANDOM_LEVEL);
            if (pPerson.Level < 8)
                return;

            pPerson.Level += RandomHelper.Instance.Next(Constants.LOW_RANDOM_LEVEL, Constants.HIGH_RANDOM_LEVEL);
        }

        public static int GetSumOfPersonAttributes(Person pPerson)
        {
            return pPerson.Intangibles
                   + pPerson.Mental
                   + pPerson.Skill
                   + pPerson.Physical;
        }

        public static int GetValueOfPlayerItems(Person pPerson)
        {
            return DbAccessor.ReadItems(p => p.ItemOwnerId == pPerson.Id).Sum(p => p.ItemValue);
        }

        public class PlayablePositions
        {
            public PositionEnum Position { get; set; }
            public BodyTypeEnum BodyType { get; set; }
            public int Count { get; set; }
        }

        public static ConcurrentBag<Person> CreatePersonsOnTeamAndAssignJob()
        {
            var personList = new ConcurrentBag<Person>();
            var players = new List<PlayablePositions>()
            {
                new PlayablePositions() {Position = PositionEnum.Quarterback, BodyType = BodyTypeEnum.Basic, Count = RandomHelper.Instance.Next(2, 4)},
                new PlayablePositions() {Position = PositionEnum.HalfBack, BodyType = BodyTypeEnum.Basic, Count = RandomHelper.Instance.Next(3, 5)},
                new PlayablePositions() {Position = PositionEnum.WideReceiver, BodyType = BodyTypeEnum.Basic, Count = RandomHelper.Instance.Next(5, 8)},
                new PlayablePositions() {Position = PositionEnum.TightEnd, BodyType = BodyTypeEnum.Stocky, Count = RandomHelper.Instance.Next(2, 4)},
                new PlayablePositions() {Position = PositionEnum.OffensiveLineman, BodyType = BodyTypeEnum.Big, Count = RandomHelper.Instance.Next(8, 10)},
                new PlayablePositions() {Position = PositionEnum.DefensiveLineman, BodyType = BodyTypeEnum.Big, Count = RandomHelper.Instance.Next(6, 9)},
                new PlayablePositions() {Position = PositionEnum.Linebacker, BodyType = BodyTypeEnum.Stocky, Count = RandomHelper.Instance.Next(6, 9)},
                new PlayablePositions() {Position = PositionEnum.Defensiveback, BodyType = BodyTypeEnum.Basic, Count = RandomHelper.Instance.Next(7, 9)},
                new PlayablePositions() {Position = PositionEnum.Kicker, BodyType = BodyTypeEnum.Small, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions() {Position = PositionEnum.Punter, BodyType = BodyTypeEnum.Small, Count = RandomHelper.Instance.Next(1, 1)},
            };

            var staff = new List<PlayablePositions>()
            {
                new PlayablePositions(){Position = PositionEnum.President, BodyType = BodyTypeEnum.Old, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions(){Position = PositionEnum.GeneralManager, BodyType = BodyTypeEnum.Old, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions(){Position = PositionEnum.HeadCoach, BodyType = BodyTypeEnum.Old, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions(){Position = PositionEnum.OffensiveCoordinator, BodyType = BodyTypeEnum.Old, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions(){Position = PositionEnum.DefensiveCoordinator, BodyType = BodyTypeEnum.Old, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions(){Position = PositionEnum.SpecialTeamsCoordinator, BodyType = BodyTypeEnum.Old, Count = RandomHelper.Instance.Next(1, 1)},
                new PlayablePositions(){Position = PositionEnum.Scout, BodyType = BodyTypeEnum.Basic, Count = RandomHelper.Instance.Next(5, 10)},
            };

            foreach (var playablePositions in players)
            {
                for (int i = 0; i < playablePositions.Count; i++)
                {
                    var p = new Person {Position = playablePositions.Position, BodyType = playablePositions.BodyType, IsPlayer = true};
                    personList.Add(p);
                }
            }

            foreach (var member in staff)
            {
                for (int i = 0; i < member.Count; i++)
                {
                    var p = new Person { Position = member.Position, BodyType = member.BodyType};
                    personList.Add(p);
                }
            }

            return personList;
        }

        public static void AssignPlayerAbilities(Person p)
        {
            try
            {
                p.Intangibles = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                p.Mental = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                p.Skill = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                p.Physical = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);

                _log.DebugFormat("IsProspect {0} \t\t Skill {1}", p.IsProspect, p.Skill);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        public static void AssignNonPlayerAbilities(Person p)
        {
            try
            {
                p.Intangibles = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                p.Mental = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                p.Skill = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                
                //p.Size = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                //p.Speed = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                //p.Strength= RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);
                p.Physical = RandomHelper.GenerateAbilityValue(Constants.ATTRIBUTE_LOW, Constants.ATTRIBUTE_HIGH, p.IsProspect);

                _log.DebugFormat("IsProspect {0} \t\t Skill {1}", p.IsProspect, p.Skill);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        public static void AssignPersonInfo(Person pPerson, DateTime pLeagueDate)
        {
            if (pPerson.IsPlayer)
                pPerson.DateOfBirth = RandomHelper.GetPlayerRandomDOB(pLeagueDate.Year);
            else // Non players are at least 15-25 years older
                pPerson.DateOfBirth = RandomHelper.GetStaffRandomDOB(pLeagueDate.Year);
                
            pPerson.College = StateDictionary.StateList[RandomHelper.GetRandomCollegeIndex()];

            AssignCash(pPerson);
        }

        public static void AssignCash(Person pPerson)
        {
            if (pPerson.Position == PositionEnum.President)
                pPerson.Cash = RandomHelper.Instance.Next(50000000, 100000000);
            else
                pPerson.Cash = RandomHelper.Instance.Next(50000, 1000000);
        }

        public static string GenerateFirstName()
        {
            return Enum.GetName(typeof(FirstNameEnum), RandomHelper.Instance.Next((int)FirstNameEnum.Begin + 1, (int)FirstNameEnum.End - 1));
        }

        public static string GenerateLastName()
        {
            return Enum.GetName(typeof (LastNameEnum), RandomHelper.Instance.Next((int) LastNameEnum.Begin + 1, (int) LastNameEnum.End - 1));
        }

        public static string GetAgeFromDateTime(TimeSpan? ageTimeSpan)
        {
            return (ageTimeSpan.Value.Days / Constants.DaysInYear).ToString();
        }
    }
}

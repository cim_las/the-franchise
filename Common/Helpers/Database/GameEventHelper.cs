﻿using System;
using System.Linq;
using Common.Types;
using log4net;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class GameEventHelper
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //public static GameEvent AddEventToSeason(Season pSeason, int pHumanTeamId)
        //{
        //    var gameEvent = GameEventHelper.GetNextGameEvent(pSeason);

        //    if (gameEvent != null)
        //    {
        //        SeasonHelper.ProcessSeasonEvent((GameEventEnum)gameEvent.EventType, pHumanTeamId);
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }            
        //}

        

        public static void CreateStartOfSeasonEvents(Season pNewSeason)
        {
            log.Info("Creating Season events");
            int seasonYear = pNewSeason.SeasonStartDate.Year;

            // Reveal results of testing(show 40 time, 3 cone, bench press, vertical, long jump, wonderlic)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.PlayerTestingResults.ToString(),
                EventType = (int)GameEventEnum.PlayerTestingResults,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 2, 15),
                Info = "Player Testing Results are in, your scouts have reported back on their opinion of some of the prospects."
            });

            // Allow user to choose which Positions they want their scouts to focus on
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.FacilityVisits.ToString(),
                EventType = (int)GameEventEnum.FacilityVisits,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 2, 21),
                Info = "You may now choose which positions your scouts will focus on."
            });

            // Early Free Agency(March)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.EarlyFreeAgency.ToString(),
                EventType = (int)GameEventEnum.EarlyFreeAgency,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 3, 1),
                Info = "Early Free Agency has begun, you may offer contracts to free agents."
            });

            // Draft(April)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.Draft.ToString(),
                EventType = (int)GameEventEnum.Draft,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 4, 15),
                Info = "The draft begins today. Go to the draft screen to decide your teams future."
            });

            // Late Free Agency(May-June)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.LateFreeAgency.ToString(),
                EventType = (int)GameEventEnum.LateFreeAgency,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 5, 1),
                Info = "Late Free Agency has begun, time to sure up your teams holes."
            });

            // Camp(July)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.Camp.ToString(),
                EventType = (int)GameEventEnum.Camp,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 7, 5),
                Info = "Camp has begun, your coaches can lay the foundation for the rest of the year."
            });

            // Pre-Season(August)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.PreSeason.ToString(),
                EventType = (int)GameEventEnum.PreSeason,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 8, 1),
                Info = "Pre Season, you can't win a championship in the preseason but you can sure lose your season with the wrong injury."
            });

            // Season(Septempber-December)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.Season.ToString(),
                EventType = (int)GameEventEnum.Season,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear, 9, 1),
                Info = "The regular season has begun."
            });

            // Playoffs(January)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.Playoffs.ToString(),
                EventType = (int)GameEventEnum.Playoffs,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear + 1, 1, 1),
                Info = "Now the Real season begins."
            });

            // BigBowl(January)
            DbAccessor.SaveGameEvent(new GameEvent()
            {
                EventName = GameEventEnum.BigBowl.ToString(),
                EventType = (int)GameEventEnum.BigBowl,
                SeasonID = pNewSeason.Id,
                EventTime = new DateTime(seasonYear + 1, 2, 1),
                Info = "Big Bowl"
            });
        }

        public static void ProcessPlayerTestingResults()
        {
            int i = 1;
            foreach (Person per in DbAccessor.ReadPeople().Where(p => p.IsProspect == true))
            {
                if (per.IsProspect && per.Skill % 2 == 0)
                {
                    
                    // About half the players go through the Combine and get physical results
                    per.IsPhysicalKnown = RandomHelper.Instance.Next(1) == 1;

                    log.DebugFormat("Just Scouted {0} Position {1} #Players Scouted {2}", per.Name, per.Position, i++);
                    DbAccessor.SavePerson(per);
                }
            }
        }
    }
}

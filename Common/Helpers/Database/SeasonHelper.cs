﻿using System;
using System.Diagnostics;
using System.Linq;
using Common.Types;
using log4net;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Game;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class SeasonHelper
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Season CreateNewSeason()
        {
            log.Info("Creating New Season");

            int leagueYear = DbAccessor.ReadSetting().LeagueDate.Year;

            var newSeason = new Season() { SeasonStartDate = new DateTime(leagueYear, 2, 7) };

            log.InfoFormat("End of Create Season...Year: {0}", leagueYear);

            DbAccessor.SaveSeason(newSeason);

            // Create rookies
            var s = new Stopwatch();
            s.Start();
            log.InfoFormat("Start of Create Rookies...Year: {0}", leagueYear);
            ProspectHelper.CreateRookies(newSeason.SeasonStartDate);
            log.InfoFormat("End of Create Rookies \t\t\tTime:{0}", s.Elapsed);

            // Create season events
            GameEventHelper.CreateStartOfSeasonEvents(newSeason);
            log.InfoFormat("End of Create Season Events...Year: {0}", leagueYear);

            return newSeason;
        }

        public static void ProcessSeasonEvent()
        {
            var gameEvent = DbAccessor.ReadGameEvents().Where(p => p.IsComplete == false).OrderBy(p => p.EventTime).FirstOrDefault();
            log.InfoFormat("Processing season event: {0}", (gameEvent.EventType));

            switch ((GameEventEnum)gameEvent.EventType)
            {
                case GameEventEnum.PlayerTestingResults:
                    GameEventHelper.ProcessPlayerTestingResults();
                    break;
                case GameEventEnum.FacilityVisits:
                    break;
                case GameEventEnum.EarlyFreeAgency:
                    break;
                case GameEventEnum.Draft:
                    break;
                case GameEventEnum.LateFreeAgency:
                    break;
                case GameEventEnum.Camp:
                    break;
                case GameEventEnum.PreSeason:
                    break;
                case GameEventEnum.Season:
                    break;
                case GameEventEnum.Playoffs:
                    break;
                case GameEventEnum.CreateNewSeason:
                {
                    SeasonHelper.CreateNewSeason();
                    break;
                }
                default:
                    break;
            }
        }
    }
}

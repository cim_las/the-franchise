﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class SettingsHelper
    {
        public static Setting FactorySettings(string pDbPath, string pGameName)
        {
            return new Setting()
            {
                DbPath = pDbPath,
                GameName = pGameName,
                LeagueDate = new DateTime(DateTime.Now.Year, 2, 7),
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Types;
using log4net;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class DraftHelper
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public const int INT_NumberAdditionalDrafts = 2;
        public const int INT_MaxDraftRounds = 7;
        private const int INT_DraftDelay = 1;
        public const int INT_FirstRound = 1;

        public static void InitialDraftSetup()
        {
            var draftDate = DbAccessor.ReadGameEvents().Where(p => p.EventType == (int) GameEventEnum.Draft && p.IsComplete == false).OrderBy(i=>i.Id).First();
            // If 0 then will only create for the DraftDate passed in, if > 0 then will create for the date passed in plus additional years
            for (int i = 0; i <= INT_NumberAdditionalDrafts; i++)
            {
                SetupDraft(draftDate.EventTime.AddYears(i), ref i );
            }
        }

        private static void SetupDraft(DateTime pDraftDate, ref int pDraftNumber)
        {
            var draft = new Draft() { DraftDate = pDraftDate };
            //pDraftPickList = new List<DraftPick>();

            DbAccessor.SaveDraft(draft);
            var draftPicks = new List<DraftPick>();

            // First draft
            if (pDraftNumber == 0)
            {
                List<int> determineOrder = DetermineOrder();
                
                for (int roundsCompleted = 0; roundsCompleted < 7; roundsCompleted++)
                {
                    // if is first draft then randomize the order
                    foreach (int t in determineOrder)
                    {
                        var draftPick = new DraftPick() { TeamId = t, DraftId = draft.Id, DraftPickNumber = (determineOrder.IndexOf(t) + 1) + roundsCompleted * TeamHelper.IntNumberOfTeams};
                        draftPicks.Add(draftPick);
                        log.DebugFormat("Pick # {0}", draftPick.DraftPickNumber);
                    }
                }
            }
            else
            {
                for (int round = 1; round <= 7; round++)
                {
                    foreach (Team t in DbAccessor.ReadTeams())
                    {
                        // Order is not set in this case until the season is played
                        var draftPick = new DraftPick() { TeamId = t.Id, DraftId = draft.Id };
                        draftPicks.Add(draftPick);
                    }
                }
            }

            DbAccessor.BulkSave(draftPicks);
        }

        //Determine the order
        public static List<int> DetermineOrder()
        {
            // Only 1 season
            if (DbAccessor.ReadSeasons().Count() <= 1)
            {
                var teamIdList = new List<int>();

                // First draft use random draft order
                for (int i = 1; i <= 32; i++)
                {
                    teamIdList.Add(i);
                }

                List<int> scrambledTeamIdList = RandomHelper.ScrambleAList<int>(teamIdList);
                return scrambledTeamIdList;
            }
            else
            {
                int currentSeasonId = DbAccessor.ReadSeasons().Select(s => s.Id).Max();
                var firstOrDefault = DbAccessor.ReadSeasons().FirstOrDefault(pSeason => pSeason.Id == currentSeasonId - 1);
                if (firstOrDefault != null)
                    return firstOrDefault.FinalStandings;
            }
            return null;
        }

        //receive a player and select that player, then save the selection in Draft Results
        public static void SelectPlayer(int pIdentification)
        {

        }

        //Auto draft until a human controlled Team
        public static void FastDraft(Draft pDraft)
        {
            //pDraftViewModel.AutoDraftIsOn = true;
            while (pDraft.AutoDraft && pDraft.IsComplete == false)
            {
                var nextPick = DbAccessor.ReadDraftPicks().Where(p => p.IsUsed == false && p.DraftId == pDraft.Id).OrderBy(p => p.DraftPickNumber).FirstOrDefault();

                if (nextPick == null)
                {
                    pDraft.IsComplete = true;
                    break;
                }

                if (nextPick.IsHumanPick)
                {
                    pDraft.AutoDraft = false;
                    return;
                }

                Person person = FindDraftablePersonForTeam();

                DraftPerson(nextPick, person);
                
                // Wait according to draft speed
                // TODO: Add selectable draft delay
                System.Threading.Thread.Sleep(new TimeSpan(0, 0, INT_DraftDelay));
            }
        }

        public static bool DraftPerson(DraftPick pDraftPick, Person pPerson)
        {
            // Set prospects new team
            pPerson.TeamId = pDraftPick.TeamId;

            // Set person selected in Draft pick
            pDraftPick.PersonId = pPerson.Id;
            
            // Set Draft pick number
            pPerson.DraftPickId = pDraftPick.Id;
            
            // Save changes
            DbAccessor.SavePerson(pPerson);
            DbAccessor.SaveDraftPick(pDraftPick);

            return true;
        }

        public static Person FindDraftablePersonForTeam()
        {
            var person = DbAccessor.ReadPeople().Where(p => p.TeamId == null && p.IsProspect == true).OrderByDescending(p => p.Skill).FirstOrDefault();

            return person;
        }

        public static DraftPick GetNextDraftPick(int pDraftID)
        {
            return DbAccessor.ReadDraftPicks().Where(pDraft => pDraft.Id == pDraftID).OrderBy(p => p.DraftPickNumber).FirstOrDefault();
        }
    }
}

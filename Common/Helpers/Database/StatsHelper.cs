﻿using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class StatsHelper
    {
        public static void FillPassingStats(OffensiveStat pOffensiveStat, int pGamesPlayed)
        {
            pOffensiveStat.PassAttempts = 1;
            pOffensiveStat.PassCompletions = 1;
            pOffensiveStat.PassYards = 1;
            pOffensiveStat.PassingTouchdowns = 1;
        }

        public static void FillRushingStats(OffensiveStat pOffensiveStat, int pGamesPlayed)
        {
            pOffensiveStat.RushingAttempts = 1;
            pOffensiveStat.RushYards = 1;
            pOffensiveStat.RushingTouchdowns = 1;
        }

        public static void FillReceivingStats(OffensiveStat pOffensiveStat, int pGamesPlayed)
        {
            pOffensiveStat.ReceivingAttempts = 1;
            pOffensiveStat.ReceivingYards = 1;
            pOffensiveStat.ReceivingTouchdowns = 1;
        }

        public static void FillOffensiveLinemanStats(OffensiveStat pOffensiveStat, int pGamesPlayed)
        {
            pOffensiveStat.Pancakes = 1;
        }

        public static void FillKickerStats(OffensiveStat pOffensiveStat, int pGamesPlayed)
        {
            pOffensiveStat.FieldGoalAttempts = 1;
            pOffensiveStat.FieldGoalsMade = 1;
        }

        public static void FillDefensiveLinemanStats(DefensiveStat pDefensiveStat, int pGamesPlayed)
        {
            pDefensiveStat.Sacks = 1;
            pDefensiveStat.Tackles = 1;
            pDefensiveStat.PassesDefensed = 1;
            pDefensiveStat.ForcedFumbles = 1;
            pDefensiveStat.Interceptions = 1;
            pDefensiveStat.Touchdowns = 1;
        }

        public static void FillLinebackerStats(DefensiveStat pDefensiveStat, int pGamesPlayed)
        {
            pDefensiveStat.Sacks = 1;
            pDefensiveStat.Tackles = 1;
            pDefensiveStat.PassesDefensed = 1;
            pDefensiveStat.ForcedFumbles = 1;
            pDefensiveStat.Interceptions = 1;
            pDefensiveStat.Touchdowns = 1;
        }

        public static void FillDefensivebackStats(DefensiveStat pDefensiveStat, int pGamesPlayed)
        {
            pDefensiveStat.Sacks = 1;
            pDefensiveStat.Tackles = 1;
            pDefensiveStat.PassesDefensed = 1;
            pDefensiveStat.ForcedFumbles = 1;
            pDefensiveStat.Interceptions = 1;
            pDefensiveStat.Touchdowns = 1;
        }

        public static void FillPuntStats(DefensiveStat pDefensiveStat, int pGamesPlayed)
        {
            pDefensiveStat.PuntAvg = 1.1;
        }
    }
}

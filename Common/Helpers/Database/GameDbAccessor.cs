﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.SqlServerCe;
using System.IO;
using System.Web;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Initializers;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class GameDbAccessor
    {
        #region Fields

        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string _CONNECTION_FORMAT = "DataSource=\"{0}\"; Password='{1}'";
        private const string _DB_FOLDER = "db";
        private static string _storedConnectionString = string.Empty;

        #endregion Fields

        #region Properties
        #endregion Properties

        #region Connection

        public static bool CreateDb(int pUserId, int pDbId, string pPassword = "present1234")
        {
            try
            {
                string dbFolder = Path.Combine(pUserId.ToString(), _DB_FOLDER);
                string dbPath = Path.Combine(HttpRuntime.AppDomainAppPath, Path.Combine(dbFolder, pDbId + ".sdf"));
                _storedConnectionString = string.Format(_CONNECTION_FORMAT, dbPath, pPassword);

                if (!Directory.Exists(dbFolder))
                {
                    Directory.CreateDirectory(dbFolder);
                }

                using (var en = new SqlCeEngine(_storedConnectionString))
                {
                    en.CreateDatabase();
                }

                System.Data.Entity.Database.SetInitializer(new FootballProDbInitializer());
                System.Data.Entity.Database.DefaultConnectionFactory = new SqlCeConnectionFactory(
                    "System.Data.SqlServerCe.4.0",
                    dbPath,
                    _storedConnectionString);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                throw;
            }
            return true;
        }

        public static bool OpenDb(int pUserId, int pDbId, string pPassword = "wave1234")
        {
            try
            {
                string dbFolder = Path.Combine(pUserId.ToString(), "db");
                string dbPath = Path.Combine(dbFolder, pDbId + ".sdf");
                _storedConnectionString = string.Format(_CONNECTION_FORMAT, dbPath, pPassword);

                if (!File.Exists(dbPath))
                    return false;

                System.Data.Entity.Database.SetInitializer(new FootballProDbInitializer());
                System.Data.Entity.Database.DefaultConnectionFactory = new SqlCeConnectionFactory(
                    "System.Data.SqlServerCe.4.0",
                    dbPath,
                    _storedConnectionString);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                throw;
            }
            return true;
        }

        #endregion Connection

    }
}

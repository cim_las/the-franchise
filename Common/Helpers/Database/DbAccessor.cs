﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ErikEJ.SqlCe;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Context;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Initializers;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Utils;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class DbAccessor
    {
        #region Fields

        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string _CONNECTION_FORMAT = "DataSource=\"{0}\"; Password='{1}'";
        private const string _DB = "db";
        private static string _storedConnectionString = string.Empty;

        #endregion Fields

        #region Properties
        #endregion Properties

        #region Connection

        public static bool CreateDb(string pDatabaseName, string pGameName, string pPassword = "present1234")
        {
            try
            {
                string dbDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _DB);
                string filePath = Path.Combine(dbDirectory, pDatabaseName + ".sdf");
                _storedConnectionString = string.Format(_CONNECTION_FORMAT, filePath, pPassword);

                if (!Directory.Exists(dbDirectory))
                {
                    Directory.CreateDirectory(dbDirectory);
                }

                using (var en = new SqlCeEngine(_storedConnectionString))
                {
                    en.CreateDatabase();
                }

                System.Data.Entity.Database.SetInitializer(new TheFranchiseDbInitializer());

                System.Data.Entity.Database.DefaultConnectionFactory = new SqlCeConnectionFactory(
                    "System.Data.SqlServerCe.4.0",
                    filePath,
                    _storedConnectionString);

                // This creates all the tables
                SaveSettings(SettingsHelper.FactorySettings(filePath, pDatabaseName));
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                throw;
            }
            return true;
        }

        public static bool OpenDb(string pPath, string pPassword = "present1234")
        {
            try
            {
                _storedConnectionString = string.Format(_CONNECTION_FORMAT, pPath, pPassword);

                if (!File.Exists(pPath))
                    return false;

                System.Data.Entity.Database.SetInitializer(new TheFranchiseDbInitializer());
                System.Data.Entity.Database.DefaultConnectionFactory = new SqlCeConnectionFactory(
                    "System.Data.SqlServerCe.4.0",
                    pPath,
                    _storedConnectionString);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                throw;
            }
            return true;
        }

        #endregion Connection

        #region Generic

        public static bool BulkSave<T>(IEnumerable<T> pEntities)
        {
            try
            {
                var options = new SqlCeBulkCopyOptions();

                using (var bc = new SqlCeBulkCopy(_storedConnectionString, options))
                {
                    bc.DestinationTableName = Pluralizer.Pluralize(typeof(T).Name);
                    bc.WriteToServer(pEntities);
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return false;
            }
        }

        #endregion Generic

        #region Settings

        public static void SaveSettings(Setting pSettings)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pSettings.Id != 0)
                    {
                        projectContext.Settings.Attach(pSettings);
                        projectContext.Entry(pSettings).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        projectContext.Settings.Add(pSettings);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
        }

        public static Setting ReadSetting()
        {
            try
            {
                // Load from db
                using (var context = new TheFranchiseDbContext())
                {
                    return context.Settings.Any() ? context.Settings.First() : null;
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        #endregion Settings

        #region People

        public static List<Person> ReadPeople(Expression<Func<Person, bool>> pPredicate = null)
        {
            try
            {
                // Load from db
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.Persons.ToList() : context.Persons.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static Person SavePerson(Person pPerson)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pPerson.Id != 0)
                    {
                        projectContext.Persons.Attach(pPerson);
                        projectContext.Entry(pPerson).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        var what = projectContext.Persons.Add(pPerson);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
            return null;
        }

        #endregion People

        #region Seasons

        public static IList<Season> ReadSeasons(Expression<Func<Season, bool>> pPredicate = null)
        {
            try
            {
                // Load from db
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.Seasons.ToList() : context.Seasons.Where(pPredicate).ToList(); ;
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static void SaveSeason(Season pSeason)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pSeason.Id != 0)
                    {
                        projectContext.Seasons.Attach(pSeason);
                        projectContext.Entry(pSeason).State = EntityState.Modified;
                    }
                    else // the Id is 0; it's a new record
                    {
                        projectContext.Seasons.Add(pSeason);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
        }

        #endregion Seasons

        #region Teams

        public static IList<Team> ReadTeams(Expression<Func<Team, bool>> pPredicate = null)
        {
            try
            {
                // Load from db
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.Teams.ToList() : context.Teams.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static void SaveTeam(Team pTeam)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pTeam.Id != 0)
                    {
                        projectContext.Teams.Attach(pTeam);
                        projectContext.Entry(pTeam).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        projectContext.Teams.Add(pTeam);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
        }

        #endregion Teams

        #region Drafts

        public static void SaveDraft(Draft pDraft)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pDraft.Id != 0)
                    {
                        projectContext.Drafts.Attach(pDraft);
                        projectContext.Entry(pDraft).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        projectContext.Drafts.Add(pDraft);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }   
        }

        public static IList<Draft> ReadDrafts(Expression<Func<Draft, bool>> pPredicate = null)
        {
            try
            {
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.Drafts.ToList() : context.Drafts.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static IList<DraftPick> ReadDraftPicks(Expression<Func<DraftPick, bool>> pPredicate = null)
        {
            try
            {
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.DraftPicks.ToList() : context.DraftPicks.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static void SaveDraftPick(DraftPick pDraftPick)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pDraftPick.Id != 0)
                    {
                        projectContext.DraftPicks.Attach(pDraftPick);
                        projectContext.Entry(pDraftPick).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        projectContext.DraftPicks.Add(pDraftPick);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }   
        }

        #endregion Drafts

        #region GameEvents

        public static IList<GameEvent> ReadGameEvents(Expression<Func<GameEvent, bool>> pPredicate = null)
        {
            try
            {
                // Load from db
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.GameEvents.ToList() : context.GameEvents.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static void SaveGameEvent(GameEvent pGameEvent)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pGameEvent.Id != 0)
                    {
                        projectContext.GameEvents.Attach(pGameEvent);
                        projectContext.Entry(pGameEvent).State = EntityState.Modified;
                    }
                    else // the Id is 0; it's a new record
                    {
                        projectContext.GameEvents.Add(pGameEvent);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
        }

        #endregion GameEvents

        #region Item

        public static IList<Item> ReadItems(Expression<Func<Item, bool>> pPredicate = null)
        {
            try
            {
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.Items.ToList() : context.Items.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static void SaveItem(Item pItem)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pItem.Id != 0)
                    {
                        projectContext.Items.Attach(pItem);
                        projectContext.Entry(pItem).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        projectContext.Items.Add(pItem);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
        }

        #endregion Item

        #region Roster

        public static List<RosterSpot> ReadRosterSpots(Expression<Func<RosterSpot, bool>> pPredicate = null)
        {
            try
            {
                // Load from db
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.RosterSpots.ToList() : context.RosterSpots.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        public static RosterSpot UpdateRoster(TeamNameEnum? pSelectedTeam, Person pSelectedPerson, PositionEnum pSelectedPosition)
        {
            if (pSelectedTeam == null)
                return null;

            var rosterSpot = ReadRosterSpots(p => p.Position == pSelectedPosition).FirstOrDefault();

            if (rosterSpot == null)
                rosterSpot = new RosterSpot() { TeamId = (int)pSelectedTeam, PersonId = pSelectedPerson.Id, Position = pSelectedPosition };
            else
                rosterSpot.PersonId = pSelectedPerson.Id;

            SaveRosterSpot(rosterSpot);

            return rosterSpot;
        }

        public static void SaveRosterSpot(RosterSpot pRosterSpot)
        {
            try
            {
                using (var projectContext = new TheFranchiseDbContext())
                {
                    if (pRosterSpot.Id != 0)
                    {
                        projectContext.RosterSpots.Attach(pRosterSpot);
                        projectContext.Entry(pRosterSpot).State = EntityState.Modified;
                    }
                    else // the ID is 0; it's a new record
                    {
                        projectContext.RosterSpots.Add(pRosterSpot);
                    }
                    projectContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
            }
        }

        #endregion Roster

        #region Abilities

        public static IList<Ability> ReadAbilities(Expression<Func<Ability, bool>> pPredicate = null)
        {
            try
            {
                using (var context = new TheFranchiseDbContext())
                {
                    return pPredicate == null ? context.Abilities.ToList() : context.Abilities.Where(pPredicate).ToList();
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(Constants.ERROR_MESSAGE_FORMAT, ex, ex.Message);
                return null;
            }
        }

        #endregion Abilities
    }
}
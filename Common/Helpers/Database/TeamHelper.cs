﻿using System.Collections.Generic;
using System.Linq;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Extensions;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class TeamHelper
    {
        public static int IntNumberOfTeams = 32;

        public static void CreateTeams()
        {
            var teams = new List<Team>();

            foreach (string teamName in TeamList)
            {
                var t = new Team
                {
                    TeamName = teamName
                };

                CalculateTeamAttributes(t);

                teams.Add(t);
            }

            DbAccessor.BulkSave(teams);
        }

        private static void CalculateTeamAttributes(Team pTeam)
        {
            pTeam.FanLoyalty = Random.RandomHelper.Instance.Next(0, 9);
            pTeam.MegaBowlsWon = Random.RandomHelper.Instance.Next(0, 6);
            pTeam.HallOfFamers = Random.RandomHelper.Instance.Next(0, 17);
            pTeam.Prestige = Random.RandomHelper.Instance.Next(0, 9);
        }

        private static List<string> _teamList;
        public static List<string> TeamList
        {
            get
            {
                if (_teamList == null)
                {
                    _teamList = new List<string>
                    {
                        TeamNameEnum.NewYorkTitans.GetDescription(),
                        TeamNameEnum.PhiladelphiaTurkeys.GetDescription(),
                        TeamNameEnum.WashingtonMemorials.GetDescription(),
                        TeamNameEnum.DallasAmericans.GetDescription(),
                        TeamNameEnum.NewOrleansCajuns.GetDescription(),
                        TeamNameEnum.AtlantaFowls.GetDescription(),
                        TeamNameEnum.CarolinaCats.GetDescription(),
                        TeamNameEnum.TampaBayPirates.GetDescription(),
                        TeamNameEnum.ChicagoGrizzlies.GetDescription(),
                        TeamNameEnum.GreenBayBackers.GetDescription(),
                        TeamNameEnum.DetroitCheetahs.GetDescription(),
                        TeamNameEnum.MinnesotaCrusaders.GetDescription(),
                        TeamNameEnum.Sanfrancisco1999ers.GetDescription(),
                        TeamNameEnum.StLouisGoats.GetDescription(),
                        TeamNameEnum.SeattleSeagulls.GetDescription(),
                        TeamNameEnum.ArizonaRedBirds.GetDescription(),
                        TeamNameEnum.NewJerseyMigs.GetDescription(),
                        TeamNameEnum.NewEnglandTeaBags.GetDescription(),
                        TeamNameEnum.MiamiSharks.GetDescription(),
                        TeamNameEnum.Buffalos.GetDescription(),
                        TeamNameEnum.TennesseMountains.GetDescription(),
                        TeamNameEnum.IndianapolisThoroughbreds.GetDescription(),
                        TeamNameEnum.HoustonAstronauts.GetDescription(),
                        TeamNameEnum.JacksonvilleLynx.GetDescription(),
                        TeamNameEnum.PittsburghMiners.GetDescription(),
                        TeamNameEnum.CincinnatiTigers.GetDescription(),
                        TeamNameEnum.BaltimoreVultures.GetDescription(),
                        TeamNameEnum.ClevelandBlues.GetDescription(),
                        TeamNameEnum.KansasCityNatives.GetDescription(),
                        TeamNameEnum.OaklandPillagers.GetDescription(),
                        TeamNameEnum.SanDiegoPower.GetDescription(),
                        TeamNameEnum.DenverMustangs.GetDescription()
                    };
                }
                return _teamList;
            }
        }

        public static void SetTeamValue()
        {
            var teams = DbAccessor.ReadTeams();
            foreach (var team in DbAccessor.ReadTeams())
            {
                int teamValue = DbAccessor.ReadPeople(p => p.TeamId == team.Id).Sum(person => person.PersonValue);

                teamValue += team.MegaBowls*25000;
                teamValue += team.MegaBowlsWon*25000;
                teamValue += team.HallOfFamers*10000;
                teamValue += team.FanLoyalty*25000;
                teamValue += team.Prestige*25000;

                team.EstimatedValue = teamValue;

                DbAccessor.SaveTeam(team);
            }            
        }
    }
}

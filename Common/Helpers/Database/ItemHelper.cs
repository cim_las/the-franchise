﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Extensions;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Database
{
    public static class ItemHelper
    {
        public static Item CreateRandomItem()
        {
            return ItemDictionary.DictionaryOfItems[(ItemEnum)RandomHelper.Instance.Next((int)ItemEnum.Begin + 1, (int)ItemEnum.End - 1)].Clone();
        }

        public static Item CreateRandomItemWithExclusions(params ItemEnum[] pEnum)
        {
            Item item;
            do
            {
                item = ItemDictionary.DictionaryOfItems[(ItemEnum) RandomHelper.Instance.Next((int) ItemEnum.Begin + 1, (int) ItemEnum.End - 1)].Clone();
            } while (pEnum.Contains(item.ItemName));

            return item;
        }

        public static string ConvertValueToSymbol(int pValue)
        {
            int i = pValue/Constants.ATTRIBUTE_SEGMENT_SIZE;
            var b = new StringBuilder();

            for (int j = 0; j < i; j++)
            {
                b.Append(Constants.ATTRIBUTE_SEGMENT_SYMBOL);
            }

            return b.ToString();
        }
    }
}

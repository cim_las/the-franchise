﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Factories;

namespace PresentSoftware.Common.Helpers.Game
{
    public static class ActionHelper
    {
        public static List<Loot> ProcessAction(Person pOwner, Person pTarget, ActionDefinition pSelectedAction, ref string pResultTitle )
        {
            var attributesUsed = AttributesUsedFactory.FactoryAttributesUsed(pSelectedAction.Name);
            bool successful = EncounterHelper.SimulateInteraction(pOwner, pTarget, pSelectedAction.Difficulty);
            var loot = new List<Loot>();
            pResultTitle = successful ? "Success!!!" : "Failed...";

            pOwner.ActionPoints -= ActionDictionary.DictionaryOfActions[pSelectedAction.Name].Cost;

            if (successful)
                loot = LootFactory.Factory(pOwner, pTarget, attributesUsed, pSelectedAction.Difficulty, pSelectedAction.Name);
            
            DbAccessor.SavePerson(pOwner);
            DbAccessor.SavePerson(pTarget);

            return loot;
        }
    }
}

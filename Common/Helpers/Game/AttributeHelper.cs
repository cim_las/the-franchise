﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Game
{
    public static class AttributeHelper
    {
        public static void AdjustSelectedAttribute(Person pPerson, AttributeEnum pEnum, int pAdjustmentAmount)
        {
            switch (pEnum)
            {
                case AttributeEnum.Mental:
                {
                    pPerson.Mental += pAdjustmentAmount;
                    break;
                }
                case AttributeEnum.Intangibles:
                {
                    pPerson.Intangibles += pAdjustmentAmount;
                    break;
                }
                case AttributeEnum.Skill:
                {
                    pPerson.Skill += pAdjustmentAmount;
                    break;
                }
            }
        }

        public static int GetRandomAttributeGain(int pLow, int pHigh)
        {
            return RandomHelper.Instance.Next(pLow, pHigh);
        }
    }
}

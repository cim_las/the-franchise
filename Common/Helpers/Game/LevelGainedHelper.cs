﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Dictionaries;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Data.Types;
using PresentSoftware.Common.Helpers.Database;
using PresentSoftware.Common.Helpers.Factories;

namespace PresentSoftware.Common.Helpers.Game
{
    public class LevelGainedHelper
    {
        public static List<Loot> ProcessLevelGained(Person pPerson, bool pIsOwner)
        {
            // Check if level(s) gained for Person
            if (pPerson.Level != (int)ExperienceLevelEnum.Level15
                && ExperienceDictionary.LevelDictionary[(ExperienceLevelEnum)pPerson.Level + 1] <= pPerson.Experience)
            {
                pPerson.Level++;

                // Now they might be level 15, if not then set experience for next level
                if (pPerson.Level != (int) ExperienceLevelEnum.Level15)
                {
                    pPerson.ExpForNextLevel = ExperienceDictionary.LevelDictionary[(ExperienceLevelEnum) pPerson.Level + 1] - pPerson.Experience;
                    pPerson.MaxExperienceForLevel = ExperienceDictionary.LevelDictionary[(ExperienceLevelEnum) pPerson.Level + 1];
                    pPerson.CurrentExperienceForLevel = pPerson.MaxExperienceForLevel - pPerson.ExpForNextLevel;
                }

                var loot = LootFactory.FactoryPersonGainedLevelAttributeIncreases(pPerson, pIsOwner);

                loot.Add(LootFactory.FactoryMaxApIncrease(pPerson, pIsOwner));

                DbAccessor.SavePerson(pPerson);

                return loot;
            }

            return null;
        }
    }
}

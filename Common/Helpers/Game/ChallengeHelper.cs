﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Core;
using PresentSoftware.Common.Data.Enums;
using PresentSoftware.Common.Data.Models;

namespace PresentSoftware.Common.Helpers.Game
{
    public static class ChallengeHelper
    {
        private const int _MINIMUM_CHALLENGE_LEVEL = 1;

        public static int DetermineChallengeLevel(Person pOwner, Person pTarget, List<AttributeEnum> pAttributesUsed)
        {
            int challengeLevel = pAttributesUsed.Count;
            int skillDifference = GetSkillDifference(pOwner, pTarget, pAttributesUsed);
            int levelDiff = pOwner.Level - pTarget.Level;

            // Target has stronger skills, increase challengeLevel
            if (skillDifference < 0)
            {
                challengeLevel += (int) skillDifference/100 + 1;
            }
            else
            {
                challengeLevel -= (int) skillDifference/100 - 1;
            }

            // Challenge is increased if you are lower level than the target
            challengeLevel += (levelDiff*-1);

            challengeLevel = Math.Max(_MINIMUM_CHALLENGE_LEVEL, challengeLevel);

            return challengeLevel;
        }

        public static int GetSkillDifference(Person pOwner, Person pTarget, List<AttributeEnum> pAttributesUsed)
        {
            int skillDifference = 0;

            foreach (var attributeEnum in pAttributesUsed)
            {
                switch (attributeEnum)
                {
                    case AttributeEnum.Intangibles:
                    {
                        skillDifference += (pOwner.IntangiblesWithItems - pTarget.IntangiblesWithItems);
                        break;
                    }
                    case AttributeEnum.Mental:
                    {
                        skillDifference += (pOwner.MentalWithItems - pTarget.MentalWithItems);
                        break;
                    }
                    case AttributeEnum.Skill:
                    {
                        skillDifference += (pOwner.SkillWithItems - pTarget.SkillWithItems);
                        break;
                    }
                }
            }
            return skillDifference;
        }
    }
}

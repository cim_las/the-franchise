﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentSoftware.Common.Data.Models;
using PresentSoftware.Common.Helpers.Random;

namespace PresentSoftware.Common.Helpers.Game
{
    public static class EncounterHelper
    {
        private const int _TWENT_SIDES = 20;
        public static bool SimulateInteraction(Person pOwner, Person pTarget, int pChallengeLevel)
        {
            int roll = RandomHelper.Instance.Next(1, _TWENT_SIDES);

            return roll > pChallengeLevel;
        }
    }
}
